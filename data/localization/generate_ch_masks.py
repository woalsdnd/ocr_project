import os
import argparse
import numpy as np
from PIL import Image

from scipy import ndimage

def generate_mask_from_bbox_coords(coords, img_shape, unit_gaussian_blur_normalized):
    
    mask = np.zeros(img_shape)
    for coord in coords:
        topleft_w, topleft_h, bottomright_w, bottomright_h = coord
        bottomright_w = int(bottomright_w)
        bottomright_h = int(bottomright_h)
        topleft_w = int(topleft_w)
        topleft_h = int(topleft_h)
        if bottomright_w > 0 and bottomright_h > 0:
            topleft_w = max(0, topleft_w)
            topleft_h = max(0, topleft_h)
            width = bottomright_w - topleft_w
            height = bottomright_h - topleft_h
            patch = cv2.resize(unit_gaussian_blur_normalized, (width, height), interpolation=cv2.INTER_LINEAR)
            for i in range(patch.shape[0]):
                for j in range(patch.shape[1]):
                    mask[topleft_h+i, topleft_w+j] = max(patch[i,j], mask[topleft_h+i, topleft_w+j])
            # mask[topleft_h:bottomright_h, topleft_w:bottomright_w] = patch # it overwrites the previous values
    
    # return np.expand_dims(mask , axis=-1)
    return mask

def all_files_under(dir_path, extension=None, append_path=True, sort=True):
    """
    List all files under a given path

    Args:
        dir_path (str): path to the directory

    Returns:
        filenames (list of str): filenames if append_path=False or filepaths if append_path=True

    Examples:
        >>> all_files_under("tmp")
        >>> all_files_under("tmp", extension=".jpg")
        >>> all_files_under("tmp", extension=".jpg", append_path=False)
    """
    if append_path:
        if extension is None:
            filenames = [os.path.join(dir_path, fname)
                         for fname in os.listdir(dir_path)]
        else:
            filenames = [os.path.join(dir_path, fname) for fname in os.listdir(
                dir_path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname)
                         for fname in os.listdir(dir_path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(
                dir_path) if fname.endswith(extension)]

    if sort:
        filenames = sorted(filenames)

    return filenames

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--path_data_home',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

for sigma in [8, 24]:
    # en-digits-specialchars
    list_fpath = all_files_under("{}/images".format(FLAGS.path_data_home))
    path_coords_file = "{}/ch_coords.txt".format(FLAGS.path_data_home)
    outdir_path  = "{}/character_masks_sigma_{}".format(FLAGS.path_data_home, sigma)
    os.makedirs(outdir_path, exist_ok=True)
    
    tmp = np.zeros((101, 101))
    tmp[50, 50] = 1
    unit_gaussian_blur = ndimage.filters.gaussian_filter(tmp, sigma=sigma)
    unit_gaussian_blur_normalized = unit_gaussian_blur / np.max(unit_gaussian_blur)

    # bbox: [topleft_w, topleft_h, bottomright_w, bottomright_h]
    with open(path_coords_file, "r") as f:
        dict_bbox = {l.rstrip().split("\t")[0].replace(".jpg", ""):[int(coord) for coord in l.rstrip().split("\t")[1].split(",")] for l in f.readlines()}

    for fpath in list_fpath:

        # get img
        img = np.array(Image.open(fpath))
        # make width and height divisible by 2
        if img.shape[0] % 2==1:
            img = img[:-1,...]
        if img.shape[1] % 2==1:
            img = img[:,:-1,...]
        h, w, _ = img.shape
        
        # get coords
        fid = os.path.basename(fpath).replace(".jpg", "")
        list_coords = []
        for bbox_index in range(1000): # assume max bbox < 1000
            bbox_id = "{}_{}".format(fid, bbox_index)
            if bbox_id in dict_bbox:
                w_min, h_min, w_max, h_max = dict_bbox[bbox_id]
                w_min = max(0, w_min)
                h_min = max(0, h_min)
                w_max = min(w_max, w)
                h_max = min(h_max, h)
                list_coords.append((w_min, h_min, w_max, h_max))
                # sanity check
                # there exist coordinates outside the image!!! no cases for w_min >= w_max or h_min >= h_max
                # img = np.array(Image.open(fpath))
                # h,w,_=img.shape
                # w_min, h_min, w_max, h_max = dict_bbox[bbox_id]
                # if np.any([d < 0 for d in dict_bbox[bbox_id]]) or w_min > w or w_max > w or h_min > h or h_max > h:
                    # print(fpath, dict_bbox[bbox_id])
                # if w_min >= w_max or h_min >= h_max:
                    # print(fpath, dict_bbox[bbox_id])
        print(fpath)
        mask = generate_mask_from_bbox_coords(list_coords, img.shape[:2], unit_gaussian_blur_normalized)
        Image.fromarray((mask*255).astype(np.uint8)).save(os.path.join(outdir_path, os.path.basename(fpath)))