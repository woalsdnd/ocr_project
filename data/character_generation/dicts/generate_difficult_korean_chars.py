from itertools import combinations
import numpy as np

N_CASES = 800000


# KSX1001 
list_korean_KSX1001_double_cons = ["몫", "넋", "삯", "얹", "앉", "않", "찮", "잖", "끊", "많",  "갉", "굵", "긁", "얽", "맑", "밝", "낡", "늙", "묽", "붉", "읽", "닭", "삵", "칡", "흙", '젊', '곪', '굶', '닮', '삶', "밟", "떫", "엷", "넓", "얇", "짧", "섧", "덟", '곬', '돐', '옰', "핥", "훑", "읊", "뚫", "앓", "끓", "잃", "꿇", "닳", "싫", "옳", "값", '없', '엾', "긍", "궁", "능", "눙", "등", "둥", "뭉", "붕", "승", "숭", "응", "웅", "증", "중", "층", "충", "킁", "쿵", "퉁", "풍", "흥", "훙", "끙", "꿍", "뚱", "쯩", "쭝", "쑹", "뿡", "켜", "꺽", "항", "화", "어", "언", "추", "초"]

N_MIN_LEN = 1
N_MAX_LEN = 10

with open("korean_difficult_characters.txt", "a") as f:
    for i in range(N_CASES):
        list_gen_chars = []
        gen_str_len = np.random.randint(N_MIN_LEN, N_MAX_LEN)
        p = np.random.uniform(0.5, 1)
        for _ in range(gen_str_len):
            list_gen_chars.append(list_korean_KSX1001_double_cons[np.random.randint(0, len(list_korean_KSX1001_double_cons))])
            
        # write to file
        generated_str = "".join(list_gen_chars)
        f.write(generated_str + "\n")
