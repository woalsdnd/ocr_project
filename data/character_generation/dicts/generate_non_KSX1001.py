from itertools import combinations
import numpy as np

N_CASES = 300000

# non-KSX1001 
list_korean = ['깄', '꺍', '붥', '닁', '럄', '얬', '칢', '긂', '읩', '뢔', '홥', '렜', '괐', '놰', '뇄', '귬', '찟', '씃', '좠', '쌰', '켙', '잌', '겍', '훕', '띡', '됭', '푈', '뱡', '샾', '궨', '궬', '됸', '솁', '캪', '똠', '슌', '똣', '뚭', '똡']

N_MIN_LEN = 1
N_MAX_LEN = 10

with open("korean_non_KSX1001.txt", "a") as f:
    for i in range(N_CASES):
        list_gen_chars = []
        gen_str_len = np.random.randint(N_MIN_LEN, N_MAX_LEN)
        p = np.random.uniform(0.5, 1)
        for _ in range(gen_str_len):
            list_gen_chars.append(list_korean[np.random.randint(0, len(list_korean))])
            
        # write to file
        generated_str = "".join(list_gen_chars)
        f.write(generated_str + "\n")