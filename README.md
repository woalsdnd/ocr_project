# OCR development for document image parsing   
### Executable files: https://drive.google.com/drive/u/1/folders/1k0k8NzCuvYJenYKCqxPkwz6dRxF_F_-C

### train neural networks ###
```python3 train_localizer.py --config_file=../config/<config_file> ```    
```python3 train_CTC.py --config_file=../config/<config_file> --mode=train```   

### train localization networks with failure cases ###
1. 데이터 생성 (img, mask)
   ```cd <home_syn_imgs>```   
   ```bash prepare_syn_imgs.sh```  
   ```mv <gen_data> <data/localization>```  
   ```cd data/localization```  
   ```python3 generate_ch_masks.py --path_data_home=<path_data_home>```  
2. 학습 ```python3 train_localizer.py --config_file=../config/localization_unet_multiple_output.cfg```    


### train CTC network with failure cases ###
1. 잘못 판별한 글자들을 추가하고 데이터 재생성  
       ```cd data/character_generation/dicts/generate_difficult_korean_chars.py``` 의 리스트에 글자 추가  
       ```python3 generate_difficult_korean_chars```  
2. 데이터 재생성  
       ```cd data/character_generation```  
       ```rm -rf korean_difficult_characters korean_difficult_characters.txt```  
       ```python3 run.py -w 1 -f 32 -l ko_en_digits_special_unambiguous  --output_dir korean_difficult_characters --input_file dicts/korean_difficult_characters.txt --name_format=2 --n_repeat=1```     
3. weight file 의 path 를 맞춰줌 (```config/ctc_5th_training.cfg``` 수정)       
4. 학습 ```python3 train_CTC.py --config_file=../config/ctc_5th_training.cfg --mode=train```   




### generate synthetic characters ###
 ```python3 run.py -w 1 -f 32 -l ko --output_dir uniform_generation --input_file dicts/ko.txt```   

### generate synthetic characters (should download from https://github.com/youngkyung/SynthText_kr)     
```bash prepare_syn_imgs.sh``` 

