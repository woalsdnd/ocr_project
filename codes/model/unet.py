import os

from keras import backend as K
from keras import objectives
from keras.layers import Conv2D, MaxPooling2D, UpSampling2D, Dense, GlobalAveragePooling2D
from keras.layers import Input
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.core import Activation, Flatten
from keras.layers.merge import Concatenate
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.optimizers import Adam

os.environ['KERAS_BACKEND'] = 'tensorflow'
K.set_image_dim_ordering('tf')


def unet(img_shape):

    # set image specifics
    k = 3  # kernel size
    s = 2  # stride
    n_filters = 32
    img_ch = 3  # image channels
    out_ch = 1  # output channel
    img_height, img_width = img_shape[0], img_shape[1]
    padding = 'same'
    
    inputs = Input((img_height, img_width, img_ch))
    conv1 = Conv2D(n_filters, (k, k), padding=padding)(inputs)
    conv1 = BatchNormalization(scale=False, axis=3)(conv1)
    conv1 = Activation('relu')(conv1)    
    conv1 = Conv2D(n_filters, (k, k), strides=(s,s), padding=padding)(conv1)
    conv1 = BatchNormalization(scale=False, axis=3)(conv1)
    conv1 = Activation('relu')(conv1)    
    
    conv2 = Conv2D(2 * n_filters, (k, k), padding=padding)(conv1)
    conv2 = BatchNormalization(scale=False, axis=3)(conv2)
    conv2 = Activation('relu')(conv2)    
    conv2 = Conv2D(2 * n_filters, (k, k), strides=(s,s), padding=padding)(conv2)
    conv2 = BatchNormalization(scale=False, axis=3)(conv2)
    conv2 = Activation('relu')(conv2)    
     
    conv3 = Conv2D(4 * n_filters, (k, k), padding=padding)(conv2)
    conv3 = BatchNormalization(scale=False, axis=3)(conv3)
    conv3 = Activation('relu')(conv3)    
    conv3 = Conv2D(4 * n_filters, (k, k), strides=(s,s), padding=padding)(conv3)
    conv3 = BatchNormalization(scale=False, axis=3)(conv3)
    conv3 = Activation('relu')(conv3)    
    
    conv4 = Conv2D(8 * n_filters, (k, k), padding=padding)(conv3)
    conv4 = BatchNormalization(scale=False, axis=3)(conv4)
    conv4 = Activation('relu')(conv4)    
    conv4 = Conv2D(8 * n_filters, (k, k), strides=(s,s), padding=padding)(conv4)
    conv4 = BatchNormalization(scale=False, axis=3)(conv4)
    conv4 = Activation('relu')(conv4)    
    
    up1 = Concatenate(axis=3)([UpSampling2D(size=(s, s))(conv4), conv3])
    deconv1 = Conv2D(8 * n_filters, (k, k), padding=padding)(up1)
    deconv1 = BatchNormalization(scale=False, axis=3)(deconv1)
    deconv1 = Activation('relu')(deconv1)    
    deconv1 = Conv2D(8 * n_filters, (k, k), padding=padding)(deconv1)
    deconv1 = BatchNormalization(scale=False, axis=3)(deconv1)
    deconv1 = Activation('relu')(deconv1)    
     
    up2 = Concatenate(axis=3)([UpSampling2D(size=(s, s))(deconv1), conv2])
    deconv2 = Conv2D(4 * n_filters, (k, k), padding=padding)(up2)
    deconv2 = BatchNormalization(scale=False, axis=3)(deconv2)
    deconv2 = Activation('relu')(deconv2)    
    deconv2 = Conv2D(4 * n_filters, (k, k), padding=padding)(deconv2)
    deconv2 = BatchNormalization(scale=False, axis=3)(deconv2)
    deconv2 = Activation('relu')(deconv2)    
    
    up3 = Concatenate(axis=3)([UpSampling2D(size=(s, s))(deconv2), conv1])
    deconv3 = Conv2D(2 * n_filters, (k, k), padding=padding)(up3)
    deconv3 = BatchNormalization(scale=False, axis=3)(deconv3)
    deconv3 = Activation('relu')(deconv3)    
    deconv3 = Conv2D(2 * n_filters, (k, k), padding=padding)(deconv3)
    deconv3 = BatchNormalization(scale=False, axis=3)(deconv3)
    deconv3 = Activation('relu')(deconv3)
    
    ch_confidence_map = Conv2D(1, (k, k), strides=(1, 1), padding="same", activation='sigmoid')(deconv3)
    ch_space_confidence_map = Conv2D(1, (k, k), strides=(1, 1), padding="same", activation='sigmoid')(deconv3)

    network = Model(inputs, [ch_confidence_map, ch_space_confidence_map], name="ocr_localizer")
    
    return network

def unet_multi_outputs(img_shape):

    # set image specifics
    k = 3  # kernel size
    s = 2  # stride
    n_filters = 32
    img_ch = 3  # image channels
    out_ch = 1  # output channel
    img_height, img_width = img_shape[0], img_shape[1]
    padding = 'same'
    
    inputs = Input((img_height, img_width, img_ch))
    conv1_1 = Conv2D(n_filters, (k, k), padding=padding)(inputs)
    conv1_1 = BatchNormalization(scale=False, axis=3)(conv1_1)
    conv1_1 = Activation('relu')(conv1_1)    
    conv1 = Conv2D(n_filters, (k, k), strides=(s,s), padding=padding)(conv1_1)
    conv1 = BatchNormalization(scale=False, axis=3)(conv1)
    conv1 = Activation('relu')(conv1)    
    
    conv2 = Conv2D(2 * n_filters, (k, k), padding=padding)(conv1)
    conv2 = BatchNormalization(scale=False, axis=3)(conv2)
    conv2 = Activation('relu')(conv2)    
    conv2 = Conv2D(2 * n_filters, (k, k), strides=(s,s), padding=padding)(conv2)
    conv2 = BatchNormalization(scale=False, axis=3)(conv2)
    conv2 = Activation('relu')(conv2)    
     
    conv3 = Conv2D(4 * n_filters, (k, k), padding=padding)(conv2)
    conv3 = BatchNormalization(scale=False, axis=3)(conv3)
    conv3 = Activation('relu')(conv3)    
    conv3 = Conv2D(4 * n_filters, (k, k), strides=(s,s), padding=padding)(conv3)
    conv3 = BatchNormalization(scale=False, axis=3)(conv3)
    conv3 = Activation('relu')(conv3)    
    
    conv4 = Conv2D(8 * n_filters, (k, k), padding=padding)(conv3)
    conv4 = BatchNormalization(scale=False, axis=3)(conv4)
    conv4 = Activation('relu')(conv4)    
    conv4 = Conv2D(8 * n_filters, (k, k), strides=(s,s), padding=padding)(conv4)
    conv4 = BatchNormalization(scale=False, axis=3)(conv4)
    conv4 = Activation('relu')(conv4)    
    
    up1 = Concatenate(axis=3)([UpSampling2D(size=(s, s))(conv4), conv3])
    deconv1 = Conv2D(8 * n_filters, (k, k), padding=padding)(up1)
    deconv1 = BatchNormalization(scale=False, axis=3)(deconv1)
    deconv1 = Activation('relu')(deconv1)    
    deconv1 = Conv2D(8 * n_filters, (k, k), padding=padding)(deconv1)
    deconv1 = BatchNormalization(scale=False, axis=3)(deconv1)
    deconv1 = Activation('relu')(deconv1)    
     
    up2 = Concatenate(axis=3)([UpSampling2D(size=(s, s))(deconv1), conv2])
    deconv2 = Conv2D(4 * n_filters, (k, k), padding=padding)(up2)
    deconv2 = BatchNormalization(scale=False, axis=3)(deconv2)
    deconv2 = Activation('relu')(deconv2)    
    deconv2 = Conv2D(4 * n_filters, (k, k), padding=padding)(deconv2)
    deconv2 = BatchNormalization(scale=False, axis=3)(deconv2)
    deconv2 = Activation('relu')(deconv2)    
    
    up3 = Concatenate(axis=3)([UpSampling2D(size=(s, s))(deconv2), conv1])
    deconv3 = Conv2D(2 * n_filters, (k, k), padding=padding)(up3)
    deconv3 = BatchNormalization(scale=False, axis=3)(deconv3)
    deconv3 = Activation('relu')(deconv3)    
    deconv3 = Conv2D(2 * n_filters, (k, k), padding=padding)(deconv3)
    deconv3 = BatchNormalization(scale=False, axis=3)(deconv3)
    deconv3 = Activation('relu')(deconv3)
    
    up4 = Concatenate(axis=3)([UpSampling2D(size=(s, s))(deconv3), conv1_1])
    deconv4 = Conv2D(n_filters, (k, k), padding=padding)(up4)
    deconv4 = BatchNormalization(scale=False, axis=3)(deconv4)
    deconv4 = Activation('relu')(deconv4)    
    deconv4 = Conv2D(n_filters, (k, k), padding=padding)(deconv4)
    deconv4 = BatchNormalization(scale=False, axis=3)(deconv4)
    deconv4 = Activation('relu')(deconv4)
    
    ch_confidence_map = Conv2D(1, (1, 1), strides=(1, 1), padding="same", activation='sigmoid')(deconv4)
    ch_space_confidence_map = Conv2D(1, (k, k), strides=(1, 1), padding="same", activation='sigmoid')(deconv4)

    network = Model(inputs, [ch_confidence_map, ch_space_confidence_map], name="ocr_localizer")
    
    return network


def unet_3outputs(img_shape):

    # set image specifics
    k = 3  # kernel size
    s = 2  # stride
    n_filters = 32
    img_ch = 3  # image channels
    out_ch = 1  # output channel
    img_height, img_width = img_shape[0], img_shape[1]
    padding = 'same'
    
    inputs = Input((img_height, img_width, img_ch))
    conv1_1 = Conv2D(n_filters, (k, k), padding=padding)(inputs)
    conv1_1 = BatchNormalization(scale=False, axis=3)(conv1_1)
    conv1_1 = Activation('relu')(conv1_1)    
    conv1 = Conv2D(n_filters, (k, k), strides=(s,s), padding=padding)(conv1_1)
    conv1 = BatchNormalization(scale=False, axis=3)(conv1)
    conv1 = Activation('relu')(conv1)    
    
    conv2 = Conv2D(2 * n_filters, (k, k), padding=padding)(conv1)
    conv2 = BatchNormalization(scale=False, axis=3)(conv2)
    conv2 = Activation('relu')(conv2)    
    conv2 = Conv2D(2 * n_filters, (k, k), strides=(s,s), padding=padding)(conv2)
    conv2 = BatchNormalization(scale=False, axis=3)(conv2)
    conv2 = Activation('relu')(conv2)    
     
    conv3 = Conv2D(4 * n_filters, (k, k), padding=padding)(conv2)
    conv3 = BatchNormalization(scale=False, axis=3)(conv3)
    conv3 = Activation('relu')(conv3)    
    conv3 = Conv2D(4 * n_filters, (k, k), strides=(s,s), padding=padding)(conv3)
    conv3 = BatchNormalization(scale=False, axis=3)(conv3)
    conv3 = Activation('relu')(conv3)    
    
    conv4 = Conv2D(8 * n_filters, (k, k), padding=padding)(conv3)
    conv4 = BatchNormalization(scale=False, axis=3)(conv4)
    conv4 = Activation('relu')(conv4)    
    conv4 = Conv2D(8 * n_filters, (k, k), strides=(s,s), padding=padding)(conv4)
    conv4 = BatchNormalization(scale=False, axis=3)(conv4)
    conv4 = Activation('relu')(conv4)    
    
    up1 = Concatenate(axis=3)([UpSampling2D(size=(s, s))(conv4), conv3])
    deconv1 = Conv2D(8 * n_filters, (k, k), padding=padding)(up1)
    deconv1 = BatchNormalization(scale=False, axis=3)(deconv1)
    deconv1 = Activation('relu')(deconv1)    
    deconv1 = Conv2D(8 * n_filters, (k, k), padding=padding)(deconv1)
    deconv1 = BatchNormalization(scale=False, axis=3)(deconv1)
    deconv1 = Activation('relu')(deconv1)    
     
    up2 = Concatenate(axis=3)([UpSampling2D(size=(s, s))(deconv1), conv2])
    deconv2 = Conv2D(4 * n_filters, (k, k), padding=padding)(up2)
    deconv2 = BatchNormalization(scale=False, axis=3)(deconv2)
    deconv2 = Activation('relu')(deconv2)    
    deconv2 = Conv2D(4 * n_filters, (k, k), padding=padding)(deconv2)
    deconv2 = BatchNormalization(scale=False, axis=3)(deconv2)
    deconv2 = Activation('relu')(deconv2)    
    
    up3 = Concatenate(axis=3)([UpSampling2D(size=(s, s))(deconv2), conv1])
    deconv3 = Conv2D(2 * n_filters, (k, k), padding=padding)(up3)
    deconv3 = BatchNormalization(scale=False, axis=3)(deconv3)
    deconv3 = Activation('relu')(deconv3)    
    deconv3 = Conv2D(2 * n_filters, (k, k), padding=padding)(deconv3)
    deconv3 = BatchNormalization(scale=False, axis=3)(deconv3)
    deconv3 = Activation('relu')(deconv3)
    
    up4 = Concatenate(axis=3)([UpSampling2D(size=(s, s))(deconv3), conv1_1])
    deconv4 = Conv2D(n_filters, (k, k), padding=padding)(up4)
    deconv4 = BatchNormalization(scale=False, axis=3)(deconv4)
    deconv4 = Activation('relu')(deconv4)    
    deconv4 = Conv2D(n_filters, (k, k), padding=padding)(deconv4)
    deconv4 = BatchNormalization(scale=False, axis=3)(deconv4)
    deconv4 = Activation('relu')(deconv4)
    
    ch_region_confidence_map = Conv2D(1, (1, 1), strides=(1, 1), padding="same", activation='sigmoid')(deconv4)
    ch_center_confidence_map = Conv2D(1, (1, 1), strides=(1, 1), padding="same", activation='sigmoid')(deconv4)
    ch_space_confidence_map = Conv2D(1, (k, k), strides=(1, 1), padding="same", activation='sigmoid')(deconv4)
    

    network = Model(inputs, [ch_region_confidence_map, ch_center_confidence_map, ch_space_confidence_map], name="ocr_localizer")
    
    return network


def set_weighted_bce_loss(network, lr_start_value, pos_weight, n_outputs=2):
    

    def bce(y_true, y_pred):
        y_true_flat = K.batch_flatten(y_true)
        y_pred_flat = K.batch_flatten(y_pred)
        return objectives.binary_crossentropy(y_true_flat, y_pred_flat)

    def weighted_bce(y_true, y_pred):
        y_true_flat = K.batch_flatten(y_true)
        y_pred_flat = K.batch_flatten(y_pred)
        y_pred_flat = K.clip(y_pred_flat, K.epsilon(), 1 - K.epsilon())
        
        weighted_binary_crossentropy = -K.mean(y_true_flat * K.log(y_pred_flat) * pos_weight
                                               + (1 - y_true_flat) * K.log(1 - y_pred_flat), axis=-1)
        return weighted_binary_crossentropy
    
    if n_outputs==3:
        network.compile(optimizer=Adam(lr=lr_start_value, beta_1=0.5), loss=[bce, weighted_bce, weighted_bce], loss_weights=[1, 1, 1], metrics=None)
    else:
        network.compile(optimizer=Adam(lr=lr_start_value, beta_1=0.5), loss=[bce, weighted_bce], loss_weights=[1, 1], metrics=None)
    return network


def unet_3outputs_smaller_network(img_shape):

    # set image specifics
    k = 3  # kernel size
    s = 2  # stride
    n_filters = 32
    img_ch = 3  # image channels
    out_ch = 1  # output channel
    img_height, img_width = img_shape[0], img_shape[1]
    padding = 'same'
    
    inputs = Input((img_height, img_width, img_ch))
    conv1 = Conv2D(n_filters, (k, k), strides=(s,s), padding=padding)(inputs)
    conv1 = BatchNormalization(scale=False, axis=3)(conv1)
    conv1 = Activation('relu')(conv1)    
    
    conv2 = Conv2D(2 * n_filters, (k, k), strides=(s,s), padding=padding)(conv1)
    conv2 = BatchNormalization(scale=False, axis=3)(conv2)
    conv2 = Activation('relu')(conv2)    
     
    conv3 = Conv2D(4 * n_filters, (k, k), strides=(s,s), padding=padding)(conv2)
    conv3 = BatchNormalization(scale=False, axis=3)(conv3)
    conv3 = Activation('relu')(conv3)    
    
    conv4 = Conv2D(8 * n_filters, (k, k), strides=(s,s), padding=padding)(conv3)
    conv4 = BatchNormalization(scale=False, axis=3)(conv4)
    conv4 = Activation('relu')(conv4)    
    
    up1 = Concatenate(axis=3)([UpSampling2D(size=(s, s))(conv4), conv3])
    deconv1 = Conv2D(8 * n_filters, (k, k), padding=padding)(up1)
    deconv1 = BatchNormalization(scale=False, axis=3)(deconv1)
    deconv1 = Activation('relu')(deconv1)    
     
    up2 = Concatenate(axis=3)([UpSampling2D(size=(s, s))(deconv1), conv2])
    deconv2 = Conv2D(4 * n_filters, (k, k), padding=padding)(up2)
    deconv2 = BatchNormalization(scale=False, axis=3)(deconv2)
    deconv2 = Activation('relu')(deconv2)    
    
    up3 = Concatenate(axis=3)([UpSampling2D(size=(s, s))(deconv2), conv1])
    deconv3 = Conv2D(2 * n_filters, (k, k), padding=padding)(up3)
    deconv3 = BatchNormalization(scale=False, axis=3)(deconv3)
    deconv3 = Activation('relu')(deconv3)    
    
    up4 = Concatenate(axis=3)([UpSampling2D(size=(s, s))(deconv3), conv1_1])
    deconv4 = Conv2D(n_filters, (k, k), padding=padding)(up4)
    deconv4 = BatchNormalization(scale=False, axis=3)(deconv4)
    deconv4 = Activation('relu')(deconv4)    
    
    ch_region_confidence_map = Conv2D(1, (1, 1), strides=(1, 1), padding="same", activation='sigmoid')(deconv4)
    ch_center_confidence_map = Conv2D(1, (1, 1), strides=(1, 1), padding="same", activation='sigmoid')(deconv4)
    ch_space_confidence_map = Conv2D(1, (k, k), strides=(1, 1), padding="same", activation='sigmoid')(deconv4)
    

    network = Model(inputs, [ch_region_confidence_map, ch_center_confidence_map, ch_space_confidence_map], name="ocr_localizer")
    
    return network

