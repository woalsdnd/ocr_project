import os
import logging
import argparse
import configparser

import cv2
import keras
import numpy as np
import random
import pandas as pd
from PIL import Image
from keras import backend as K

import utils
import iterator
import models

H_INPUT, W_INPUT = 512, 1024
FINAL_FEATURE_MAP_SIZE = (256, 512)
# FINAL_FEATURE_MAP_SIZE = (128, 256)
# FINAL_FEATURE_MAP_SIZE = (64, 128)
# FINAL_FEATURE_MAP_SIZE = (32, 64)
# FINAL_FEATURE_MAP_SIZE = (16, 32)
# FINAL_FEATURE_MAP_SIZE = (4, 8)
# FINAL_FEATURE_MAP_SIZE = (8, 16)

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--config_file',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set config
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read_file(open(FLAGS.config_file))
dir_input_check = config["Path"]["dir_input_check"]
dir_save_model = config["Path"]["dir_save_model"]
path_data = config["Path"]["path_data"]
path_load_model = config["Path"]["path_load_model"] if "path_load_model" in config["Path"] else None
dir_experimental_result = config["Path"]["dir_experimental_result"]
path_label = config["Path"]["path_label"]
dir_logger = config["Path"]["dir_logger"]
path_logger = os.path.join(dir_logger, os.path.basename(FLAGS.config_file)) 
batch_size = int(config["Train"]["batch_size"])
n_epochs = int(config["Train"]["n_epochs"])
lr_decay_tolerance = int(config["Train"]["lr_decay_tolerance"])
lr_decay_decay_factor = float(config["Train"]["lr_decay_decay_factor"])
lr_min_value = float(config["Train"]["lr_min_value"])
gpu_index = config["Train"]["gpu_index"]
loss_type = config["Train"]["loss_type"]
init_lr = float(config["Train"]["init_lr"])
utils.makedirs(dir_save_model)
utils.makedirs(dir_experimental_result)
utils.makedirs(dir_logger)

# set loss weight
list_loss_weight = []
n = 1
while "loss_weight{}".format(n) in config["Train"]:
    list_loss_weight.append(float(config["Train"]["loss_weight{}".format(n)]))
    n += 1

# set logger
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
handler = logging.FileHandler(path_logger)
handler.setFormatter(formatter)
logger.addHandler(handler)

# set gpu index
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_index

# split data
df = pd.read_csv(path_label)
assert len(df[(df["Left_Label"] != df["Right_Label"])]) == 0  # left and right labels are same
df.sort_values(by=['ASOCT_Name'], inplace=True)
# df = df[["ASOCT_Name", "label"]]
df["filepath"] = df["ASOCT_Name"].map(lambda x:os.path.join(path_data, x.replace(".jpg", ".png")))
df["X1"] = df["X1"].map(lambda x:x / 2130)
df["X2"] = df["X2"].map(lambda x:x / 2130.)
df["Y1"] = df["Y1"].map(lambda x:x / 998.)
df["Y2"] = df["Y2"].map(lambda x:x / 998.)
df["coords"] = df.apply(lambda x:(x["Y1"], x["X1"], x["Y2"], x["X2"]), axis=1)
train_data = df.iloc[:-len(df) // 10]
test_data = df.iloc[-len(df) // 10:]
# train_data = df
# test_data = df
train_input_data = train_data["filepath"]
test_input_data = test_data["filepath"]
train_label_data = train_data["coords"]
test_label_data = test_data["coords"]

# set iterator
sample_weight = utils.uniform_sample_weight(len(train_label_data))
logger.info("uniform sample weight")
train_batch_fetcher = iterator.BatchFetcher((train_input_data, train_label_data), sample_weight,
                                           utils.oct_localization_processing_func_train, batch_size, shuffle=True, replace=True)
val_batch_fetcher = iterator.BatchFetcher((test_input_data, test_label_data), None,
                                          utils.oct_localization_processing_func_val, batch_size, shuffle=False, replace=False)

# define network
pretrained_network = utils.load_network("../model/localization_segmentation_bce_l1_loss_higher_weight", True)
network = models.detection(network=pretrained_network, final_feature_map_size=FINAL_FEATURE_MAP_SIZE)
# network = utils.load_network("/home/vuno/development/AGE/localization/model/detection", True)
network = models.set_optimizer_detection(network, init_lr, list_loss_weight)
# network = models.localization_segmentation(new_network, loss_type)
# network = utils.load_network(path_load_model, True)
# network = models.set_optimizer_loose_loss(network, init_lr)
network.summary()
with open(os.path.join(dir_save_model, "network.json"), 'w') as f:
    f.write(network.to_json())

for epoch in range(n_epochs):
    # train loop
    list_training_segmentation_loss, list_training_distance_loss = [], []
    for batch in train_batch_fetcher:
        for index in range(len(batch[2])):
            if batch[2][index][1] > batch[2][index][3]:  # right, left
                right_y, right_x, left_y, left_x = batch[2][index] 
                batch[2][index] = [left_y, left_x, right_y, right_x]
        
        segmap = np.zeros((batch[1].shape[0], FINAL_FEATURE_MAP_SIZE[0], FINAL_FEATURE_MAP_SIZE[1], 1))
        cy_map = -np.ones((batch[1].shape[0], FINAL_FEATURE_MAP_SIZE[0], FINAL_FEATURE_MAP_SIZE[1], 1))
        cx_map = -np.ones((batch[1].shape[0], FINAL_FEATURE_MAP_SIZE[0], FINAL_FEATURE_MAP_SIZE[1], 1))
        for index in range(segmap.shape[0]):
            if int(FINAL_FEATURE_MAP_SIZE[0] * batch[2][index, 0]) < FINAL_FEATURE_MAP_SIZE[0] and int(FINAL_FEATURE_MAP_SIZE[1] * batch[2][index, 1]) < FINAL_FEATURE_MAP_SIZE[1]:
                segmap[index][int(FINAL_FEATURE_MAP_SIZE[0] * batch[2][index, 0]), int(FINAL_FEATURE_MAP_SIZE[1] * batch[2][index, 1]), 0] = 1
                cy_map[index][int(FINAL_FEATURE_MAP_SIZE[0] * batch[2][index, 0]), int(FINAL_FEATURE_MAP_SIZE[1] * batch[2][index, 1]), 0] = FINAL_FEATURE_MAP_SIZE[0] * batch[2][index, 0] - int(FINAL_FEATURE_MAP_SIZE[0] * batch[2][index, 0]) 
                cx_map[index][int(FINAL_FEATURE_MAP_SIZE[0] * batch[2][index, 0]), int(FINAL_FEATURE_MAP_SIZE[1] * batch[2][index, 1]), 0] = FINAL_FEATURE_MAP_SIZE[1] * batch[2][index, 1] - int(FINAL_FEATURE_MAP_SIZE[1] * batch[2][index, 1])
            if int(FINAL_FEATURE_MAP_SIZE[0] * batch[2][index, 2]) < FINAL_FEATURE_MAP_SIZE[0] and int(FINAL_FEATURE_MAP_SIZE[1] * batch[2][index, 3]) < FINAL_FEATURE_MAP_SIZE[1]:
                segmap[index][int(FINAL_FEATURE_MAP_SIZE[0] * batch[2][index, 2]), int(FINAL_FEATURE_MAP_SIZE[1] * batch[2][index, 3]), 0] = 1
                cy_map[index][int(FINAL_FEATURE_MAP_SIZE[0] * batch[2][index, 2]), int(FINAL_FEATURE_MAP_SIZE[1] * batch[2][index, 3]), 0] = FINAL_FEATURE_MAP_SIZE[0] * batch[2][index, 2] - int(FINAL_FEATURE_MAP_SIZE[0] * batch[2][index, 2]) 
                cx_map[index][int(FINAL_FEATURE_MAP_SIZE[0] * batch[2][index, 2]), int(FINAL_FEATURE_MAP_SIZE[1] * batch[2][index, 3]), 0] = FINAL_FEATURE_MAP_SIZE[1] * batch[2][index, 3] - int(FINAL_FEATURE_MAP_SIZE[1] * batch[2][index, 3])
            
        loss_total, loss_confidence, loss_cy, loss_cx = network.train_on_batch(np.repeat(np.expand_dims(batch[1], axis=-1), 3, axis=-1), [segmap, cy_map, cx_map])
        list_training_segmentation_loss += [loss_confidence] * len(batch[0])
        list_training_distance_loss += [loss_cy + loss_cx] * len(batch[0])
    logger.info("{}th epoch ==> segmentation loss: {},".format(epoch, np.mean(list_training_segmentation_loss), np.mean(list_training_distance_loss)))

    # val loop
    list_coord = [[] for _ in range(4)]
    list_gt = [[] for _ in range(4)]
    for batch in val_batch_fetcher:
        for index in range(len(batch[2])):
            if batch[2][index][1] > batch[2][index][3]:  # right, left
                right_y, right_x, left_y, left_x = batch[2][index] 
                batch[2][index] = [left_y, left_x, right_y, right_x]
        
        # save gt
        list_gt[0] += list(batch[2][:, 0])
        list_gt[1] += list(batch[2][:, 1])
        list_gt[2] += list(batch[2][:, 2])
        list_gt[3] += list(batch[2][:, 3])
        
        confidence_map, cy_map, cx_map = network.predict(np.repeat(np.expand_dims(batch[1], axis=-1), 3, axis=-1))
        _, h, w, _ = confidence_map.shape
        confidence_map[:, :, 3 * w // 8:5 * w // 8, :] = 0  # delete center area
        for index in range(len(batch[2])):
            for laterality in ["left", "right"]:
                target = confidence_map[index, :, :w // 2, 0]  if laterality == "left" else confidence_map[index, :, w // 2:, 0]
                index_max = np.argwhere(target.max() == target)[0]
                if laterality == "left":
                    list_coord[0].append((index_max[0] + cy_map[0, index_max[0], index_max[1], 0]) * H_INPUT // h)
                    list_coord[1].append((index_max[1] + cx_map[0, index_max[0], index_max[1], 0]) * W_INPUT // w)
                elif laterality == "right":
                    list_coord[2].append((index_max[0] + cy_map[0, index_max[0], index_max[1] + w // 2, 0]) * H_INPUT // h)
                    list_coord[3].append(W_INPUT // 2 + (index_max[1] + cx_map[0, index_max[0], index_max[1] + w // 2, 0]) * W_INPUT // w)
        
    arr_Y_LEFT = np.array(list_coord[0]) * 998. / H_INPUT
    arr_X_LEFT = np.array(list_coord[1]) * 2130. / W_INPUT
    arr_Y_RIGHT = np.array(list_coord[2]) * 998. / H_INPUT
    arr_X_RIGHT = np.array(list_coord[3]) * 2130. / W_INPUT
    
    error_left = np.sqrt(np.square(arr_Y_LEFT - np.array(list_gt[0]) * 998.) + np.square(arr_X_LEFT - np.array(list_gt[1]) * 2130.))
    error_right = np.sqrt(np.square(arr_Y_RIGHT - np.array(list_gt[2]) * 998.) + np.square(arr_X_RIGHT - np.array(list_gt[3]) * 2130.))
    total_error = (error_left + error_right) / 2
    distance = np.mean(total_error)
    logger.info("validation at {}th epoch -- left distance: {}, right distance: {}, total distance: {} pixels".format(epoch, np.mean(error_left), np.mean(error_right), distance))
    
    # adjust learning rate
    if K.get_value(network.optimizer.lr) < lr_min_value:  # restart when lr is less than min 
        K.set_value(network.optimizer.lr, init_lr)
        logger.info("set learning rate to ".format(init_lr))
    elif "best_distance" not in locals() and "best_distance" not in globals():
        best_distance = 100
        n_no_improvement = 0
        init_lr = K.get_value(network.optimizer.lr)
    else:
        if best_distance > distance:
            best_distance = distance
            n_no_improvement = 0
            # save network
            network.save_weights(os.path.join(dir_save_model, "weight_least_distance.h5"))
        else:
            n_no_improvement += 1
            if n_no_improvement == lr_decay_tolerance:
                new_lr = K.get_value(network.optimizer.lr) * lr_decay_decay_factor
                K.set_value(network.optimizer.lr, new_lr)    
                logger.info("set learning rate to {}".format(new_lr))
                n_no_improvement = 0
    
    # save network
    if epoch % 1 == 0:
        network.save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))
