import os
import logging
import argparse
import configparser
import time

import numpy as np
import pandas as pd
import random
from PIL import Image

import utils
import iterator_target_ch_extractor_shared_array
from model.efficient_net import tiny_network_target_ch_extraction, tiny_network_target_ch_extraction_v2, tiny_network_target_ch_extraction_v3, set_optimizer_detection

FEATURE_MAP_SIZE = (4, 4)

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--config_file',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set config
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read_file(open(FLAGS.config_file))
dir_training_input_data1 = config["Path"]["path_data1"]
dir_training_input_data2 = config["Path"]["path_data2"]
dir_training_input_data3 = config["Path"]["path_data3"]
dir_save_model = config["Path"]["dir_save_model"]
path_pretrained_weight = config["Path"]["path_pretrained_weight"] if "path_pretrained_weight" in config["Path"] else None
dir_experimental_result = os.path.join(config["Path"]["dir_experimental_result"], os.path.basename(FLAGS.config_file).replace(".cfg", ".log")) 
dir_logger = config["Path"]["dir_logger"]
path_logger = os.path.join(dir_logger, os.path.basename(FLAGS.config_file).replace(".cfg", ".log")) 
batch_size = int(config["Train"]["batch_size"])
n_epochs = int(config["Train"]["n_epochs"])
lr_decay_tolerance = int(config["Train"]["lr_decay_tolerance"])
lr_decay_factor = float(config["Train"]["lr_decay_factor"])
lr_start_value = float(config["Train"]["lr_start"])
lr_min_value = float(config["Train"]["lr_min_value"])
optimizer = config["Train"]["optimizer"]
gpu_index = config["Train"]["gpu_index"]
architecture = config["Train"]["architecture"] if "architecture" in config["Train"] else None
input_shape = (int(config["Input"]["height"]), int(config["Input"]["width"]), int(config["Input"]["depth"]))
utils.makedirs(dir_save_model)
utils.makedirs(dir_experimental_result)
utils.makedirs(dir_logger)
n_character_classes = len(utils.list_string)

# set logger and tensorboard
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
handler = logging.FileHandler(path_logger)
handler.setFormatter(formatter)
logger.addHandler(handler)
tensorboard = utils.CustomTensorBoard(
  log_dir=os.path.join(dir_logger, "tensorboard", os.path.basename(FLAGS.config_file).replace(".cfg", ".log")),
  write_graph=False,
  batch_size=batch_size
)

# set gpu index
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_index

# split data
path_data1 = utils.all_files_under(dir_training_input_data1)
path_data2 = utils.all_files_under(dir_training_input_data2)
path_data3 = utils.all_files_under(dir_training_input_data3)
list_path_data = path_data1 + path_data2 + path_data3
random.seed(0)
random.shuffle(list_path_data)
train_input_data = list_path_data[:-10]
val_input_data = list_path_data[-10:]
# set iterator
train_sample_weight = utils.uniform_sample_weight(len(train_input_data))
train_batch_fetcher = iterator_target_ch_extractor_shared_array.BatchFetcher((train_input_data, ), train_sample_weight,
                                           utils.ocr_target_ch_extraction_processing_func, batch_size, sample=True, replace=True, shared_array_shape=[input_shape, (4,)])
val_sample_weight = utils.uniform_sample_weight(len(val_input_data))
val_batch_fetcher = iterator_target_ch_extractor_shared_array.BatchFetcher((val_input_data, ), val_sample_weight,
                                          utils.ocr_target_ch_extraction_processing_func, batch_size, sample=True, replace=False, shared_array_shape=[input_shape, (4,)])

# define network
if path_pretrained_weight:
    if architecture=="tiny":
        network = tiny_network_target_ch_extraction(input_shape=input_shape)
    elif architecture=="tiny_v2":
        network = tiny_network_target_ch_extraction_v2(input_shape=input_shape)
    elif architecture=="tiny_v3":
        network = tiny_network_target_ch_extraction_v3(input_shape=input_shape)
    network = set_optimizer_detection(network, lr_start_value, [1,1,1]) 
    network.load_weights(path_pretrained_weight)
    logger.info("model loaded from {}".format(path_pretrained_weight))
else:
    if architecture=="tiny":
        network = tiny_network_target_ch_extraction(input_shape=input_shape)
    elif architecture=="tiny_v2":
        network = tiny_network_target_ch_extraction_v2(input_shape=input_shape)
    elif architecture=="tiny_v3":
        network = tiny_network_target_ch_extraction_v3(input_shape=input_shape)
    network = set_optimizer_detection(network, lr_start_value, [1,1,1]) 
network.summary()
tensorboard.set_model(network)  # set tensorboard callback associated with network
# save network
with open(os.path.join(dir_save_model, "network.json"), 'w') as f:
    f.write(network.to_json())

lr_scheduler = utils.LrScheduler(lr_object=network.optimizer.lr, lr_start_value=lr_start_value,
                                 lr_min_value=lr_min_value, lr_decay_tolerance=lr_decay_tolerance,
                                 lr_decay_factor=lr_decay_factor, logger=logger, score_func=lambda x:x["L2_distance"])
h, w = FEATURE_MAP_SIZE
for epoch in range(n_epochs):
    # train loop
    list_training_segmentation_loss, list_training_distance_loss = [], []
    for data, list_arr in train_batch_fetcher:
        imgs , coords_on_imgs = list_arr
        batch_size, h_img, w_img, _ = imgs.shape
        coords = np.copy(coords_on_imgs) # (min_x, min_y, max_x, max_y)
        coords[:,0] /= w_img
        coords[:,1] /= h_img
        coords[:,2] /= w_img
        coords[:,3] /= h_img
        segmap = np.zeros((batch_size, h, w, 1))
        cy_map = -np.ones((batch_size, h, w, 1))
        cx_map = -np.ones((batch_size, h, w, 1))
        for index in range(batch_size):
            segmap[index, int(h * coords[index, 1]), int(w * coords[index, 0]), 0] = 1
            cy_map[index, int(h * coords[index, 1]), int(w * coords[index, 0]), 0] = h * coords[index, 1] - int(h * coords[index, 1]) 
            cx_map[index, int(h * coords[index, 1]), int(w * coords[index, 0]), 0] = w * coords[index, 0] - int(w * coords[index, 0])
            segmap[index, int(h * coords[index, 3]), int(w * coords[index, 2]), 0] = 1
            cy_map[index, int(h * coords[index, 3]), int(w * coords[index, 2]), 0] = h * coords[index, 3] - int(h * coords[index, 3]) 
            cx_map[index, int(h * coords[index, 3]), int(w * coords[index, 2]), 0] = w * coords[index, 2] - int(w * coords[index, 2])

            # check input masks
            # for coord_type in ["topleft", "bottomright"]:
            #     target = segmap[index, :h // 2, :w // 2, 0]  if coord_type == "topleft" else segmap[index, h // 2:, w // 2:, 0]
            #     max_args = np.argwhere(target.max() == target)[0]
            #     if coord_type == "topleft":
            #         topleft_y = int((max_args[0] + cy_map[index, max_args[0], max_args[1], 0]) * h_img // h)
            #         topleft_x = int((max_args[1] + cx_map[index, max_args[0], max_args[1], 0]) * w_img // w)
            #     elif coord_type == "bottomright":
            #         bottomright_y = int(h_img // 2 + (max_args[0] + cy_map[index, max_args[0] + h // 2, max_args[1] + w // 2, 0]) * h_img // h)
            #         bottomright_x = int(w_img // 2 + (max_args[1] + cx_map[index, max_args[0] + h // 2, max_args[1] + w // 2, 0]) * w_img // w)
            
            # img_for_output = (imgs[index,...,0] * 10 +50).astype(np.uint8)
            # img_for_output[topleft_y, topleft_x] = 255
            # img_for_output[bottomright_y, bottomright_x] = 255
            # Image.fromarray(img_for_output).save("debug/{}th_img.png".format(index))

        loss_total, loss_confidence, loss_cy, loss_cx = network.train_on_batch(imgs, [segmap, cy_map, cx_map])
        list_training_segmentation_loss += [loss_confidence] * batch_size
        list_training_distance_loss += [loss_cy + loss_cx] * batch_size
    train_metrics = {"training_seg_loss":np.mean(list_training_segmentation_loss), "training_dist_loss":np.mean(list_training_distance_loss)}
    utils.log_summary(logger, phase="training", epoch=epoch, **train_metrics)
    tensorboard.on_epoch_end(epoch, train_metrics)

    # val loop
    list_val_loss = []
    for data, list_arr in val_batch_fetcher:
        imgs , coords = list_arr
        batch_size, h_img, w_img, _ = imgs.shape
        confidence_map, cy_map, cx_map = network.predict(imgs)

        _, h, w, _ = confidence_map.shape
        list_coord = [[] for _ in range(4)]
        for index in range(batch_size):
            for coord_type in ["topleft", "bottomright"]:
                target = confidence_map[index, :h // 2, :w // 2, 0]  if coord_type == "topleft" else confidence_map[index, h // 2:, w // 2:, 0]
                max_args = np.argwhere(target.max() == target)[0]
                if coord_type == "topleft":
                    topleft_y = int((max_args[0] + cy_map[index, max_args[0], max_args[1], 0]) * h_img // h)
                    topleft_x = int((max_args[1] + cx_map[index, max_args[0], max_args[1], 0]) * w_img // w)
                    list_coord[0].append(topleft_y)
                    list_coord[1].append(topleft_x)
                elif coord_type == "bottomright":
                    bottomright_y = int(h_img // 2 + (max_args[0] + cy_map[index, max_args[0] + h // 2, max_args[1] + w // 2, 0]) * h_img // h)
                    bottomright_x = int(w_img // 2 + (max_args[1] + cx_map[index, max_args[0] + h // 2, max_args[1] + w // 2, 0]) * w_img // w)
                    list_coord[2].append(bottomright_y)
                    list_coord[3].append(bottomright_x)
            
            img_for_output = (imgs[index,...,0] * 10 +50).astype(np.uint8)
            img_for_output[topleft_y, topleft_x] = 255
            img_for_output[bottomright_y, bottomright_x] = 255
            Image.fromarray(img_for_output).save(os.path.join(dir_experimental_result, "{}th_img_{}th_epoch.png".format(index, epoch)))

        error_topleft = np.sqrt(np.square(np.array(list_coord[0]) - coords[:,1]) + np.square(np.array(list_coord[1]) - coords[:,0]))
        error_bottomright = np.sqrt(np.square(np.array(list_coord[2]) - coords[:,3]) + np.square(np.array(list_coord[3]) - coords[:,2]))
        total_error = (error_topleft + error_bottomright) / 2
        distance = np.mean(total_error)
        list_val_loss.append([distance]*batch_size)
    val_metrics={"L2_distance":np.mean(list_val_loss)}
    utils.log_summary(logger, phase="validation", epoch=epoch, **val_metrics)
    tensorboard.on_epoch_end(epoch, val_metrics)

    # adjust learning rate
    lr_scheduler.adjust_lr(epoch, val_metrics)

    # save network
    network.save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))