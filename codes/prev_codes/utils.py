import os
import random
import time
import io

import numpy as np
import math
import pandas as pd
import matplotlib
import matplotlib.cm as cm
import tensorflow as tf

from sklearn.metrics import confusion_matrix
from sklearn.metrics.ranking import roc_auc_score
from sklearn.utils import class_weight
from PIL import Image, ImageEnhance, ImageFilter
from subprocess import Popen, PIPE
from skimage.transform import warp, AffineTransform
from skimage.util import random_noise
from scipy.misc import imresize

from keras.models import model_from_json
from keras import backend as K
from keras.callbacks import TensorBoard

from skimage import measure
from sklearn.manifold import TSNE
from sklearn.metrics import cohen_kappa_score
from sklearn.cross_decomposition import CCA
from scipy.stats import pearsonr

from albumentations.augmentations import transforms

import cv2 

list_string = ['훨', '양', '늦', '러', '려', '희', '녹', '생', '턴', '증', '케', '윽', '얕', '휴', '껏', '변', '잇', '게', '섹', '치', '뉴', '테', '훈', '폴', '섞', '혐', '먼', '킬', '손', '베', '대', '쾌', '줬', '헌', '즌', '크', '걱', '쩐', '켜', '배', '권', '겪', '겨', '킹', '태', '곡', '튜', '버', '쳐', '덩', '셀', '히', '쩍', '씹', '몹', '른', '매', '멍', '롭', '럿', '명', '킨', '삶', '밖', '츰', '성', '뮤', '까', '락', '출', '슘', '황', '좋', '옮', '결', '어', '읩', '코', '륜', '륙', '미', '묻', '김', '최', '혼', '마', '쁨', '감', '넓', '값', '뼉', '깅', '액', '링', '였', '잉', '쳤', '롬', '빼', '줍', '엎', '뵙', '심', '끄', '복', '얀', '리', '숙', '참', '읽', '뿐', '뿌', '싶', '몸', '밟', '댓', '섬', '캘', '판', '션', '흡', '욱', '앵', '픔', '발', '은', '곱', '팅', '주', '든', '딧', '즘', '웍', '쌍', '흘', '골', '돕', '깐', '준', '앗', '젖', '육', '슨', '줄', '움', '랫', '놀', '튿', '땅', '끓', '다', '촉', '더', '뺏', '검', '탤', '청', '죄', '솜', '젠', '그', '씌', '못', '놔', '엿', '뇌', '객', '온', '컵', '틀', '스', '둥', '졸', '빚', '위', '건', '웃', '털', '튀', '멈', '즉', '섯', '율', '팔', '애', '릎', '낮', '쿠', '천', '뽑', '혜', '첫', '정', '듬', '원', '오', '저', '약', '랙', '낱', '렌', '징', '툼', '폐', '픽', '귀', '윗', '냇', '빗', '화', '꼭', '간', '만', '홍', '찢', '깎', '역', '붓', '찍', '로', '짚', '왜', '막', '긁', '흑', '콘', '낸', '뻗', '빵', '빡', '없', '묘', '눔', '냐', '찰', '었', '논', '끌', '섰', '넘', '파', '겼', '물', '냥', '엔', '뛰', '융', '방', '똑', '달', '항', '왼', '봤', '계', '령', '고', '깍', '인', '유', '체', '푸', '비', '빌', '몬', '톤', '금', '란', '뜩', '팎', '탈', '쇄', '덤', '곤', '맹', '푼', '눕', '민', '휘', '찾', '릇', '왠', '혔', '직', '옳', '썹', '렵', '림', '벤', '팝', '찬', '굵', '언', '벼', '품', '도', '해', '바', '례', '특', '므', '현', '붐', '분', '한', '글', '짙', '떨', '뜨', '옛', '너', '광', '셔', '몇', '맨', '쌀', '숫', '쫄', '갈', '당', '망', '촌', '뻐', '벗', '브', '쨌', '낭', '라', '환', '켓', '택', '협', '낡', '딩', '꽂', '점', '맑', '걀', '자', '께', '구', '근', '응', '후', '싸', '곁', '데', '홀', '랜', '좁', '혀', '냄', '렉', '볕', '늑', '줌', '뜯', '왕', '옆', '맙', '돗', '춧', '네', '귓', '영', '람', '밉', '될', '탐', '틸', '나', '가', '숭', '눈', '젓', '헬', '쿄', '갔', '믿', '찻', '끗', '음', '펜', '춰', '댁', '붉', '틈', '쇠', '받', '페', '뤼', '솥', '터', '착', '일', '룹', '샌', '끔', '식', '캄', '햄', '꼬', '낙', '뚜', '지', '흥', '꿈', '염', '씩', '략', '던', '굴', '졌', '놓', '떤', '떡', '찌', '녕', '톨', '뇽', '붙', '얹', '늪', '캠', '뱉', '밀', '뷰', '칠', '흐', '롱', '취', '펼', '수', '팽', '앞', '공', '외', '조', '법', '업', '층', '올', '에', '됐', '상', '측', '혁', '템', '재', '벨', '처', '픈', '툴', '밑', '떼', '뵈', '삭', '쩔', '레', '퇴', '멀', '국', '총', '돋', '죠', '종', '밭', '늘', '추', '시', '랍', '않', '았', '죽', '렬', '쟁', '덥', '슈', '닌', '우', '팩', '균', '좌', '얼', '츄', '트', '젯', '맛', '르', '닷', '폰', '과', '번', '져', '보', '되', '삼', '껑', '말', '많', '강', '늬', '센', '슴', '긴', '뉜', '녁', '잃', '겠', '맵', '찮', '승', '칙', '규', '긋', '맞', '축', '엽', '낫', '척', '같', '넷', '엉', '왔', '땀', '률', '습', '텍', '짧', '쑤', '롯', '여', '년', '엘', '닫', '패', '있', '윌', '전', '기', '료', '설', '븐', '접', '형', '님', '꾼', '단', '요', '퍼', '프', '돌', '속', '얗', '싹', '놈', '뒷', '닮', '옷', '느', '득', '념', '암', '듣', '존', '송', '병', '윤', '뻔', '젝', '걷', '뒤', '딪', '썩', '향', '짐', '꼈', '멋', '춤', '깃', '포', '째', '빙', '퉁', '옥', '녀', '잦', '챙', '굳', '뉘', '씀', '억', '꾸', '하', '쩜', '홈', '덧', '껍', '둘', '능', '깝', '맘', '랐', '싯', '밝', '펴', '캐', '딸', '곳', '뚫', '통', '예', '린', '칫', '볼', '격', '타', '칼', '맣', '셜', '극', '교', '튼', '낯', '행', '괴', '야', '괄', '봉', '넉', '헤', '핑', '띄', '앨', '활', '뭇', '잡', '룻', '풍', '촛', '컴', '렁', '램', '혹', '웬', '긍', '껴', '블', '멩', '새', '집', '효', '삿', '듯', '볶', '앙', '떻', '얄', '엊', '횟', '충', '탄', '편', '떠', '킴', '훼', '량', '꽃', '허', '맡', '으', '닭', '굉', '댐', '탕', '장', '백', '퓨', '련', '릭', '챔', '깊', '소', '흔', '뀌', '맺', '담', '며', '즈', '빔', '날', '궁', '혈', '몰', '진', '답', '플', '갖', '남', '뜸', '깜', '제', '창', '슬', '완', '별', '빈', '꽤', '홉', '신', '룩', '익', '텔', '셋', '럼', '굽', '솟', '빛', '뚝', '엌', '동', '쭈', '텐', '류', '춘', '아', '런', '쁘', '잘', '델', '빠', '럴', '벌', '부', '웅', '목', '뚱', '틱', '술', '멸', '옹', '확', '닦', '몽', '낚', '농', '했', '얇', '사', '컸', '싼', '갓', '팬', '앤', '큼', '적', '의', '숨', '쉬', '깡', '늄', '핸', '때', '끝', '됨', '카', '학', '북', '훔', '순', '톱', '산', '닥', '곧', '욕', '세', '덜', '중', '덟', '키', '납', '싫', '콤', '쥐', '퀴', '앓', '악', '콩', '티', '닿', '팀', '및', '살', '첩', '훌', '각', '봄', '따', '톡', '친', '무', '쓰', '알', '초', '토', '씻', '둑', '디', '빨', '뱃', '들', '샤', '립', '메', '력', '섭', '압', '즐', '뢰', '랑', '십', '덮', '늙', '본', '된', '난', '켰', '힌', '면', '쌓', '높', '짜', '경', '서', '쎄', '컨', '독', '쏘', '흩', '를', '노', '큰', '입', '랗', '급', '잎', '밤', '머', '엄', '쇼', '넣', '둠', '됩', '험', '솔', '록', '츠', '럽', '숟', '멎', '박', '울', '거', '짝', '핏', '럭', '획', '쪽', '볍', '걸', '괜', '워', '딘', '젊', '탁', '열', '닐', '이', '색', '싣', '견', '범', '니', '돈', '횡', '꼼', '족', '운', '컬', '냉', '릿', '안', '질', '석', '문', '얘', '웠', '실', '을', '벽', '낳', '임', '연', '둡', '씨', '호', '끊', '겁', '잠', '잊', '잖', '싱', '룬', '깨', '먹', '갛', '선', '는', '함', '쓴', '폭', '뭄', '용', '깔', '봇', '샀', '잣', '철', '널', '절', '읍', '뇨', '관', '멘', '꽁', '햇', '렇', '덕', '월', '밌', '씬', '깥', '쏟', '륭', '커', '채', '길', '둔', '촬', '할', '짓', '묶', '흰', '춥', '잔', '넥', '풀', '밥', '투', '내', '갚', '엇', '것', '낄', '묵', '칩', '콜', '와', '등', '드', '론', '썰', '맥', '랄', '힘', '딱', '컷', '칭', '갑', '반', '넌', '회', '뤄', '차', '났', '클', '또', '냈', '슷', '낌', '책', '돼', '피', '꼽', '갇', '모', '표', '엷', '끼', '써', '흉', '꺼', '굶', '평', '렘', '름', '침', '개', '쓸', '듭', '합', '작', '두', '쫓', '루', '웨', '불', '필', '누', '쩌', '얻', '핵', '렸', '렀', '륨', '래', '겹', '휩', '쉽', '붕', '군', '앉', '뜻', '꺾', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '+', '=', '|', '\\', '}', '{', '[', ']', "'", '"', ';', ':', '/', '?', '.', '>', ',', '<', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0']

########################################################
########################################################
############### GENERAL UTILITY CLASSES ################
########################################################
########################################################
class CustomTensorBoard(TensorBoard):
    """
    Custom Tensor Board class inheriting keras.callbacks.TensorBoard
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.has_plot_img = False

    def draw_imgs(self, tag, epoch, img_arr, plot_once=False):
        """
        Draw images in numpy array format. Assume epoch starts with 0.

        Args:
            tag: str
            epoch: int
            img_arr: (N, H, W, D)
            plot_once: bool

        Returns:
            None
            summary will be written in the log path

        Raises:
            AssertionError: wrong dtypes (tag, epoch, img_arr)
            AssertionError: img_arr is not 4-dimensional (N, H, W, D)

        """
        if (plot_once and not self.has_plot_img) or not plot_once:
            assert isinstance(tag, str) and isinstance(
                epoch, int) and isinstance(img_arr, np.ndarray)
            img_shape = img_arr.shape
            assert len(img_shape) == 4
            n, height, width, channel = img_shape
            writer = tf.summary.FileWriter(self.log_dir)
            for index in range(n):
                image = Image.fromarray(img_arr[index])
                with io.BytesIO() as f:
                    image.save(f, format='PNG')
                    image_string = f.getvalue()
                    tf_img = tf.Summary.Image(
                        height=height, width=width, colorspace=channel, encoded_image_string=image_string)
                    summary = tf.Summary(value=[tf.Summary.Value(
                        tag="{}/{}".format(tag, index), image=tf_img)])
                    writer.add_summary(summary, epoch)
            writer.close()
            self.has_plot_img = True


class LrScheduler:
    """
    Learning Rate Scheduler.
    Reduce on validation plateau and reset to init lr when the lr becomes less than minimum
    """

    def __init__(self, lr_object, lr_start_value, lr_min_value, lr_decay_tolerance, lr_decay_factor, logger, score_func):
        """
        Args:
            lr_object: keras optimizer lr object
            lr_start_value: float
            lr_min_value: float
            lr_decay_tolerance: int
            lr_decay_factor: float
            logger: logging object
            score_func: function that maps validation metric to scalar
        """
        self.lr_object = lr_object
        self.lr_start_value = lr_start_value
        self.lr_min_value = lr_min_value
        self.lr_decay_tolerance = lr_decay_tolerance
        self.lr_decay_factor = lr_decay_factor
        self.logger = logger
        self.best_val_score = None
        self.score_func = score_func
        self.n_no_improvements = 0

    def adjust_lr(self, epoch, val_metrics):
        curr_val_score = self.score_func(val_metrics)
        if self.best_val_score is None:  # first epoch
            self.best_val_score = curr_val_score
        elif self.best_val_score < curr_val_score:  # improved
            self.best_val_score = curr_val_score
            self.n_no_improvements = 0
        else:  # not improved
            self.n_no_improvements += 1
            if self.n_no_improvements == self.lr_decay_tolerance:
                new_lr = K.get_value(self.lr_object) * self.lr_decay_factor
                K.set_value(self.lr_object, new_lr)
                self.logger.info(
                    "set learning rate to {} at epoch {}".format(new_lr, epoch))
                self.n_no_improvements = 0

        # restart when lr is less than lr_min
        if K.get_value(self.lr_object) < self.lr_min_value:
            K.set_value(self.lr_object, self.lr_start_value)
            self.logger.info("reset learning rate to {} at epoch {}".format(self.lr_start_value, epoch))


########################################################
########################################################
############## GENERAL UTILITY FUNCTIONS ###############
########################################################
########################################################
def count2ratio(data):
    """
    return a list of ratio from list (or array) of count

    Args:
        data: (array-like) count

    Returns:
        list of a ratio
        logs will be written in the file specified by the logger

    Examples:
        >>> count2ratio([4,3,2,1])
        >>> [0.4, 0.3, 0.2, 0.1]

    Raises:
        AssertionError: data in the format of list, tuple or ndarray

    """
    assert isinstance(data, (list, tuple, np.ndarray)
                      )  # data should be given as an array-like object

    return [1.*c / np.sum(data) for c in data]


def log_summary(logger, **kwargs):
    """
    Leave logs to logger.
    Reserved keyword: "phase", "epoch"

    Args:
        logger: logging object

    Returns:
        None
        logs will be written in the file specified by the logger

    """
    assert "phase" in kwargs.keys() and "epoch" in kwargs.keys(
    )  # training, validation, test info
    phase, epoch = kwargs["phase"], kwargs["epoch"]
    logging_msg = "{} at {}th epoch -- ".format(phase, epoch)
    for metric in [key for key in kwargs.keys() if key != "msg" and key != "phase" and key != epoch]:
        logging_msg = logging_msg + "{} : {}, ".format(metric, kwargs[metric])

    logger.info(logging_msg)


def stack_list(**kwargs):
    """
    Stack tail (list) to head (list)

    Args:
        head<num>, tail<num> (e.g. head1, tail1, head2, tail2, ...)

    Returns:
        None
        head lists will appended tail lists individually

    Raises:
        ValueError: head and tail should be list 
        AssertionError: head<num>, tail<num> should be given as keywords together

    """
    head_keywords = [key for key in kwargs.keys() if "head" in key]
    for head_keyword in head_keywords:
        tail_keyword = head_keyword.replace("head", "tail")
        assert tail_keyword in kwargs.keys()
        assert isinstance(kwargs[head_keyword], list)
        assert isinstance(kwargs[tail_keyword], list)
        kwargs[head_keyword] += kwargs[tail_keyword]


def compute_CCA(list_arr1, list_arr2):
    """
    Compute CCA between two list of arrays

    Args:
        list_arr1 (list of arrays with length M)
        list_arr2 (list of arrays with length N)

    Returns:
        CCA value in MxN matrix 

    """
    result = np.zeros((len(list_arr1), len(list_arr2)))
    for m in range(len(list_arr1)):
        for n in range(len(list_arr2)):
            min_dim = 1
            cca = CCA(n_components=min_dim)
            try:
                cca.fit(list_arr1[m], list_arr2[n])
                list_rho = []
                for i in range(min_dim):
                    list_rho.append(
                        pearsonr(cca.x_scores_[:, i], cca.y_scores_[:, i])[0])
                score = np.mean(list_rho)
            except:
                score = 0

            result[m, n] = score
    return result


def copy_weights(from_network, to_network):
    """
    Copy weights from "from_network" to "to_network"
    Weights are copied if the shape matches, first-come-first-served from lower layers

    Args:
        from_network: keras model from which weights are copied
        to_network: keras model to which weights are copied

    Returns:
        to_network: keras model with weights copied
    """
    from_layers_with_weights = [
        l for l in from_network.layers if len(l.get_weights()) > 0]
    to_layers_with_weights = [
        l for l in to_network.layers if len(l.get_weights()) > 0]

    from_layers_weight_shapes = []
    for from_layer in from_layers_with_weights:
        from_layers_weight_shapes.append(
            [w.shape for w in from_layer.get_weights()])
    to_layers_weight_shapes = []
    for to_layer in to_layers_with_weights:
        to_layers_weight_shapes.append(
            [w.shape for w in to_layer.get_weights()])

    curr_index_from_layer = 0
    for index_to_layer in range(len(to_layers_with_weights)):
        for index_from_layer in range(curr_index_from_layer, len(from_layers_with_weights)):
            to_layer_shapes = to_layers_weight_shapes[index_to_layer]
            from_layer_shapes = from_layers_weight_shapes[index_from_layer]
            if len(to_layer_shapes) == len(from_layer_shapes) and np.all([to_layer_shapes[index] == from_layer_shapes[index] for index in range(len(from_layer_shapes))]):
                to_layers_with_weights[index_to_layer].set_weights(
                    from_layers_with_weights[index_from_layer].get_weights())
                print("{} loaded from {}".format(
                    to_layers_with_weights[index_to_layer].name, from_layers_with_weights[index_from_layer].name))
                curr_index_from_layer = index_from_layer + 1
                break

    return to_network


def run_tsne(features):
    """
    T-SNE to 2d plane

    Args:
        features (N x D numpy array) -- N: #data, D: #dimension of a data

    Returns:
        embedded matrix (N x D numpy array) -- N: #data, D: #dimension of a data in 2d plane
    """
    embedded = TSNE(n_components=2, verbose=1,
                    n_iter=3000).fit_transform(features)
    return embedded


def rotate_pt(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin in 2D plane.

    Args:
        origin: (oy, ox)
        point: (py,px)
        angle: angle in range of 0-360

    Returns:
        rotated point: (ry, rx)
    """
    rad = angle * math.pi / 180
    oy, ox = origin
    py, px = point
    qx = ox + math.cos(rad) * (px - ox) - math.sin(rad) * (py - oy)
    qy = oy + math.sin(rad) * (px - ox) + math.cos(rad) * (py - oy)
    return qy, qx


def drop_extension(fname):
    """
    Drop extension for the given filename 

    Args:
        fname (str): a name (or path) of the file

    Returns:
        fid (str): string without the extension 

    Examples:
        >>> drop_image_extension("jaemin.png")
        >>> jaemin
    """
    fid, _ = os.path.splitext(fname)
    return fid


def load_network(dir_path, trainable=False):
    """
    Load a network from a pair of a json and an h5 file

    Args:
        dir_path (str): path to the model files (1 json + 1 h5)
        trainable (boolean, optional): trainable parameters for keras networks

    Returns:
        network (keras network object): loaded network 

    Raises:
        AssertionError: one json file and one h5 file should exist under dir_path

    Examples:
        >>> load_network("./trained_models", trainable=True)
    """
    network_file = all_files_under(dir_path, extension=".json")
    weight_file = all_files_under(dir_path, extension=".h5")
    assert len(network_file) == 1 and len(weight_file) == 1
    with open(network_file[0], 'r') as f:
        network = model_from_json(f.read())
    network.load_weights(weight_file[0])
    network.trainable = trainable
    for l in network.layers:
        l.trainable = trainable
    return network


def keras_intermediate_output_func(network, list_input_layer_names, list_output_layer_names):
    """
    Return keras function for intermediate outputs

    Args:
        network (keras network object)
        list_input_layer_names (list of str): list of input layers in the network
        list_output_layer_names (list of str): list of output layers in the network

    Returns:
        func (keras function): a function that takes the inputs and generate outputs 

    Examples:
        >>> keras_intermediate_output_func(network, ["Input1", "Input2"], ["Output1", "Output2"])
    """
    list_inputs, list_outputs = [], []
    for input_layer_name in list_input_layer_names:
        list_inputs.append(network.get_layer(input_layer_name).input)
    for output_layer_name in list_output_layer_names:
        list_outputs.append(network.get_layer(output_layer_name).output)
    func = K.function(list_inputs, list_outputs)
    return func


def makedirs(dir_path):
    """
    Make directories if not exists

    Args:
        dir_path (str): path to the directory

    Returns:
        None 

    Examples:
        >>> makedirs("tmp")
    """
    if not os.path.isdir(dir_path):
        os.makedirs(dir_path)


def all_files_under(dir_path, extension=None, append_path=True, sort=True):
    """
    List all files under a given path

    Args:
        dir_path (str): path to the directory

    Returns:
        filenames (list of str): filenames if append_path=False or filepaths if append_path=True

    Examples:
        >>> all_files_under("tmp")
        >>> all_files_under("tmp", extension=".jpg")
        >>> all_files_under("tmp", extension=".jpg", append_path=False)
    """
    if append_path:
        if extension is None:
            filenames = [os.path.join(dir_path, fname)
                         for fname in os.listdir(dir_path)]
        else:
            filenames = [os.path.join(dir_path, fname) for fname in os.listdir(
                dir_path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname)
                         for fname in os.listdir(dir_path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(
                dir_path) if fname.endswith(extension)]

    if sort:
        filenames = sorted(filenames)

    return filenames


def load_imgs(path):
    """
    Load images from a list of filepath.

    Args:
        filepaths (list or array of str): a list of filepaths for images 

    Returns:
        numpy array for images (n_files, d1, d2, ...) 

    Raises:
        ValueError: path includes non-image file, images with irregular shape
    """
    filepaths = all_files_under(path) if isinstance(path, str) else path
    img_shape = image_shape(filepaths[0])
    images_arr = np.zeros((len(filepaths),) + img_shape, dtype=np.float32)
    for file_index in range(len(filepaths)):
        images_arr[file_index] = np.array(Image.open(filepaths[file_index]))
    return images_arr


def normalize(data, n_bit=8):
    """
    Normalize numpy array by dividing by 2**n_bit

    Args:
        data (numpy array)
        n_bit (int)

    Returns:
        normalized data (numpy array)
    """
    return 1.*data / (2 ** n_bit)


def standardize(data, axis=-1):
    """
    Standardize numpy array by subtracting mean and dividing by std along the given axis

    Args:
        data (numpy array) 

    Returns:
        standardized data (numpy array)

    Examples:
        >>> a=np.array([[0,1,2,3],[4,8,9,10]])
        >>> utils.standardize(a)
        array([[-1.34164079, -0.4472136 ,  0.4472136 ,  1.34164079],
               [-1.6464639 ,  0.10976426,  0.5488213 ,  0.98787834]])
    """
    means = np.mean(data, axis=axis, keepdims=True)
    stds = np.std(data, axis=axis, keepdims=True)
    return (data - means) / stds


def image_shape(filepath):
    """
    Get shape of a image file

    Args:
        filepath (str): path to the image file

    Returns:
        tuple of int
    """
    return np.array(Image.open(filepath)).shape


def random_perturbation(img, rescale_factor=1.2):
    """
    Perturb image (color, contrast, brightness, sharpness).

    Args:
        img (numpy array): image array
        rescale_factor (float): rescale factor for image enhancement (color, contrast, brightness, sharpness)

    Returns:
        tuple of int for shape
    """
    im = Image.fromarray(img.astype(np.uint8))
    en_color = ImageEnhance.Color(im)
    im = en_color.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_cont = ImageEnhance.Contrast(im)
    im = en_cont.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_bright = ImageEnhance.Brightness(im)
    im = en_bright.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_sharpness = ImageEnhance.Sharpness(im)
    im = en_sharpness.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    return np.asarray(im)


def float2class(float_vals, min_val, max_val):
    """
    Round and clip float values to convert to categorical classes.

    Args:
        float_vals (list or array of floats): array of float values

    Examples:
        >>> float2class([0.1, 0.4, 0.6], 0, 1)
        [0, 0, 1]
    """
    return np.clip(np.round(float_vals), min_val, max_val)


def binary_stats(y_true, y_pred):
    """
    Return dictionary of statistics for binary classes

    Args:
        y_true (array-like): true class labels 
        y_pred (array-like): predicted class labels

    Returns:
        stats (dict): dict for statistics (confusion matrix, specificity, sensitivity, AUROC) 

    Note:
        if no positive cases exist in y_true, stats contains only specificity 
    """
    y_true = np.array(y_true)
    y_pred = np.array(y_pred)
    stats = {}
    if len(y_true[y_true == 1]) > 0:
        cm = confusion_matrix(y_true, float2class(y_pred, 0, 1))
        stats["confusion_matrix"] = cm
        stats["specificity"] = 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
        stats["sensitivity"] = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
        stats["AUROC"] = roc_auc_score(y_true, y_pred)
    else:
        stats["specificity"] = 1.*len(y_true[y_true == y_pred]) / len(y_true)
    return stats


def categorical_stats(y_true, y_pred, return_weighted_kappa=True, return_confusion_matrix=True):
    """
    Return dictionary of statistics for categorical classes

    Args:
        y_true (array-like): true class labels 
        y_pred (array-like): predicted class labels
        weighted_kappa (bool): whether to return quadratic weighted kappa

    Returns:
        stats (dict): dict for statistics (confusion matrix, specificity, sensitivity, AUROC) 

    Note:
        if no positive cases exist in y_true, stats contains only specificity 
    """
    stats = {}
    cm = confusion_matrix(y_true, y_pred)
    if return_confusion_matrix:
        stats["confusion_matrix"] = cm
    stats["accuracy"] = 1.*np.trace(cm) / np.sum(cm)
    if return_weighted_kappa:
        stats["weighted_kappa"] = cohen_kappa_score(
            y_true, y_pred, weights="quadratic")
    return stats


def save_imgs(imgs, dir_path):
    """
    Save images to a designated path

    Args:
        imgs (3d or 4d numpy array)
        dir_path (str): path to which images are saved

    Examples:
        >>> save_imgs(imgs, "results")
        1.png, 2.png, ..., N.png are saved
    """
    makedirs(dir_path)
    n_data = imgs.shape[0]
    for i in range(n_data):
        Image.fromarray((imgs[i, ...]).astype(np.uint8)).save(
            os.path.join(dir_path, "imgs_{}.png".format(i + 1)))


def run_once(func):

    def wrapper(*args, **kwargs):
        if not kwargs["phase"] in wrapper.phase_run:
            wrapper.phase_run.append(kwargs["phase"])
            return func(*args, **kwargs)

    wrapper.phase_run = []
    return wrapper


@run_once
def fundus_classification_check_data(data, dir_path, phase):
    filenames, imgs, labels = data
    save_imgs(imgs * 255, dir_path)


@run_once
def vessel_segmentation_check_data(data, dir_path, phase):
    imgs, masks = data
    save_imgs(imgs * 30 + 100, os.path.join(dir_path, "imgs"))
    save_imgs(masks * 255, os.path.join(dir_path, "masks"))


@run_once
def oct_classification_check_data(data, dir_path, phase):
    filenames, imgs, labels = data
    save_imgs(imgs * 255, dir_path)


@run_once
def oct_localization_check_data(data, dir_path):
    filenames, imgs, coords = data
    imgs = np.copy(imgs)
    coords = np.copy(coords)
    n, h, w = imgs.shape
    imgs = np.expand_dims(imgs, axis=-1)
    imgs = np.repeat(imgs, 3, axis=-1)
    coords[:, 1] *= w
#     coords[:, 3] *= w
    coords[:, 0] *= h
#     coords[:, 2] *= h
    for index in range(n):
        imgs[index, int(coords[index, 0]) - 3:int(coords[index, 0]) + 3,
             int(coords[index, 1]) - 3:int(coords[index, 1]) + 3, 0] = 1
#         imgs[index, int(coords[index, 2]) - 3:int(coords[index, 2]) + 3, int(coords[index, 3]) - 3:int(coords[index, 3]) + 3, 0] = 1

    save_imgs(imgs * 255, dir_path)


@run_once
def nicking_classification_check_data(data, dir_path, phase):
    filenames, imgs, labels = data
#     save_imgs(imgs * 50, dir_path)
    save_imgs(imgs * 255, dir_path)


def balanced_class_weights(labels):
    """
    Compute balanced class weights for individual samples given classification labels.

    Args:
        labels (array-like)

    Return:
        class weights, numpy array for individual sampling weights

    Examples:
        >>> balanced_class_weights([0,0,0,0,1,1,1,2])
        (array([0.15789474, 0.21052632, 0.63157895]), array([0.08333333, 0.08333333, 0.08333333, 0.08333333, 0.11111111,
       0.11111111, 0.11111111, 0.33333333]))

    """
    labels = np.array(labels)
    cw = class_weight.compute_class_weight(
        "balanced", np.unique(labels), labels)
    cw /= cw.sum()
    p = np.zeros(len(labels))
    for i, weight in enumerate(cw):
        p[labels == i] = weight
    p /= p.sum()
    return cw, p


def uniform_sample_weight(n_element):
    """
    Return uniform sample weight.

    Args:
        n_element (int)

    Return:
        numpy array for uniform sample weight

    Examples:
        >>> uniform_sample_weight(5)
        array([0.2, 0.2, 0.2, 0.2, 0.2])

    """
    return np.array([1. / n_element] * n_element)


def shell_command(command):
    """
    Execute a shell command in shell

    Args:
        command (str)

    Raises:
        AssertionError: command should be str

    """
    assert isinstance(command, str)
    pipes = Popen(command.split(), stdout=PIPE, stderr=PIPE)
    std_out, std_err = pipes.communicate()
    return std_out, std_err


def load_img_from_filepath(filepath, normalization=None, denoise_func=None, augment_func=None):
    """
    Load an image designated by a path

    Args:
        filepath (str): full path of an image file
        normalization (str): normalization method for images
        denoise_func (function object): de-noising function
        augment_func (function object): augmentation function


    """
    resize_shape = (32,32)
    img = np.array(Image.open(filepath))
    img = imresize(img, resize_shape, "bicubic")
    if denoise_func:
        img = denoise_func(img)
    if augment_func:
        img = augment_func(img)
    if normalization == "normalize":
        img = img / 255.0
    elif normalization == "z_score":
        means = np.mean(img, axis=(0,1), keepdims=True)
        stds = np.std(img, axis=(0,1), keepdims=True)
        img = (img - means) / (stds + 1)
    return img

def generate_mask_from_bbox_coords(coords, img_shape, unit_gaussian_blur_normalized):
    
    mask = np.zeros(img_shape)
    for coord in coords:
        topleft_w, topleft_h, bottomright_w, bottomright_h = coord
        bottomright_w = int(bottomright_w)
        bottomright_h = int(bottomright_h)
        topleft_w = int(topleft_w)
        topleft_h = int(topleft_h)
        if bottomright_w > 0 and bottomright_h > 0:
            topleft_w = max(0, topleft_w)
            topleft_h = max(0, topleft_h)
            width = bottomright_w - topleft_w
            height = bottomright_h - topleft_h
            patch = cv2.resize(unit_gaussian_blur_normalized, (width, height), interpolation=cv2.INTER_LINEAR)
            for i in range(patch.shape[0]):
                for j in range(patch.shape[1]):
                    mask[topleft_h+i, topleft_w+j] = max(patch[i,j], mask[topleft_h+i, topleft_w+j])
            # mask[topleft_h:bottomright_h, topleft_w:bottomright_w] = patch
    
    # return np.expand_dims(mask , axis=-1)
    return mask


def load_img_mask_from_filepath_coords_ocr_localization(filepath, bbox_coords, normalization=None, denoise_func=None, augment_func=None):
    """
    Load an image designated by a path

    Args:
        filepath (str): full path of an image file
        bbox_coords (array): array of list
        normalization (str): normalization method for images
        denoise_func (function object): de-noising function
        augment_func (function object): augmentation function
    """
    img = np.array(Image.open(filepath))
    # make width and height divisible by 2
    if img.shape[0] % 2==1:
        img = img[:-1,...]
    if img.shape[1] % 2==1:
        img = img[:,:-1,...]
    
    if denoise_func:
        img = denoise_func(img)
    if augment_func:
        img, bbox_coords = augment_func(img, bbox_coords)
    if normalization == "normalize":
        img = img / 255.0
    elif normalization == "z_score":
        means = np.mean(img, axis=(0,1), keepdims=True)
        stds = np.std(img, axis=(0,1), keepdims=True)
        img = (img - means) / (stds + 1)
    elif normalization == "zero_mean": 
        img = 1.*cv2.addWeighted (img, 4, cv2.GaussianBlur(img , (0, 0) , 32) , -4 , 128) / 128
    
    # generate mask
    mask = generate_mask_from_bbox_coords(bbox_coords, img.shape[:2])
    
    # crop to uniform size
    target_img_size = (448, 512) # h, w
    cropped_img = np.zeros(target_img_size+(3,))
    cropped_mask = np.zeros(target_img_size + (1,))
    h, w, _ = img.shape
    return_h, return_w, _ = cropped_img.shape
    if h <= return_h:
        h_start = random.randint(0, return_h-h)
        if w <= return_w:
            w_start = random.randint(0, return_w-w)
            cropped_img[h_start:h_start+h, w_start:w_start+w, ...] = img
            cropped_mask[h_start:h_start+h, w_start:w_start+w, ...] = mask
        else:
            w_start = random.randint(0, w-return_w)
            cropped_img[h_start:h_start+h, :, :] = img[:,w_start:w_start+return_w,...]
            cropped_mask[h_start:h_start+h, :, ...] = mask[:,w_start:w_start+return_w,...]
    else:
        h_start = random.randint(0, h-return_h)
        if w <= return_w:
            w_start = random.randint(0, return_w-w)
            cropped_img[:, w_start:w_start+w, ...] = img[h_start:h_start+return_h,:,:]
            cropped_mask[:, w_start:w_start+w, ...] = mask[h_start:h_start+return_h,:,...]
        else:
            w_start = random.randint(0, w-return_w)
            cropped_img = img[h_start:h_start+return_h ,w_start:w_start+return_w, ...]
            cropped_mask = mask[h_start:h_start+return_h,w_start:w_start+return_w, ...]

    return cropped_img, cropped_mask

def load_img_mask_from_filepaths(filepaths, normalization=None, denoise_func=None, augment_func=None):
    """
    Load an image and a mask designated by a list of paths

    Args:
        filepaths (str): list of full path of an image file and a segmentation file
        normalization (str): normalization method
        denoise_func (function object): de-noising function
        augment_func (function object): augmentation function

    Returns:
        an image and a mask (numpy array)
    """
    img_filepath, mask_filepath = filepaths
    img = np.array(Image.open(img_filepath))
    mask = np.array(Image.open(mask_filepath))
    if denoise_func:
        img = denoise_func(img)
    if augment_func:
        img, mask = augment_func(img, mask)
    if normalization == "normalize":
        img = img / 255.0
    elif normalization == "z_score":
        means = np.zeros(3)
        stds = np.array([255.0, 255.0, 255.0])
        for i in range(3):
            if len(img[..., i][img[..., i] > 10]) > 1:
                means[i] = np.mean(img[..., i][img[..., i] > 10])
                std_val = np.std(img[..., i][img[..., i] > 10])
                if std_val > 0:
                    stds[i] = std_val
        img = (img - means) / stds

    if np.max(mask) != 1:
        if np.max(mask) == 255:
            mask[mask == 255] = 1
        else:
            raise ValueError("Mask should have intensity values of 255 or 1")

    return img, mask


def load_img_coords_from_filepath(filepath, coords, normalization=None, denoise_func=None, augment_func=None):
    """
    Load an image designated by a path and a coordinate

    Args:
        filepath (str): full path of an image file
        coords (numpy): 1d array for coords
        normalization (str): normalization method for images
        denoise_func (function object): de-noising function
        augment_func (function object): augmentation function


    """
    img = np.array(Image.open(filepath))
    if denoise_func:
        img = denoise_func(img)
    if augment_func:
        img, coords = augment_func(img, coords)
    if normalization == "normalize":
        img = img / 255.0
    elif normalization == "z_score":
        means = np.zeros(3)
        stds = np.array([255.0, 255.0, 255.0])
        for i in range(3):
            if len(img[..., i][img[..., i] > 10]) > 1:
                means[i] = np.mean(img[..., i][img[..., i] > 10])
                std_val = np.std(img[..., i][img[..., i] > 10])
                if std_val > 0:
                    stds[i] = std_val
        img = (img - means) / stds

    return img, coords


def resize_img(img, h_target, w_target, method="bicubic"):
    """
    Wrapper for scipy imresize.
    """
    return imresize(img, (h_target, w_target), method)


def pad_imgs(imgs, img_size, is_batch=True):
    """
    Pad a batch of images with the shape of (N, h, w, d) or (N, h, w) if is_batch=True,
        an image with the shape of (h, w, d) or (h, w), otherwise

    Args:
        imgs (numpy array): a batch of images with the shape of (N, h, w, d) or (N, h, w)
        img_size (tuple): (target_h, target_w)

    Returns:
        padded images (numpy array)

    """
    if is_batch:
        assert len(imgs.shape) == 3 or len(imgs.shape) == 4
        img_h, img_w = imgs.shape[1], imgs.shape[2]
        target_h, target_w = img_size[0], img_size[1]
        if len(imgs.shape) == 4:
            d = imgs.shape[3]
            padded = np.zeros((imgs.shape[0], target_h, target_w, d))
        elif len(imgs.shape) == 3:
            padded = np.zeros((imgs.shape[0], img_size[0], img_size[1]))
        padded[:, (target_h - img_h) // 2:(target_h - img_h) // 2 + img_h,
               (target_w - img_w) // 2:(target_w - img_w) // 2 + img_w, ...] = imgs
    else:
        assert len(imgs.shape) == 2 or len(imgs.shape) == 3
        img_h, img_w = imgs.shape[0], imgs.shape[1]
        target_h, target_w = img_size[0], img_size[1]
        if len(imgs.shape) == 3:
            padded = np.zeros((target_h, target_w, imgs.shape[2]))
        elif len(imgs.shape) == 2:
            padded = np.zeros((img_size[0], img_size[1]))
        padded[(target_h - img_h) // 2:(target_h - img_h) // 2 + img_h,
               (target_w - img_w) // 2:(target_w - img_w) // 2 + img_w, ...] = imgs
    return padded


def available_gpu(idle_time=300):
    """
    Check gpu availability by nvidia-smi.
    Judge a gpu is idle when no memory is consumed for idle_time seconds.

    Args:
        idle_time (int): if a gpu is idle more than idel_time seconds, it is judged as idle.

    Returns:
        Available gpu index (list of str)

    """
    list_idle = []
    for _ in range(2):
        std_out, _ = shell_command("nvidia-smi")
        lines = std_out.decode("utf-8").split("\n")
        existing, occupied = [], []
        occupied_line = False
        for index, line in enumerate(lines):
            if "Processes" in line:
                occupied_line = True
            else:
                if "MiB" in line:
                    if occupied_line:
                        occupied.append(line.split()[1])
                    else:
                        existing.append(lines[index - 1].split()[1])
        idle = set(existing) - set(occupied)
        if len(idle) == 0:
            return None
        else:
            list_idle.append(idle)
            if len(list_idle) == 2:
                return list(list_idle[0].intersection(list_idle[1]))
            # wait for idle_time seconds and check if the gpu is still idle
            time.sleep(idle_time)

########################################################
########################################################
########### TASK SPECIFIC UTILITY FUNCTIONS ############
########################################################
########################################################

def str2label(string):
    try:
        index = list_string.index(string)
    except:
        raise ValueError("string {} is not recognized".format(string))
    return index

def merge_dr_label_open_data(list_df):
    """
    Merge DR labels for multiple open data

    Args:
        list_df: a list of dataframes (pandas)

    Return:
        df_concat: a concatenated dataframe (pandas)

    Raises:
        AssertionError: 1. list_df should have at least 1 element
                        2. duplicate fid(s) exist
    """
    assert len(list_df) > 0
    list_df_processed = []
    for df in list_df:
        if "id_code" in df.columns:  # APTOS
            df["id"] = df["id_code"]
            df["label"] = df["diagnosis"]
        elif "Image name" in df.columns:  # IDRiD
            df["id"] = df["Image name"]
            df["label"] = df["Retinopathy grade"]
        elif "image" in df.columns:  # kaggle
            df["id"] = df["image"]
            df["label"] = df["level"]
        elif "image_id" in df.columns:  # messidor
            df["id"] = df["image_id"].map(lambda x: drop_extension(x))
            df["label"] = df["adjudicated_dr_grade"]
        df.dropna(subset=["label"], inplace=True)
        list_df_processed.append(df[["id", "label"]])
    if len(list_df_processed) > 1:
        df_concat = pd.concat(list_df_processed, ignore_index=True)
    else:
        df_concat = list_df_processed[0]
    assert len(np.unique(df_concat["id"])) == len(
        df_concat)  # no duplicate in fid
    df_concat.set_index('id', inplace=True)
    return df_concat


def assign_labels_oct(fpaths):
    """
    Args:
        fpaths (list of str): file paths of the input images

    Returns:
        labels (2d numpy arrray): one-hot vector labels
    """
    n_classes = 4
    labels = np.zeros((len(fpaths), n_classes))
    for index, fpath in enumerate(fpaths):
        fname = os.path.basename(fpath)
        if "NORMAL" in fname:
            labels[index, 0] = 1
        elif "DRUSEN" in fname:
            labels[index, 1] = 1
        elif "CNV" in fname:
            labels[index, 2] = 1
        elif "DME" in fname:
            labels[index, 3] = 1
        else:
            raise ValueError("fname {} does not specify class".format(fname))

    return labels


def fundus_denoise(img):
    """
    Remove low values & erase time tag

    Args: 
        img (numpy array)

    Returns:
        denoised image in numpy array

    """
    img[img < 10] = 0  # remove low values
    img[:80, ...] = 0  # remove time tag
    return img


def standardize_fundus(data):
    """
    Standardize numpy array by subtracting mean and dividing by std.
    mean and std are computed for pixels with intensity above 10.

    Args:
        data (numpy array) 

    Returns:
        standardized data (numpy array)
    """
    means = np.zeros(3)
    stds = np.array([255.0, 255.0, 255.0])
    for i in range(3):
        if len(data[..., i][data[..., i] > 10]) > 1:
            means[i] = np.mean(data[..., i][data[..., i] > 10])
            std_val = np.std(data[..., i][data[..., i] > 10])
            if std_val > 0:
                stds[i] = std_val
    return (data - means) / stds


def fundus_classification_strong_augment(img, scale_factor=1.8, prob_aug=0.8):
    """
    Strong augmentation for training with pseudo-labeled data.

    Args:
        img (numpy array)
        scale_factor (float): scale factor for image augmentation (color, contrast, brightness, sharpness)
        prob_aug (float): probability for each albumentation augmentation

    Returns:
        numpy array for an augmented image in a shape of (h, w, d)
    """
    return fundus_classification_augment(img, scale_factor, prob_aug)


def fundus_classification_weak_augment(img, enhance_coeff=1.15):
    """
    Weak augmentation for pseudo-labeling (rotation 0, 90, 180, 270, and color, contrast, brightness, sharpness).

    Args:
        img (numpy array)
        enhance_coeff (float) 

    Returns:
        numpy array in a shape of (8, h, w, d)
    """
    im = Image.fromarray(img.astype(np.uint8))
    en_color = ImageEnhance.Color(im)
    img_color = np.array(en_color.enhance(enhance_coeff))
    en_cont = ImageEnhance.Contrast(im)
    img_cont = np.array(en_cont.enhance(enhance_coeff))
    en_bright = ImageEnhance.Brightness(im)
    img_bright = np.array(en_bright.enhance(enhance_coeff))
    en_sharpness = ImageEnhance.Sharpness(im)
    img_sharpness = np.array(en_sharpness.enhance(enhance_coeff))

    list_weak_augmented = [img, np.rot90(img,1), np.rot90(img,2), np.rot90(img,3), img_color, img_cont, img_bright, img_sharpness]
    concated_weak_aumented = np.array(list_weak_augmented)
    return concated_weak_aumented

def ocr_classification(img, scale_factor=1.2, prob_aug=0.1):
    h, w, d = img.shape
    aug_img = random_perturbation(img, 1.5)
    shift_y, shift_x = np.array(aug_img.shape[:2]) / 2.
    shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
    shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
    r_angle = random.randint(-15, 15)
    scale = random.uniform(1. / scale_factor, scale_factor)
    shear = random.uniform(-15, 15)
    translation_ratio = 1. / 8    
    h_trans_margin = translation_ratio * img.shape[0]
    w_trans_margin = translation_ratio * img.shape[1]
    h_translation = int(random.uniform(-h_trans_margin, h_trans_margin))
    w_translation = int(random.uniform(-w_trans_margin, w_trans_margin))
    tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(r_angle))
    aug_img = warp(aug_img, (shift_to_ori + (tform + shift_to_img_center)
                             ).inverse, output_shape=(aug_img.shape[0], aug_img.shape[1]))
    aug_img = (aug_img * 255).astype(np.uint8)

    # flip color
    if random.uniform(0, 1) < 0.5:
        aug_img = 255 - aug_img

    # CoarseDropout
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.CoarseDropout(max_holes=5, max_height=aug_img.shape[0] // 16, max_width=aug_img.shape[0] //
                                           16, min_holes=None, min_height=None, min_width=None, p=1)(image=aug_img)["image"].astype(np.uint8)

    # blur
    blur_limit = 3
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.Blur(blur_limit=blur_limit, p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # GaussNoise
    GaussNoise_val_limit = 80
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.GaussNoise(var_limit=GaussNoise_val_limit, p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # ISONoise
    ISONoise_color_shift_max = 0.1
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.ISONoise(color_shift=(0.01, ISONoise_color_shift_max), p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # JpegCompression
    JpegCompression_quality_lower = 60
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.JpegCompression(quality_lower=JpegCompression_quality_lower, quality_upper=100, p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # RandomGamma
    RandomGamma_min = 50
    RandomGamma_max = 150
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.RandomGamma(gamma_limit=(
            RandomGamma_min, RandomGamma_max), p=1)(image=aug_img)["image"].astype(np.uint8)

    return aug_img

def ocr_localization(img, bbox_coords, scale_factor=1.2, prob_aug=0.1):
    h, w, d = img.shape
    aug_img = random_perturbation(img, 1.5)
        

    # flip color
    if random.uniform(0, 1) < 0.5:
        aug_img = 255 - aug_img

    # rotate
    rot_angle_limit = 30
    rotater = transforms.Rotate(limit = rot_angle_limit, interpolation=cv2.INTER_CUBIC, p=1)
    result = rotater(image=aug_img, bboxes=bbox_coords)
    aug_img = result["image"].astype(np.uint8)
    bbox_coords = [bbox_coord for bbox_coord in result["bboxes"]]

    # resize
    scale_factor = 1.2
    scale_x = random.uniform(1. / scale_factor, scale_factor)
    scale_y = random.uniform(1. / scale_factor, scale_factor)
    h,w,_ = aug_img.shape
    target_height = int(scale_y * h)
    target_width = int(scale_x * w)
    resizer = transforms.Resize(height=target_height, width=target_width, interpolation=cv2.INTER_CUBIC, p=1)
    result = resizer(image=aug_img, bboxes=bbox_coords)
    aug_img = result["image"].astype(np.uint8)
    bbox_coords = [bbox_coord for bbox_coord in result["bboxes"]]

    # translation
    h, w, _ = aug_img.shape
    translation_x = int(random.uniform(-h//10, h//10))
    translation_y = int(random.uniform(-w//10, w//10))
    aug_img_copy = np.copy(aug_img)
    aug_img = np.zeros(aug_img_copy.shape, dtype=np.uint8)
    if translation_x < 0:
        if translation_y < 0:
            aug_img[:h + translation_y, :w + translation_x, ...] = aug_img_copy[-translation_y:, -translation_x:, ...]
        else:
            aug_img[translation_y:, :w + translation_x, ...] = aug_img_copy[:h-translation_y,  -translation_x:, ...]
    else:
        if translation_y < 0:
            aug_img[:h + translation_y, translation_x:, :] = aug_img_copy[-translation_y:, :w-translation_x, :]
        else:
            aug_img[translation_y:, translation_x:, :] = aug_img_copy[:h-translation_y, :w-translation_x, :]
    bbox_coords_tmp = []
    for bbox_coord in bbox_coords:
        topleft_w, topleft_h, bottomright_w, bottomright_h = bbox_coord
        bbox_coords_tmp.append((topleft_w+translation_x, topleft_h+translation_y, bottomright_w+translation_x, bottomright_h+translation_y))
    bbox_coords = bbox_coords_tmp

    # CoarseDropout
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.CoarseDropout(max_holes=5, max_height=aug_img.shape[0] // 16, max_width=aug_img.shape[0] //
                                           16, min_holes=None, min_height=None, min_width=None, p=1)(image=aug_img)["image"].astype(np.uint8)

    # blur
    blur_limit = 3
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.Blur(blur_limit=blur_limit, p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # GaussNoise
    GaussNoise_val_limit = 80
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.GaussNoise(var_limit=GaussNoise_val_limit, p=1)(
            image=aug_img)["image"].astype(np.uint8)


    # ISONoise
    ISONoise_color_shift_max = 0.1
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.ISONoise(color_shift=(0.01, ISONoise_color_shift_max), p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # JpegCompression
    JpegCompression_quality_lower = 60
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.JpegCompression(quality_lower=JpegCompression_quality_lower, quality_upper=100, p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # RandomGamma
    RandomGamma_min = 50
    RandomGamma_max = 150
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.RandomGamma(gamma_limit=(
            RandomGamma_min, RandomGamma_max), p=1)(image=aug_img)["image"].astype(np.uint8)

    return aug_img, bbox_coords


def fundus_classification_augment(img, scale_factor=1.3, prob_aug=0.1):
    """
    Augment a macula-centered image (image perturbation, affine transform, various albumentation transform).

    Args:
        img (numpy array)
        scale_factor (float): scale factor for image augmentation (color, contrast, brightness, sharpness)
        prob_aug (float): probability for each albumentation augmentation

    Returns:
        numpy array for an augmented image in a shape of (h, w, d)
    """
    h, w, d = img.shape

    aug_img = random_perturbation(img, 1.5)
    shift_y, shift_x = np.array(aug_img.shape[:2]) / 2.
    shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
    shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
    r_angle = random.randint(0, 359)
    scale = random.uniform(1. / scale_factor, scale_factor)
    shear = random.uniform(-10, 10)
    tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(r_angle), shear=np.deg2rad(shear))
    aug_img = warp(aug_img, (shift_to_ori + (tform + shift_to_img_center)
                             ).inverse, output_shape=(aug_img.shape[0], aug_img.shape[1]))
    aug_img = (aug_img * 255).astype(np.uint8)

    # GridDistortion
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.GridDistortion(num_steps=5, distort_limit=0.5, value=None,
                                            mask_value=None, always_apply=True, p=1)(image=aug_img)["image"].astype(np.uint8)

    # CoarseDropout
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.CoarseDropout(max_holes=5, max_height=aug_img.shape[0] // 10, max_width=aug_img.shape[0] //
                                           10, min_holes=None, min_height=None, min_width=None, p=1)(image=aug_img)["image"].astype(np.uint8)

    # blur
    blur_limit = 10
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.Blur(blur_limit=blur_limit, p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # GaussNoise
    GaussNoise_val_limit = 80
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.GaussNoise(var_limit=GaussNoise_val_limit, p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # ISONoise
    ISONoise_color_shift_max = 0.1
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.ISONoise(color_shift=(0.01, ISONoise_color_shift_max), p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # JpegCompression
    JpegCompression_quality_lower = 60
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.JpegCompression(quality_lower=JpegCompression_quality_lower, quality_upper=100, p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # RGBShift
    RGBShift_val = 40
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.RGBShift(r_shift_limit=RGBShift_val, g_shift_limit=RGBShift_val,
                                      b_shift_limit=RGBShift_val, p=1)(image=aug_img)["image"].astype(np.uint8)

    # RandomGamma
    RandomGamma_min = 50
    RandomGamma_max = 150
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.RandomGamma(gamma_limit=(
            RandomGamma_min, RandomGamma_max), p=1)(image=aug_img)["image"].astype(np.uint8)

    # RandomSunFlare
    RandomSunFlare_radius = int(random.uniform(256, 512))
    RandomSunFlare_draw = random.uniform(0, 1)
    if RandomSunFlare_draw < 0.25:
        flare_roi = (0, 0, 0.25, 0.25)
    elif RandomSunFlare_draw < 0.5:
        flare_roi = (0.75, 0, 1, 0.25)
    elif RandomSunFlare_draw < 0.75:
        flare_roi = (0, 0.75, 0.25, 1)
    else:
        flare_roi = (0.75, 0.75, 1, 1)
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.RandomSunFlare(flare_roi=flare_roi, src_radius=RandomSunFlare_radius,
                                            num_flare_circles_lower=0, num_flare_circles_upper=1, p=1)(image=aug_img)["image"].astype(np.uint8)

    # ElasticTransform
    ElasticTransform_alpha = int(random.uniform(512, 2048))
    ElasticTransform_sigma = ElasticTransform_alpha * \
        random.uniform(0.03, 0.05)
    if random.uniform(0, 1) < prob_aug:
        aug_img = transforms.ElasticTransform(alpha=ElasticTransform_alpha, sigma=ElasticTransform_sigma, alpha_affine=0, p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # downsample
    if random.uniform(0, 1) < prob_aug:
        downsize_factor = random.uniform(1, 2)
        aug_img = imresize(aug_img, (int(h / downsize_factor),
                                     int(w / downsize_factor)), interp="nearest")
        aug_img = imresize(aug_img, (h, w), interp="nearest").astype(np.uint8)

    return aug_img


def fundus_classification_augment_v0(img, scale_factor=1.15, rotation_from_angle=0, rotation_to_angle=360):
    """
    Color, contrast, brightness perturbation, horizontal flip, affine transform (rotation, resize)

    Args: 
        img (numpy array)
        scale_factor (float): image is scaled from 1. / scale_factor to scale_factor
        rotation_from_angle (float): angle from which an image is rotated
        rotation_to_angle (float): angle to which an image is rotated

    Returns:
        augmented image in numpy array

    """
    img = np.copy(img)
    # random color, contrast, brightness perturbation
    img = random_perturbation(img)
    if random.getrandbits(1):
        img = img[:, ::-1, :]  # random horizontal flip
    # affine transform (scale, rotation)
    shift_y, shift_x = np.array(img.shape[:2]) / 2.
    shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
    shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
    r_angle = np.random.uniform(rotation_from_angle, rotation_to_angle)
    scale = random.uniform(1. / scale_factor, scale_factor)
    tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(r_angle))
    img = warp(img, (shift_to_ori + (tform + shift_to_img_center)
                     ).inverse, output_shape=(img.shape[0], img.shape[1]))
    img *= 255  # revert to 0-255 range
    return img


def nicking_classification_augment(img, scale_factor=1.15, rotation_from_angle=0, rotation_to_angle=360, translation_ratio=0.1):
    """
    Color, contrast, brightness perturbation, horizontal flip, affine transform (rotation, resize, translation)
    and Resize the img to 128x128

    Args: 
        img (numpy array)
        scale_factor (float): image is scaled from 1. / scale_factor to scale_factor
        rotation_from_angle (float): angle from which an image is rotated
        rotation_to_angle (float): angle to which an image is rotated

    Returns:
        augmented image in numpy array

    """
    img = np.copy(img)
    # random color, contrast, brightness perturbation
    img = random_perturbation(img)
    if random.getrandbits(1):
        img = img[:, ::-1, :]  # random horizontal flip
    # affine transform (scale, rotation)
    shift_y, shift_x = np.array(img.shape[:2]) / 2.
    shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
    shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
    r_angle = np.random.uniform(rotation_from_angle, rotation_to_angle)
    scale = random.uniform(1. / scale_factor, scale_factor)
    h_trans_margin = translation_ratio * img.shape[0]
    w_trans_margin = translation_ratio * img.shape[1]
    h_translation = int(random.uniform(-h_trans_margin, h_trans_margin))
    w_translation = int(random.uniform(-w_trans_margin, w_trans_margin))
    tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(
        r_angle), translation=(w_translation, h_translation))
    img = warp(img, (shift_to_ori + (tform + shift_to_img_center)
                     ).inverse, output_shape=(img.shape[0], img.shape[1]))
    img *= 255  # revert to 0-255 range
    img = imresize(img, (128, 128), 'bicubic')  # resize img as size varies
    return img


def oct_classification_augment(img, scale_factor=1.15, rotation_from_angle=0, rotation_to_angle=360, shear_degree=10):
    """
    Color, contrast, brightness perturbation, horizontal flip, affine transform (share, rotation, resize)

    Args: 
        img (numpy array)
        scale_factor (float): image is scaled from 1. / scale_factor to scale_factor
        rotation_from_angle (float): angle from which an image is rotated
        rotation_to_angle (float): angle to which an image is rotated

    Returns:
        augmented image in numpy array

    """
    img = np.copy(img)
    # random color, contrast, brightness perturbation
    img = random_perturbation(img)
    if random.getrandbits(1):
        img = img[:, ::-1]  # random horizontal flip
    # affine transform (shear, scale, rotation)
    shift_y, shift_x = np.array(img.shape[:2]) / 2.
    shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
    shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
    r_angle = np.random.uniform(rotation_from_angle, rotation_to_angle)
    scale = random.uniform(1. / scale_factor, scale_factor)
    shear = random.uniform(-shear_degree, shear_degree)
    tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(
        r_angle), shear=np.deg2rad(shear))
    img = warp(img, (shift_to_ori + (tform + shift_to_img_center)
                     ).inverse, output_shape=(img.shape[0], img.shape[1]))
    # add noise
    noisy_img = random_noise(img)
    noisy_img *= 255
    return noisy_img


def oct_localization_augment(img, coords, scale_factor=1.3, rotation_from_angle=-15, rotation_to_angle=15):
    """
    Color, contrast, brightness perturbation, horizontal flip, affine transform
    CAVEATE: assume oct image size is 512x1024 (h,w)

    Args: 
        img (numpy array)
        coords: coords for (ly,lx,ry,rx)
        scale_factor (float): image is scaled from 1. / scale_factor to scale_factor
        rotation_from_angle (float): angle from which an image is rotated
        rotation_to_angle (float): angle to which an image is rotated

    Returns:
        augmented image in numpy array
        converted coords in numpy array (ly,lx,ry,rx)

    """
    h, w = 512, 1024
    img = np.copy(img)
    converted_coords = np.copy(coords)
    converted_coords[0] *= h
    converted_coords[2] *= h
    converted_coords[1] *= w
    converted_coords[3] *= w
    # random color, contrast, brightness perturbation
    img = random_perturbation(img)
    if random.getrandbits(1):
        img = img[:, ::-1]  # random horizontal flip
        converted_coords[1] = w - converted_coords[1]  # lx
        converted_coords[3] = w - converted_coords[3]  # rx

    # affine transform (scale, rotation)
    shift_y, shift_x = np.array(img.shape[:2]) / 2.
    shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
    shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
    r_angle = np.random.uniform(rotation_from_angle, rotation_to_angle)
    scale = random.uniform(1. / scale_factor, scale_factor)
    tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(r_angle))
    img = warp(img, (shift_to_ori + (tform + shift_to_img_center)
                     ).inverse, output_shape=(img.shape[0], img.shape[1]))

    # adjust coords
    converted_coords[0] = scale * (converted_coords[0] - shift_y)
    converted_coords[1] = scale * (converted_coords[1] - shift_x)
    converted_coords[2] = scale * (converted_coords[2] - shift_y)
    converted_coords[3] = scale * (converted_coords[3] - shift_x)
    converted_coords[:2] = rotate_pt(
        (0, 0), (converted_coords[0], converted_coords[1]), r_angle)
    converted_coords[2:] = rotate_pt(
        (0, 0), (converted_coords[2], converted_coords[3]), r_angle)
    converted_coords[0] += shift_y
    converted_coords[1] += shift_x
    converted_coords[2] += shift_y
    converted_coords[3] += shift_x
    converted_coords[0] /= h
    converted_coords[2] /= h
    converted_coords[1] /= w
    converted_coords[3] /= w

    # add noise
    noisy_img = random_noise(img)
    noisy_img *= 255
    return noisy_img, converted_coords


def vessel_segmentation_augment(img, mask, scale_factor=1.3, rotation_from_angle=0, rotation_to_angle=360, shear_degree=10):
    """
    Horizontal flip, affine transform

    Args: 
        img (numpy array)
        vessel (numpy array)
        scale_factor (float): image is scaled from 1. / scale_factor to scale_factor
        rotation_from_angle (float): angle from which an image is rotated
        rotation_to_angle (float): angle to which an image is rotated

    Returns:
        augmented image in numpy array

    """
    img = np.copy(img)
    mask = np.copy(mask)
    if random.getrandbits(1):
        img = img[:, ::-1, :]  # random horizontal flip
        mask = mask[:, ::-1]
    # affine transform (scale, rotation)
    shift_y, shift_x = np.array(img.shape[:2]) / 2.
    shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
    shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
    r_angle = np.random.uniform(rotation_from_angle, rotation_to_angle)
    scale = random.uniform(1. / scale_factor, scale_factor)
    shear = random.uniform(-shear_degree, shear_degree)
    tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(
        r_angle), shear=np.deg2rad(shear))
    img = warp(img, (shift_to_ori + (tform + shift_to_img_center)
                     ).inverse, output_shape=(img.shape[0], img.shape[1]))
    img *= 255  # revert back to 8 bit image
    mask = warp(mask, (shift_to_ori + (tform + shift_to_img_center)
                       ).inverse, output_shape=(mask.shape[0], mask.shape[1]))
    mask = np.round(mask)
    return img, mask


def fundus_classification_processing_func_strong_aug_DeepDRiD(data):
    return load_img_from_filepath(data[0], "normalize", None, fundus_classification_strong_augment), data[1]

def fundus_classification_processing_func_weak_aug_DeepDRiD(data):
    return load_img_from_filepath(data[0], "normalize", None, fundus_classification_weak_augment)


def fundus_classification_processing_func_train_DeepDRiD(data):
    list_return=[]
    for index in range(0,len(data),2):
        list_return.append(load_img_from_filepath(data[index], "normalize", None, fundus_classification_augment))
        list_return.append(data[index+1])
    return tuple(list_return)


def fundus_classification_processing_func_val_DeepDRiD(data):
    return load_img_from_filepath(data[0], "normalize", None), data[1]

def ocr_classification_processing_func_train(data):
    return load_img_from_filepath(data[0], "z_score", None, ocr_classification), data[1]

def ocr_classification_processing_func_val(data):
    return load_img_from_filepath(data[0], "z_score", None, None), data[1]

def ocr_localization_processing_func_train(data):
    imgs, masks = load_img_mask_from_filepath_coords_ocr_localization(data[0], data[1], "zero_mean", None, ocr_localization)
    return imgs, masks

def ocr_localization_processing_func_val(data):
    imgs, masks = load_img_mask_from_filepath_coords_ocr_localization(data[0], data[1], "zero_mean", None, None)
    return imgs, masks

def fundus_classification_processing_func_train(data):
    return data[0], load_img_from_filepath(data[0], "normalize", fundus_denoise, fundus_classification_augment_v0), data[1]


def fundus_classification_processing_func_val(data):
    return data[0], load_img_from_filepath(data[0], "normalize", fundus_denoise), data[1]


def vessel_segmentation_processing_func_train(filepaths):
    return load_img_mask_from_filepaths(filepaths, "z_score", augment_func=vessel_segmentation_augment)


def vessel_segmentation_processing_func_val(filepaths):
    return load_img_mask_from_filepaths(filepaths, "z_score")


def oct_classification_processing_func_train(data):
    return data[0], load_img_from_filepath(data[0], "normalize", augment_func=nicking_classification_augment), data[1]


def oct_classification_processing_func_val(data):
    return data[0], load_img_from_filepath(data[0], "normalize"), data[1]


def oct_localization_processing_func_train(data):
    img, coord = load_img_coords_from_filepath(
        data[0], data[1], "normalize", augment_func=oct_localization_augment)
    return data[0], img, coord


def oct_localization_processing_func_val(data):
    img, coord = load_img_coords_from_filepath(data[0], data[1], "normalize")
    return data[0], img, coord


def nicking_classification_processing_func_train(data):
    return data[0], load_img_from_filepath(data[0], "z_score", augment_func=nicking_classification_augment), data[1]
#     return data[0], load_img_from_filepath(data[0], "normalize", augment_func=nicking_classification_augment), data[1]


def nicking_classification_processing_func_val(data):
    return data[0], load_img_from_filepath(data[0], "z_score"), data[1]
#     return data[0], load_img_from_filepath(data[0], "normalize"), data[1]


def crop_fundus_img(img, red_threshold=20):
    """
    Crop a fundus image to remove the black background.
    Following images are not cropped
        - Pixels have red channel values < red_threshold
        - Pixels in central area are dominantly < red_threshold
        - The largest blob with red channel > red_threshold has a height or width less than 100

    Args:
        img (3d numpy array): fundus image

    Returns:
        cropped img (3d numpy array)

    """

    # read the image and resize
    h_ori, w_ori, _ = np.shape(img)
    roi_check_len = h_ori // 5

    # Find Connected Components with intensity above the threshold
    blobs_labels, n_blobs = measure.label(
        img[:, :, 0] > red_threshold, return_num=True)
    if n_blobs == 0:
        raise ValueError("crop failed [no blob found]")

    # Find the Index of Connected Components of the Fundus Area (the central area)
    majority_vote = np.argmax(np.bincount(blobs_labels[h_ori // 2 - roi_check_len // 2:h_ori // 2 + roi_check_len // 2,
                                                       w_ori // 2 - roi_check_len // 2:w_ori // 2 + roi_check_len // 2].flatten()))
    if majority_vote == 0:
        raise ValueError("crop failed [invisible areas (intensity in red channel less than " + str(red_threshold) + ") are dominant in the center]")

    row_inds, col_inds = np.where(blobs_labels == majority_vote)
    row_max = np.max(row_inds)
    row_min = np.min(row_inds)
    col_max = np.max(col_inds)
    col_min = np.min(col_inds)
    if row_max - row_min < 100 or col_max - col_min < 100:
        for i in range(1, n_blobs + 1):
            print(len(blobs_labels[blobs_labels == i]))
        raise ValueError("crop failed [too small areas]")

    # crop the image
    crop_img = img[row_min:row_max, col_min:col_max]
    max_len = max(crop_img.shape[0], crop_img.shape[1])
    img_h, img_w, _ = crop_img.shape
    padded = np.zeros((max_len, max_len, 3), dtype=np.uint8)
    padded[(max_len - img_h) // 2:(max_len - img_h) // 2 + img_h,
           (max_len - img_w) // 2:(max_len - img_w) // 2 + img_w, ...] = crop_img
    return padded


def network_input_fundus(filepath):
    """
    Prepare a network input for a file

    Args:
        filepath (str): path to a file

    Returns:
        cropped and resized and normalized numpy array for a fundus image

    """
    input_size = (512, 512)  # (h,w)
    img = np.array(Image.open(filepath))
    cropped_img = crop_fundus_img(img)
    resized_img = resize_img(cropped_img, input_size[0], input_size[1])
    normalized_resized_img = normalize(resized_img)
    return np.expand_dims(normalized_resized_img, axis=0)


def network_input_nicking(filepath):
    """
    Prepare a network input for a file

    Args:
        filepath (str): path to a file

    Returns:
        cropped and resized and normalized numpy array for a fundus image

    """
    input_size = (128, 128)  # (h,w)
    img = np.array(Image.open(filepath))
    resized_img = resize_img(img, input_size[0], input_size[1])
    # compute z-score
    means = np.zeros(3)
    stds = np.array([255.0, 255.0, 255.0])
    for i in range(3):
        if len(resized_img[..., i][resized_img[..., i] > 10]) > 1:
            means[i] = np.mean(resized_img[..., i][resized_img[..., i] > 10])
            std_val = np.std(resized_img[..., i][resized_img[..., i] > 10])
            if std_val > 0:
                stds[i] = std_val
        normalized_resized_img = (resized_img - means) / stds
    return np.expand_dims(normalized_resized_img, axis=0)


def save_heatmap_funuds_img(img, activation, outdir, filepath):
    """
    Save heatmap for fundus 

    Args:
        img (numpy array): a numpy array for an image (h,w,d)
        activation (numpy arrray): a numpy array for an activation map
        outdir (str): output directory
        filepath (str): path to the original file 

    """
    makedirs(outdir)
    out_fn = os.path.basename(filepath) if filepath else ""
    Image.fromarray(img.astype(np.uint8)).save(
        os.path.join(outdir, out_fn + "original.png"))
    h_target, w_target, _ = img.shape
    heatmap = imresize(activation, (h_target, w_target), 'bilinear')
    heatmap_pil_img = Image.fromarray(heatmap)
    heatmap_pil_img = heatmap_pil_img.filter(ImageFilter.GaussianBlur(16))
    heatmap_blurred = np.asarray(heatmap_pil_img)
    cm_spectral = cm.get_cmap("Spectral")
    heatmap_blurred = np.uint8(cm_spectral(heatmap_blurred)[..., :3] * 255)
    overlapped = (img * 0.7 + heatmap_blurred * 0.3).astype(np.uint8)
    Image.fromarray(overlapped).save(os.path.join(
        outdir, out_fn + "activation_overlapped.png"))


########################################################
########################################################
################## METRIC FUNCTIONS ####################
########################################################
########################################################
def dice(arr1, arr2):
    """
    Compute dice coefficient using scipy package

    Args:
        arr1, arr2: two numpy arrays

    Returns:
        dice coefficient (float)

    """
    arr1_bool = arr1.astype(np.bool)
    arr2_bool = arr2.astype(np.bool)
    intersection = np.count_nonzero(arr1_bool & arr2_bool)
    size1 = np.count_nonzero(arr1)
    size2 = np.count_nonzero(arr2)
    try:
        dc = 2. * intersection / float(size1 + size2)
    except ZeroDivisionError:
        dc = 0.0

    return dc
