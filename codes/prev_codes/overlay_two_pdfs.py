# CAVEATE: generated pdf 의 font 는 흰색으로 작성되어야함 (generated pdf 의 transparency 조절 불가)
#          mergeRoatedTranslatedPage 의 각도 계산시 pdf 의 \Rotate 값을 바탕으로 계산됨 (pyPDF2 의 rotate 는 clockwise 로 계산되고, \Rotate 에는 counterclockwise 로 얼마나 돌아가 있는지 표시되어있음)
#          rotateClockwise 로 rotate 해도 width, height 은 변하지 않음

from PyPDF2 import PdfFileReader,PdfFileWriter


with open('/home/apollon/OCR_project/data/inference/misc_modules/merge_pdfs/example2-1.pdf', 'rb') as in_f1:
    pdf_ori = PdfFileReader(in_f1)
    with open('/home/apollon/OCR_project/data/inference/misc_modules/merge_pdfs/example_text.pdf', 'rb') as in_f2:
        pdf_gen = PdfFileReader(in_f2)

        # get pages
        page_ori = pdf_ori.getPage(0)
        page_gen = pdf_gen.getPage(0)

        # check rotation, width, height
        print("ori -- rot: {}, w: {}, h: {}".format(page_ori.get('/Rotate'), float(page_ori.mediaBox.getWidth()), float(page_ori.mediaBox.getHeight())))
        print("gen -- rot: {}, w: {}, h: {}".format(page_gen.get('/Rotate'), float(page_gen.mediaBox.getWidth()), float(page_gen.mediaBox.getHeight())))
        # page_ori.rotateClockwise(90)
        print("ori -- rot: {}, w: {}, h: {}".format(page_ori.get('/Rotate'), float(page_ori.mediaBox.getWidth()), float(page_ori.mediaBox.getHeight())))
        print("gen -- rot: {}, w: {}, h: {}".format(page_gen.get('/Rotate'), float(page_gen.mediaBox.getWidth()), float(page_gen.mediaBox.getHeight())))

        # get rotation info
        rot_ori = page_ori.get('/Rotate')
        rot_gen = page_gen.get('/Rotate')

        # resize to the genertaed size
        if (rot_ori-rot_gen) % 180==0:
            target_width, target_height = float(page_gen.mediaBox.getWidth()), float(page_gen.mediaBox.getHeight())
        else:
            target_width, target_height = float(page_gen.mediaBox.getHeight()), float(page_gen.mediaBox.getWidth())
        page_ori.scaleTo(target_width, target_height)

        # merge two pdf
        # page_ori.mergePage(page_gen) # simple merge
        page_ori.mergeRotatedTranslatedPage(page_gen, rot_ori-rot_gen, float(page_gen.mediaBox.getWidth()/2), float(page_gen.mediaBox.getWidth()/2))
        
        # add to pdf writer
        pdfWriter = PdfFileWriter()
        pdfWriter.addPage(page_ori)
        with open('/home/apollon/OCR_project/data/inference/misc_modules/merge_pdfs/output.pdf', 'wb') as out_f:
            pdfWriter.write(out_f)
            
