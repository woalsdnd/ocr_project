import os
import numpy as np
from PIL import Image

import utils

from scipy import ndimage

# 1st training
# list_fpath = utils.all_files_under("../data/localization/images")
# path_ch_coords_file = "../data/localization/ch_coords.txt"
# path_word_coords_file = "../data/localization/wd_coords.txt"
# path_word_text_file = "../data/localization/word_text.txt"
# outdir_path  = "../data/localization/character_space_masks"

# # 2nd training
# list_fpath = utils.all_files_under("../data/localization/2nd_train/images")
# path_ch_coords_file = "../data/localization/2nd_train/ch_coords.txt"
# path_word_coords_file = "../data/localization/2nd_train/wd_coords.txt"
# path_word_text_file = "../data/localization/2nd_train/word_text.txt"
# outdir_path  = "../data/localization/2nd_train/character_space_masks"
# os.makedirs(outdir_path, exist_ok=True)

# 3rd training
list_fpath = utils.all_files_under("../data/localization/3rd_train/images")
path_ch_coords_file = "../data/localization/3rd_train/ch_coords.txt"
path_word_coords_file = "../data/localization/3rd_train/wd_coords.txt"
path_word_text_file = "../data/localization/3rd_train/word_text.txt"
outdir_path  = "../data/localization/3rd_train/character_space_masks"
os.makedirs(outdir_path, exist_ok=True)

# en-digits-specialchars
list_fpath = utils.all_files_under("../data/localization/en_digit_specialchars/images")
path_ch_coords_file = "../data/localization/en_digit_specialchars/ch_coords.txt"
path_word_coords_file = "../data/localization/en_digit_specialchars/wd_coords.txt"
path_word_text_file = "../data/localization/en_digit_specialchars/word_text.txt"
outdir_path  = "../data/localization/en_digit_specialchars/character_space_masks"
os.makedirs(outdir_path, exist_ok=True)

# sigma = 4
sigma = 8
tmp = np.zeros((101, 101))
tmp[50, 50] = 1
unit_gaussian_blur = ndimage.filters.gaussian_filter(tmp, sigma=sigma)
unit_gaussian_blur_normalized = unit_gaussian_blur / np.max(unit_gaussian_blur)

# bbox: [topleft_w, topleft_h, bottomright_w, bottomright_h]
with open(path_ch_coords_file, "r") as f:
    dict_ch_bbox = {l.rstrip().split("\t")[0].replace(".jpg", ""):[int(coord) for coord in l.rstrip().split("\t")[1].split(",")] for l in f.readlines()}
# bbox: [topleft_w, topleft_h, bottomright_w, bottomright_h]
with open(path_word_coords_file, "r") as f:
    dict_wd_bbox = {l.rstrip().split("\t")[0].replace(".jpg", ""):[int(coord) for coord in l.rstrip().split("\t")[1].split(",")] for l in f.readlines()}
# with open(path_ch_text_file, "r") as f:
#     dict_ch_text = {l.rstrip().split("\t")[0].replace(".jpg", ""):l.rstrip().split("\t")[1] for l in f.readlines()}
with open(path_word_text_file, "r") as f:
    dict_wd_text = {l.rstrip().split("\t")[0].replace(".jpg", "").replace(".jpeg", "").replace(".png",""):l.rstrip().split("\t")[1] for l in f.readlines()}

for fpath in list_fpath:

    # debugging sample
    # if "person_100" not in fpath:
    #     continue
    
    # get img
    img = np.array(Image.open(fpath))
    # make width and height divisible by 2
    if img.shape[0] % 2==1:
        img = img[:-1,...]
    if img.shape[1] % 2==1:
        img = img[:,:-1,...]
    h, w, _ = img.shape
    
    # get coords for char
    n_ch = 0
    list_ch_space_coords = []
    fid = os.path.basename(fpath).replace(".jpg", "")
    for wd_bbox_index in range(1000): # assume max words bbox < 1000
        wd_bbox_id = "{}_{}".format(fid, wd_bbox_index)
        if wd_bbox_id in dict_wd_bbox:
            # get coordinates for word box (obsolete)
            # w_min, h_min, w_max, h_max = dict_wd_bbox[wd_bbox_id]
            # w_min = max(0, w_min)
            # h_min = max(0, h_min)
            # w_max = min(w_max, w)
            # h_max = min(h_max, h)
            # word_box_coord = (w_min, h_min, w_max, h_max)
            
            # iterate over each word
            list_member_chars = []
            word = dict_wd_text[wd_bbox_id]
            for ch_bbox_index in range(len(word)):
                ch_bbox_id = "{}_{}".format(fid, n_ch+ch_bbox_index)
                if ch_bbox_id in dict_ch_bbox:
                    w_min, h_min, w_max, h_max = dict_ch_bbox[ch_bbox_id]
                    w_min = max(0, w_min)
                    h_min = max(0, h_min)
                    w_max = min(w_max, w)
                    h_max = min(h_max, h)
                    list_member_chars.append((w_min, h_min, w_max, h_max))
            n_ch += len(list_member_chars)
            
            # sort coordinates (obsolete)
            # list_w_right = [coord[2] for coord in list_member_chars]
            # list_h_right = [coord[3] for coord in list_member_chars]
            # if np.max(list_w_right)-np.min(list_w_right) >= np.max(list_h_right)-np.min(list_h_right): # horizontal
            #     horizontal = True
            # else: # vertical
            #     horizontal = False
            # sort_key = 2 if horizontal else 3
            # list_sorted_char_coords = sorted(list_member_chars, key=lambda x:x[sort_key])
            
            # compute spaces
            for index in range(len(list_member_chars)-1):
                el1, el2 = list_member_chars[index], list_member_chars[index+1]
                center1 = ((el1[0]+el1[2])//2, (el1[1]+el1[3])//2) # (c_x, c_y)
                center2 = ((el2[0]+el2[2])//2, (el2[1]+el2[3])//2) # (c_x, c_y)
                if np.abs(center1[0]-center2[0]) > np.abs(center1[1]-center2[1]): # horizontal
                    xmin = min((el1[0]+center1[0]+el1[2])//3, (el2[0]+center2[0]+el2[2])//3)
                    ymin = min((el1[1]+center1[1]+el1[1])//3, (el2[1]+center2[1]+el2[1])//3)
                    xmax = max((el2[0]+center2[0]+el2[2])//3, (el1[0]+center1[0]+el1[2])//3)
                    ymax = max((el2[3]+center2[1]+el2[3])//3, (el1[3]+center1[1]+el1[3])//3)
                else:
                    xmin = min((el1[0]+center1[0]+el1[0])//3, (el2[0]+center2[0]+el2[0])//3)
                    ymin = min((el1[1]+center1[1]+el1[3])//3, (el2[1]+center2[1]+el2[3])//3)
                    xmax = max((el2[2]+center2[0]+el2[2])//3, (el1[2]+center1[0]+el1[2])//3)
                    ymax = max((el2[1]+center2[1]+el2[3])//3, (el1[1]+center1[1]+el1[3])//3)
                if xmin<xmax and ymin<ymax:
                    list_ch_space_coords.append((xmin, ymin, xmax, ymax))
        else:
            # # sanity check (check if all characters are included)
            # for ch_bbox_index in range(1000): # assume max characters < 100 for each word
            #     ch_bbox_id = "{}_{}".format(fid, ch_bbox_index)
            #     if ch_bbox_id not in dict_ch_bbox:
            #         n_ch_txt = ch_bbox_index
            #         break
            # print(n_ch, n_ch_txt, fid)
            # assert n_ch==n_ch_txt
            
            break
    # print(wd_bbox_id, list_ch_space_coords)
    mask = utils.generate_mask_from_bbox_coords(list_ch_space_coords, img.shape[:2], unit_gaussian_blur_normalized)
    Image.fromarray((mask*255).astype(np.uint8)).save(os.path.join(outdir_path, os.path.basename(fpath)))
    