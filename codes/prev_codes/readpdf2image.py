from pdf2image import convert_from_path
import numpy as np

images = convert_from_path('/home/apollon/OCR_project/data/inference/misc_modules/read_pdfs/example3.pdf', dpi=300)
n_pages = len(images)
for index in range(n_pages):
    images[index].save("/home/apollon/OCR_project/data/inference/misc_modules/read_pdfs/page-{}.png".format(index))
    image = np.array(images[0]) # get numpy 
