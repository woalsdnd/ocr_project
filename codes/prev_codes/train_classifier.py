import os
import logging
import argparse
import configparser
import time

import numpy as np
import pandas as pd
import random
from PIL import Image

import utils
import iterator_shared_array
from model.efficient_net import tiny_network, tiny_network_v2, tiny_network_v3, tiny_network_v4, small_network, small_network_v2, EfficientNetB0, EfficientNetB4, EfficientNetB7, append_softmax, set_ce_loss
from keras.utils import to_categorical

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--config_file',
    type=str,
    required=True
    )
parser.add_argument(
    '--mode',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set config
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read_file(open(FLAGS.config_file))
dir_training_input_data1 = config["Path"]["path_data1"]
dir_training_input_data2 = config["Path"]["path_data2"]
dir_training_input_data3 = config["Path"]["path_data3"]
dir_save_model = config["Path"]["dir_save_model"]
path_pretrained_weight = config["Path"]["path_pretrained_weight"] if "path_pretrained_weight" in config["Path"] else None
dir_experimental_result = config["Path"]["dir_experimental_result"]
dir_logger = config["Path"]["dir_logger"]
path_logger = os.path.join(dir_logger, os.path.basename(FLAGS.config_file).replace(".cfg", ".log")) 
batch_size = int(config["Train"]["batch_size"])
n_epochs = int(config["Train"]["n_epochs"])
lr_decay_tolerance = int(config["Train"]["lr_decay_tolerance"])
lr_decay_factor = float(config["Train"]["lr_decay_factor"])
lr_start_value = float(config["Train"]["lr_start"])
lr_min_value = float(config["Train"]["lr_min_value"])
optimizer = config["Train"]["optimizer"]
gpu_index = config["Train"]["gpu_index"]
architecture = config["Train"]["architecture"] if "architecture" in config["Train"] else None
input_shape = (int(config["Input"]["height"]), int(config["Input"]["width"]), int(config["Input"]["depth"]))
utils.makedirs(dir_save_model)
utils.makedirs(dir_experimental_result)
utils.makedirs(dir_logger)
n_character_classes = len(utils.list_string)
dir_inference_output = config["Path"]["dir_inference_output"]
dir_inference_output = os.path.join(dir_inference_output, os.path.basename(FLAGS.config_file).replace(".cfg", "")) 
if FLAGS.mode == "val":
    os.makedirs(dir_inference_output, exist_ok=True)

# set logger and tensorboard
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
handler = logging.FileHandler(path_logger)
handler.setFormatter(formatter)
logger.addHandler(handler)
tensorboard = utils.CustomTensorBoard(
  log_dir=os.path.join(dir_logger, "tensorboard", os.path.basename(FLAGS.config_file).replace(".cfg", ".log")),
  write_graph=False,
  batch_size=batch_size
)

# set gpu index
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_index

# split data
path_data1 = utils.all_files_under(dir_training_input_data1)
path_data2 = utils.all_files_under(dir_training_input_data2)
path_data3 = utils.all_files_under(dir_training_input_data3)
list_path_data = path_data1 + path_data2 + path_data3
random.seed(0)
random.shuffle(list_path_data)
train_input_data = list_path_data[:-len(list_path_data)//10]
val_input_data = list_path_data[-len(list_path_data)//10:]

# set filenames and labels
train_label_data, val_label_data = [], []
for fpath in train_input_data:
    label_str = os.path.basename(fpath).split("_")[0]
    if len(label_str)==0: # character "_"
        label_str = "_"
    elif label_str == "slash":
        label_str = "/"
    elif label_str == "period":
        label_str = "."
    label = utils.str2label(label_str)
    train_label_data.append(label)
for fpath in val_input_data:
    label_str = os.path.basename(fpath).split("_")[0]
    if len(label_str)==0: # character "_"
        label_str = "_"
    elif label_str == "slash":
        label_str = "/"
    elif label_str == "period":
        label_str = "."
    label = utils.str2label(label_str)
    val_label_data.append(label)

# set iterator
class_weight, sample_weight = utils.balanced_class_weights(train_label_data)
logger.info("class weight: {}".format(class_weight))
if FLAGS.mode == "train":
    train_batch_fetcher = iterator_shared_array.BatchFetcher((train_input_data, train_label_data), sample_weight,
                                           utils.ocr_classification_processing_func_train, batch_size, sample=True, replace=True, shared_array_shape=[input_shape, ()])
val_batch_fetcher = iterator_shared_array.BatchFetcher((val_input_data, val_label_data), None,
                                          utils.ocr_classification_processing_func_val, batch_size, sample=False, replace=False, shared_array_shape=[input_shape, ()])

# define network
if path_pretrained_weight:
    if architecture=="tiny":
        network = tiny_network(input_shape=input_shape)
    elif architecture=="tiny_v2":
        network = tiny_network_v2(input_shape=input_shape)
    elif architecture=="tiny_v3":
        network = tiny_network_v3(input_shape=input_shape)
    elif architecture=="tiny_v4":
        network = tiny_network_v4(input_shape=input_shape)
    elif architecture=="small":
        network = small_network(input_shape=input_shape)
    elif architecture=="small_v2":
        network = small_network_v2(input_shape=input_shape)
    # network = EfficientNetB0(include_top=False, weights='imagenet', input_shape=input_shape)
    # network = EfficientNetB4(include_top=False, weights='imagenet', input_shape=input_shape)
    # network = EfficientNetB7(include_top=False, weights='imagenet', input_shape=input_shape)
    network = append_softmax(network)
    network = set_ce_loss(network, lr_start_value) 
    network.load_weights(path_pretrained_weight)
    logger.info("model loaded from {}".format(path_pretrained_weight))
else:
    if architecture=="tiny":
        network = tiny_network(input_shape=input_shape)
    elif architecture=="tiny_v2":
        network = tiny_network_v2(input_shape=input_shape)
    elif architecture=="tiny_v3":
        network = tiny_network_v3(input_shape=input_shape)
    elif architecture=="tiny_v4":
        network = tiny_network_v4(input_shape=input_shape)
    elif architecture=="small":
        network = small_network(input_shape=input_shape)
    elif architecture=="small_v2":
        network = small_network_v2(input_shape=input_shape)
    # network = EfficientNetB0(include_top=False, weights='imagenet', input_shape=input_shape)
    # network = EfficientNetB4(include_top=False, weights='imagenet', input_shape=input_shape)
    # network = EfficientNetB7(include_top=False, weights='imagenet', input_shape=input_shape)
    network = append_softmax(network)
    network = set_ce_loss(network, lr_start_value) 
network.summary()
tensorboard.set_model(network)  # set tensorboard callback associated with network
# save network
with open(os.path.join(dir_save_model, "network.json"), 'w') as f:
    f.write(network.to_json())

lr_scheduler = utils.LrScheduler(lr_object=network.optimizer.lr, lr_start_value=lr_start_value,
                                 lr_min_value=lr_min_value, lr_decay_tolerance=lr_decay_tolerance,
                                 lr_decay_factor=lr_decay_factor, logger=logger, score_func=lambda x:x["accuracy"])
for epoch in range(n_epochs):
    # train loop
    if FLAGS.mode=="train":
        list_training_loss, list_training_acc = [], []
        for data, list_arr in train_batch_fetcher:
            imgs , labels = list_arr
            tensorboard.draw_imgs("Training Image", epoch, (imgs*30+128).astype(np.uint8), plot_once=True)
            loss, acc = network.train_on_batch(imgs, to_categorical(labels, n_character_classes))
            utils.stack_list(head1=list_training_loss, tail1=[loss] * len(imgs), head2=list_training_acc, tail2=[acc] * len(imgs))
        train_metrics = {"training_loss":np.mean(list_training_loss), "training_acc":np.mean(list_training_acc)}
        utils.log_summary(logger, phase="training", epoch=epoch, **train_metrics)
        tensorboard.on_epoch_end(epoch, train_metrics)

    # val loop
    list_gt, list_pred = [], []
    for data, list_arr in val_batch_fetcher:
        imgs , labels = list_arr
        tensorboard.draw_imgs("Validation Image", epoch, (imgs*30+128).astype(np.uint8), plot_once=True)
        preds = network.predict(imgs)
        utils.stack_list(head1=list_gt, tail1=list(labels), head2=list_pred, tail2=list(np.argmax(preds, axis=-1)))
        if FLAGS.mode=="val":
            guesses = np.argmax(preds, axis=-1)
            labels = labels.astype(np.int)
            for wrong_index in np.where(labels!=guesses)[0]:
                Image.fromarray((imgs[wrong_index,...,0]*30+128).astype(np.uint8)).save(os.path.join(dir_inference_output, "true{}_pred{}.png".format(utils.list_string[labels[wrong_index]].replace("/","backspace"), utils.list_string[guesses[wrong_index]].replace("/","backspace"))))

    val_metrics = utils.categorical_stats(list_gt, list_pred, return_weighted_kappa=False, return_confusion_matrix=False)
    if FLAGS.mode=="val":
        print(val_metrics)
        exit(1)
    utils.log_summary(logger, phase="validation", epoch=epoch, **val_metrics)
    tensorboard.on_epoch_end(epoch, val_metrics)


    # adjust learning rate
    lr_scheduler.adjust_lr(epoch, val_metrics)

    # save network
    network.save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))