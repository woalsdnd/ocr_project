import os
import logging
import argparse
import configparser
import time

import numpy as np
import pandas as pd
import random
from PIL import Image

import utils
import iterator_shared_array
# from model.efficient_net import EfficientNetB0_localization, EfficientNetB4_localization, EfficientNetB7_localization # TODO network coding
from keras.utils import to_categorical

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--config_file',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set config
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read_file(open(FLAGS.config_file))
dir_training_input_data1 = config["Path"]["path_data1"]
path_training_label_data1 = config["Path"]["path_label1"]
dir_save_model = config["Path"]["dir_save_model"]
path_pretrained_weight = config["Path"]["path_pretrained_weight"] if "path_pretrained_weight" in config["Path"] else None
dir_experimental_result = config["Path"]["dir_experimental_result"]
dir_logger = config["Path"]["dir_logger"]
path_logger = os.path.join(dir_logger, os.path.basename(FLAGS.config_file).replace(".cfg", ".log")) 
batch_size = int(config["Train"]["batch_size"])
n_epochs = int(config["Train"]["n_epochs"])
lr_decay_tolerance = int(config["Train"]["lr_decay_tolerance"])
lr_decay_factor = float(config["Train"]["lr_decay_factor"])
lr_start_value = float(config["Train"]["lr_start"])
lr_min_value = float(config["Train"]["lr_min_value"])
optimizer = config["Train"]["optimizer"]
gpu_index = config["Train"]["gpu_index"]
input_shape = (int(config["Input"]["height"]), int(config["Input"]["width"]), int(config["Input"]["depth"])) if "height" in config["Input"] else None, None, None
utils.makedirs(dir_save_model)
utils.makedirs(dir_experimental_result)
utils.makedirs(dir_logger)
n_character_classes = len(utils.list_string)

# set logger and tensorboard
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
handler = logging.FileHandler(path_logger)
handler.setFormatter(formatter)
logger.addHandler(handler)
tensorboard = utils.CustomTensorBoard(
  log_dir=os.path.join(dir_logger, "tensorboard", os.path.basename(FLAGS.config_file).replace(".cfg", ".log")),
  write_graph=False,
  batch_size=batch_size
)

# set gpu index
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_index

# split data
path_data1 = utils.all_files_under(dir_training_input_data1)
list_path_data = path_data1
random.seed(0)
random.shuffle(list_path_data)
train_input_data = list_path_data[:-len(list_path_data)//10]
val_input_data = list_path_data[-len(list_path_data)//10:]

# set filenames and labels
# read and process label file
# label: [[bbox1_img1, bbox2_img1], [bbox1_img2, bbox2_img2], ...]
# bbox: [topleft_w, topleft_h, bottomright_w, bottomright_h]
with open(path_training_label_data1, "r") as f:
    dict_bbox = {l.rstrip().split("\t")[0].replace(".jpg", ""):[int(coord) for coord in l.rstrip().split("\t")[1].split(",")] for l in f.readlines()}

train_label_data, val_label_data = [], []
for fpath in train_input_data:
    fid = os.path.basename(fpath).replace(".jpg", "")
    label = []
    for bbox_index in range(1000): # assume max bbox < 1000
        bbox_id = "{}_{}".format(fid, bbox_index)
        if bbox_id in dict_bbox:
            label.append(dict_bbox[bbox_id])
    train_label_data.append(label)
for fpath in val_input_data:
    fid = os.path.basename(fpath).replace(".jpg", "")
    label = []
    for bbox_index in range(1000): # assume max bbox < 1000
        bbox_id = "{}_{}".format(fid, bbox_index)
        if bbox_id in dict_bbox:
            label.append(dict_bbox[bbox_id])
    val_label_data.append(label)

# set iterator
iterator_img_shape = (448, 512, 3)
iterator_mask_shape = (448, 512, 1)
uniform_sample_weights = utils.uniform_sample_weight(len(train_label_data))
train_batch_fetcher = iterator_shared_array.BatchFetcher((train_input_data, train_label_data), uniform_sample_weights,
                                           utils.ocr_localization_processing_func_train, batch_size, sample=True, replace=True, shared_array_shape=[iterator_img_shape, iterator_mask_shape])
val_batch_fetcher = iterator_shared_array.BatchFetcher((val_input_data, val_label_data), None,
                                          utils.ocr_localization_processing_func_val, batch_size, sample=False, replace=False, shared_array_shape=[iterator_img_shape, iterator_mask_shape])

for data, list_arr in train_batch_fetcher:
    imgs , labels = list_arr
    tensorboard.draw_imgs("Training Image", 0, (imgs*30+128).astype(np.uint8), plot_once=True)

    exit(1)

# define network
if path_pretrained_weight:
    # network = EfficientNetB0_localization(include_top=False, weights='imagenet', input_shape=input_shape)
    # network = EfficientNetB4_localization(include_top=False, weights='imagenet', input_shape=input_shape)
    network = EfficientNetB7_localization(include_top=False, weights='imagenet', input_shape=input_shape)
    network.load_weights(path_pretrained_weight)
    logger.info("model loaded from {}".format(path_pretrained_weight))
else:
    # network = EfficientNetB0_localization(include_top=False, weights='imagenet', input_shape=input_shape)
    # network = EfficientNetB4_localization(include_top=False, weights='imagenet', input_shape=input_shape)
    network = EfficientNetB7_localization(include_top=False, weights='imagenet', input_shape=input_shape)
network.summary()
tensorboard.set_model(network)  # set tensorboard callback associated with network
# save network
with open(os.path.join(dir_save_model, "network.json"), 'w') as f:
    f.write(network.to_json())

lr_scheduler = utils.LrScheduler(lr_object=network.optimizer.lr, lr_start_value=lr_start_value,
                                 lr_min_value=lr_min_value, lr_decay_tolerance=lr_decay_tolerance,
                                 lr_decay_factor=lr_decay_factor, logger=logger, score_func=lambda x:x["accuracy"])
for epoch in range(n_epochs):
    # train loop
    list_training_loss, list_training_acc = [], []
    for data, list_arr in train_batch_fetcher:
        imgs , labels = list_arr
        tensorboard.draw_imgs("Training Image", epoch, (imgs*30+128).astype(np.uint8), plot_once=True)
        loss, acc = network.train_on_batch(imgs, to_categorical(labels, n_character_classes))
        utils.stack_list(head1=list_training_loss, tail1=[loss] * len(imgs), head2=list_training_acc, tail2=[acc] * len(imgs))
    train_metrics = {"training_loss":np.mean(list_training_loss), "training_acc":np.mean(list_training_acc)}
    utils.log_summary(logger, phase="training", epoch=epoch, **train_metrics)
    tensorboard.on_epoch_end(epoch, train_metrics)

    # val loop
    list_gt, list_pred = [], []
    for data, list_arr in val_batch_fetcher:
        imgs , labels = list_arr
        tensorboard.draw_imgs("Validation Image", epoch, (imgs*30+128).astype(np.uint8), plot_once=True)
        preds = network.predict(imgs)
        utils.stack_list(head1=list_gt, tail1=list(labels), head2=list_pred, tail2=list(np.argmax(preds, axis=-1)))
    val_metrics = utils.categorical_stats(list_gt, list_pred, return_weighted_kappa=False, return_confusion_matrix=False)
    utils.log_summary(logger, phase="validation", epoch=epoch, **val_metrics)
    tensorboard.on_epoch_end(epoch, val_metrics)

    # adjust learning rate
    lr_scheduler.adjust_lr(epoch, val_metrics)

    # save network
    network.save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))