from PyPDF2 import PdfFileWriter, PdfFileReader
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.colors import white, red, black

pdfmetrics.registerFont(TTFont("NanumMyeongjo", "NanumMyeongjo.ttf"))

with open('/home/apollon/OCR_project/data/inference/misc_modules/write2pdfs/output.pdf', 'wb') as out_f:
    can = canvas.Canvas(out_f, pagesize=A4)
    can.setFont("Times-Roman", 20)
    can.setFillColor(red)
    can.drawString(10, 100, "yes") # (x_coord, y_coord, str)

    can.setFont("Helvetica", 30)
    can.setFillColor(black)
    can.drawString(50, 100, "SURE")
    
    can.setFont("Times-Roman", 40)
    can.setFillColor(white)
    can.drawString(100, 200, "WHY NOT?!?!")

    can.setFont("NanumMyeongjo", 40)
    can.setFillColor(black)
    can.drawString(50, 50, u"{}".format("독도는 우리땅"))

    can.save()



