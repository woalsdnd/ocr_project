# python3 train_localizer.py --config_file=../config/localization_unet_multiple_output.cfg
import os
import logging
import argparse
import configparser
import time

import numpy as np
import pandas as pd
import random
from PIL import Image

import utils
import iterator_shared_array
from model.efficient_net import EfficientNetB0, EfficientNetB4, EfficientNetB7, append_decoder, set_bce_loss
from model.unet import unet_multi_outputs, set_weighted_bce_loss
from keras.utils import to_categorical

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--config_file',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set config
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read_file(open(FLAGS.config_file))
dir_training_input_1 = config["Path"]["path_data1"]
dir_training_ch_output1_1 = config["Path"]["path_ch_mask1_output1"]
dir_training_ch_output2_1 = config["Path"]["path_ch_mask1_output2"]
dir_training_ch_output3_1 = config["Path"]["path_ch_mask1_output3"]
if "path_data2" in config["Path"]:
    dir_training_input_2 = config["Path"]["path_data2"]
    dir_training_ch_output1_2 = config["Path"]["path_ch_mask2_output1"]
    dir_training_ch_output2_2 = config["Path"]["path_ch_mask2_output2"]
    dir_training_ch_output3_2 = config["Path"]["path_ch_mask2_output3"]
dir_val_input = config["Path"]["path_val_data"] if "path_val_data" in config["Path"] else None
dir_val_ch_output1 = config["Path"]["path_val_ch_mask_output1"] if "path_val_ch_mask_output1" in config["Path"] else None
dir_val_ch_output2 = config["Path"]["path_val_ch_mask_output2"] if "path_val_ch_mask_output2" in config["Path"] else None
dir_val_ch_output3 = config["Path"]["path_val_ch_mask_output3"] if "path_val_ch_mask_output2" in config["Path"] else None
dir_save_model = config["Path"]["dir_save_model"]
path_pretrained_weight = config["Path"]["path_pretrained_weight"] if "path_pretrained_weight" in config["Path"] else None
dir_experimental_result = config["Path"]["dir_experimental_result"]
dir_logger = config["Path"]["dir_logger"]
path_logger = os.path.join(dir_logger, os.path.basename(FLAGS.config_file).replace(".cfg", ".log")) 
bce_loss_pos_weight = int(config["Train"]["bce_loss_pos_weight"])
batch_size = int(config["Train"]["batch_size"])
n_epochs = int(config["Train"]["n_epochs"])
lr_decay_tolerance = int(config["Train"]["lr_decay_tolerance"])
lr_decay_factor = float(config["Train"]["lr_decay_factor"])
lr_start_value = float(config["Train"]["lr_start"])
lr_min_value = float(config["Train"]["lr_min_value"])
optimizer = config["Train"]["optimizer"]
gpu_index = config["Train"]["gpu_index"]
input_shape = (int(config["Input"]["height"]), int(config["Input"]["width"]), int(config["Input"]["depth"])) if "height" in config["Input"] else (None, None, None)
resolution_division_factor = int(config["Input"]["resolution_division_factor"])
os.makedirs(dir_save_model, exist_ok=True)
os.makedirs(dir_experimental_result, exist_ok=True)
os.makedirs(dir_logger, exist_ok=True)

# set logger and tensorboard
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
handler = logging.FileHandler(path_logger)
handler.setFormatter(formatter)
logger.addHandler(handler)
tensorboard = utils.CustomTensorBoard(
  log_dir=os.path.join(dir_logger, "tensorboard", os.path.basename(FLAGS.config_file).replace(".cfg", ".log")),
  write_graph=False,
  batch_size=batch_size
)

# set gpu index
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_index

# split data
list_input_dirs = [dir_training_input_1, dir_training_input_2] if "path_data2" in config["Path"] else [dir_training_input_1]
list_output1_dirs = [dir_training_ch_output1_1, dir_training_ch_output1_2] if "path_data2" in config["Path"] else [dir_training_ch_output1_1]
list_output2_dirs = [dir_training_ch_output2_1, dir_training_ch_output2_2] if "path_data2" in config["Path"] else [dir_training_ch_output2_1]
list_output3_dirs = [dir_training_ch_output3_1, dir_training_ch_output3_2] if "path_data2" in config["Path"] else [dir_training_ch_output3_1]
train_input_data, val_input_data = [], []
train_output1, val_output1 = [], []
train_output2, val_output2 = [], []
train_output3, val_output3 = [], []
for data_index in range(len(list_input_dirs)):
    dir_training_input = list_input_dirs[data_index]
    dir_training_output1 = list_output1_dirs[data_index]
    dir_training_output2 = list_output2_dirs[data_index]
    dir_training_output3 = list_output3_dirs[data_index]
    list_path_data = utils.all_files_under(dir_training_input)
    random.seed(0)
    random.shuffle(list_path_data)
    if dir_val_input is None and dir_val_ch_output1 is None and dir_val_ch_output2 is None and dir_val_ch_output3 is None:
        fpaths_training = list_path_data[:-len(list_path_data)//500]
        fpaths_val = list_path_data[-len(list_path_data)//500:]
        # set filenames for validation images and masks by splitting the given dataset
        val_input_data += fpaths_val
        for fpath in fpaths_val:
            fname = os.path.basename(fpath)
            val_output1.append(os.path.join(dir_training_output1, fname))
            val_output2.append(os.path.join(dir_training_output2, fname))
            val_output3.append(os.path.join(dir_training_output3, fname))
    else:
        fpaths_training = list_path_data
    
    # set filenames for training images and masks
    train_input_data += fpaths_training
    for fpath in fpaths_training:
        fname = os.path.basename(fpath)
        train_output1.append(os.path.join(dir_training_output1, fname))
        train_output2.append(os.path.join(dir_training_output2, fname))
        train_output3.append(os.path.join(dir_training_output3, fname))
    
# set reserved val
if len(val_input_data)==0:
    fpaths_val = utils.all_files_under(dir_val_input)
    val_input_data = fpaths_val
    for fpath in fpaths_val:
        fname = os.path.basename(fpath)
        val_output1.append(os.path.join(dir_val_ch_output1, fname))
        val_output2.append(os.path.join(dir_val_ch_output2, fname))
        val_output3.append(os.path.join(dir_val_ch_output3, fname))

# set iterator
iterator_img_shape = input_shape
iterator_mask_shape = tuple([s // resolution_division_factor for s in input_shape[:2]]) + (1, )
uniform_sample_weights = utils.uniform_sample_weight(len(train_output1))
train_batch_fetcher = iterator_shared_array.BatchFetcher((train_input_data, train_output1, train_output2, train_output3, [iterator_img_shape]*len(train_input_data), [iterator_mask_shape]*len(train_input_data)), uniform_sample_weights,
                                           utils.ocr_localization_processing_func_train, batch_size, sample=True, replace=True, shared_array_shape=[iterator_img_shape, iterator_mask_shape, iterator_mask_shape, iterator_mask_shape])
val_batch_fetcher = iterator_shared_array.BatchFetcher((val_input_data, val_output1, val_output2, val_output3, [iterator_img_shape]*len(val_input_data), [iterator_mask_shape]*len(val_input_data)), None,
                                          utils.ocr_localization_processing_func_val, batch_size, sample=False, replace=False, shared_array_shape=[iterator_img_shape, iterator_mask_shape, iterator_mask_shape, iterator_mask_shape])

# # debug iterator
# for data, list_arr in train_batch_fetcher:
#     imgs , ch_region_masks, ch_center_masks, ch_space_masks = list_arr
#     print("fetched")
#     tensorboard.draw_imgs("Training Image", 0, (imgs*255).astype(np.uint8), plot_once=False)
#     tensorboard.draw_imgs("Training CH REGION MASK", 0, (ch_region_masks*255).astype(np.uint8), plot_once=False)
#     tensorboard.draw_imgs("Training CH CENTER MASK", 0, (ch_center_masks*255).astype(np.uint8), plot_once=False)
#     tensorboard.draw_imgs("Training CH SPACE MASK", 0, (ch_space_masks*255).astype(np.uint8), plot_once=False)
#     exit(1)

# define network
if path_pretrained_weight:
    network = unet_multi_outputs(input_shape)
    network = set_weighted_bce_loss(network, lr_start_value, bce_loss_pos_weight)
    network.load_weights(path_pretrained_weight)
    # TODO define new network that has the input and output but different loss (suppression_loss)
    logger.info("model loaded from {}".format(path_pretrained_weight))
else:
    network = unet_multi_outputs(input_shape)
    network = set_weighted_bce_loss(network, lr_start_value, bce_loss_pos_weight)
    # TODO define new network that has the input and output but different loss (suppression_loss)
network.summary()
tensorboard.set_model(network)  # set tensorboard callback associated with network
# save network
with open(os.path.join(dir_save_model, "network.json"), 'w') as f:
    f.write(network.to_json())

for epoch in range(n_epochs):
    # train loop
    list_training_ch_output1_loss, list_training_ch_output2_loss = [], []
    for data, list_arr in train_batch_fetcher:
        imgs, ch_output1_masks, ch_output2_masks, ch_output3_masks = list_arr
        tensorboard.draw_imgs("Training IMAGE", epoch, (imgs*30+128).astype(np.uint8), plot_once=epoch!=0)
        tensorboard.draw_imgs("Training 1st output", 0, (ch_output1_masks*255).astype(np.uint8), plot_once=epoch!=0)
        tensorboard.draw_imgs("Training 2nd output", 0, (ch_output2_masks*255).astype(np.uint8), plot_once=epoch!=0)
        tensorboard.draw_imgs("Training 3rd output", 0, (ch_output3_masks*255).astype(np.uint8), plot_once=epoch!=0)
        total_loss, loss_ch_output1, loss_ch_output2 = network.train_on_batch(imgs, [ch_output1_masks, ch_output2_masks])
        utils.stack_list(head1=list_training_ch_output1_loss, tail1=[loss_ch_output2] * len(imgs), head2=list_training_ch_output2_loss, tail2=[loss_ch_output2] * len(imgs))
    train_metrics = {"training_ch_output1_loss":np.mean(list_training_ch_output1_loss), "training_ch_output2_loss":np.mean(list_training_ch_output2_loss)}
    utils.log_summary(logger, phase="training", epoch=epoch, **train_metrics)
    tensorboard.on_epoch_end(epoch, train_metrics)
    

    # val loop
    list_gt_ch_output1, list_pred_ch_output1 = [], []
    list_gt_ch_output2, list_pred_ch_output2 = [], []
    list_img = []
    for data, list_arr in val_batch_fetcher:
        imgs, ch_output1_masks, ch_output2_masks, ch_output3_masks = list_arr
        tensorboard.draw_imgs("Validation Image", epoch, (imgs*30+128).astype(np.uint8), plot_once=epoch!=0)
        tensorboard.draw_imgs("Validation 1st output", 0, (ch_output1_masks*255).astype(np.uint8), plot_once=epoch!=0)
        tensorboard.draw_imgs("Validation 2nd output", 0, (ch_output2_masks*255).astype(np.uint8), plot_once=epoch!=0)
        tensorboard.draw_imgs("Validation 3rd output", 0, (ch_output3_masks*255).astype(np.uint8), plot_once=epoch!=0)
        ch_output1_mask_pred, ch_output2_mask_pred = network.predict(imgs)
        utils.stack_list(head1=list_gt_ch_output1, tail1=list(ch_output1_masks), head2=list_gt_ch_output2, tail2=list(ch_output2_masks), 
                         head3=list_pred_ch_output1, tail3=list(ch_output1_mask_pred), head4=list_pred_ch_output2, 
                         tail4=list(ch_output2_mask_pred), head5=list_img, tail5=list(imgs))
    marker = ["pred_1st_output", "pred_2nd_output", "img"]
    for index, list_data in enumerate([list_pred_ch_output1, list_pred_ch_output2, list_img]):
        arrs = np.array(list_data)
        if arrs.shape[-1]==1:
            arrs = arrs[...,0]
            if marker[index]=="pred_1st_output":
                img_arrs = np.array(list_img)
                for index_arr, arr in enumerate(arrs):
                    img_arrs[index_arr][arr <= 0.5] = 0
                    Image.fromarray((img_arrs[index_arr]*255).astype(np.uint8)).save(os.path.join(dir_experimental_result, "{}thepoch_{}_{}.png".format(epoch, marker[index], index_arr)))
            else:
                for index_arr, arr in enumerate(arrs):
                    Image.fromarray((arr*255).astype(np.uint8)).save(os.path.join(dir_experimental_result, "{}thepoch_{}_{}.png".format(epoch, marker[index], index_arr)))
        else:
            for index_arr, arr in enumerate(arrs):
                Image.fromarray((arr*255).astype(np.uint8)).save(os.path.join(dir_experimental_result, "{}thepoch{}_{}.png".format(epoch, marker[index], index_arr)))
    # save network
    network.save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))