# python3 train_CTC.py --config_file=../config/ctc_5th_training.cfg --mode=train
import os
import logging
import argparse
import configparser
import time
import sys
from keras.utils.training_utils import multi_gpu_model

import numpy as np
import pandas as pd
import random
from PIL import Image

import utils
import iterator_ctc_shared_array
from model.efficient_net import small_network_v2, append_softmax, small_network_v2_LSTM, set_ctc_loss, ctc_decode, output_as_loss
from keras.utils import to_categorical

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--config_file',
    type=str,
    required=True
    )
parser.add_argument(
    '--mode',
    choices=["train", "val"],
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

def read_label_txt(path_txt):
    with open(path_txt) as f:
        list_labels = f.readlines()
    return [l.rstrip() for l in list_labels]

def gen_tuple(fpaths, labels):
    list_tuple=[]
    for fpath in fpaths:
        index = int(os.path.basename(fpath).split("_")[0])
        list_tuple.append((fpath, labels[index]))
    return list_tuple


# set config
dict_width_divisor = {"baseline":4}
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read_file(open(FLAGS.config_file))
dir_training_input_data1 = config["Path"]["path_data1"]
path_training_label1 = config["Path"]["path_label1"]
dir_training_input_data2 = config["Path"]["path_data2"] if "path_data2" in config["Path"] else None
path_training_label2 = config["Path"]["path_label2"] if "path_label2" in config["Path"] else None
dir_training_input_data3 = config["Path"]["path_data3"] if "path_data3" in config["Path"] else None
path_training_label3 = config["Path"]["path_label3"] if "path_label3" in config["Path"] else None
dir_training_input_data4 = config["Path"]["path_data4"] if "path_data4" in config["Path"] else None
path_training_label4 = config["Path"]["path_label4"] if "path_label4" in config["Path"] else None
dir_training_input_data5 = config["Path"]["path_data5"] if "path_data5" in config["Path"] else None
path_training_label5 = config["Path"]["path_label5"] if "path_label5" in config["Path"] else None
dir_save_model = config["Path"]["dir_save_model"]
path_pretrained_weight = config["Path"]["path_pretrained_weight"] if "path_pretrained_weight" in config["Path"] else None
dir_experimental_result = config["Path"]["dir_experimental_result"]
dir_logger = config["Path"]["dir_logger"]
path_logger = os.path.join(dir_logger, os.path.basename(FLAGS.config_file).replace(".cfg", ".log")) 
batch_size = int(config["Train"]["batch_size"])
n_epochs = int(config["Train"]["n_epochs"])
lr_decay_tolerance = int(config["Train"]["lr_decay_tolerance"])
lr_decay_factor = float(config["Train"]["lr_decay_factor"])
lr_start_value = float(config["Train"]["lr_start"])
lr_min_value = float(config["Train"]["lr_min_value"])
max_char_len = int(config["Train"]["max_char_len"])
train_split = int(config["Train"]["train_split"]) if "train_split" in config["Train"] else None
optimizer = config["Train"]["optimizer"]
gpu_index = config["Train"]["gpu_index"]
architecture = config["Train"]["architecture"] if "architecture" in config["Train"] else None
input_shape = (int(config["Input"]["height"]), int(config["Input"]["width"]), int(config["Input"]["depth"]))
os.makedirs(dir_save_model, exist_ok=True)
os.makedirs(dir_experimental_result, exist_ok=True)
os.makedirs(dir_logger, exist_ok=True)
dir_inference_output = config["Path"]["dir_inference_output"]
dir_inference_output = os.path.join(dir_inference_output, os.path.basename(FLAGS.config_file).replace(".cfg", "")) 
len_final_feature_width = input_shape[1] // dict_width_divisor[architecture]
if FLAGS.mode == "val":
    os.makedirs(dir_inference_output, exist_ok=True)

# set gpu index
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_index

# define network
n_classes = len(utils.list_string_ctc) + 1
if path_pretrained_weight:
    network = small_network_v2_LSTM(input_shape, n_classes)
    network.load_weights(path_pretrained_weight)
    print("model loaded from {}".format(path_pretrained_weight))
    if FLAGS.mode=="train":
        n_gpus = len(gpu_index.split(","))
        network_train = set_ctc_loss(network, max_char_len, lr_start_value, n_gpus > 1)
        if n_gpus > 1:
            network_train = multi_gpu_model(network_train, gpus=n_gpus)
            network_train = output_as_loss(network_train, lr_start_value)
else:
    network = small_network_v2_LSTM(input_shape, n_classes)
    if FLAGS.mode=="train":
        n_gpus = len(gpu_index.split(","))
        network_train = set_ctc_loss(network, max_char_len, lr_start_value, n_gpus > 1)
        if n_gpus > 1:
            network_train = multi_gpu_model(network_train, gpus=n_gpus)
            network_train = output_as_loss(network_train, lr_start_value)

# set logger and tensorboard
if FLAGS.mode=="train":
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
    handler = logging.FileHandler(path_logger)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    tensorboard = utils.CustomTensorBoard(
    log_dir=os.path.join(dir_logger, "tensorboard", os.path.basename(FLAGS.config_file).replace(".cfg", ".log")),
    write_graph=False,
    batch_size=batch_size
    )

# misc 
if FLAGS.mode=="train":
    network.summary()
    tensorboard.set_model(network)  # set tensorboard callback associated with network
    # save network
    with open(os.path.join(dir_save_model, "network.json"), 'w') as f:
        f.write(network.to_json())
    lr_scheduler = utils.LrScheduler(lr_object=network_train.optimizer.lr, lr_start_value=lr_start_value,
                                    lr_min_value=lr_min_value, lr_decay_tolerance=lr_decay_tolerance,
                                    lr_decay_factor=lr_decay_factor, logger=logger, score_func=lambda x:x["loss"])

# split data
path_label=[]
for i in range(1, 10):
    try:
        input_data = eval("dir_training_input_data{}".format(i))
        label = eval("path_training_label{}".format(i))
        list_path_data = utils.all_files_under(input_data)
        list_labels = read_label_txt(label)
        list_tuple_path_label = gen_tuple(list_path_data, list_labels)
        random.seed(1)
        random.shuffle(list_tuple_path_label)
        # list_tuple_path_label = list_tuple_path_label[-len(list_tuple_path_label)//10:] # use 10 percent
        list_tuple_path_label = list_tuple_path_label[len(list_tuple_path_label)//10:] # drop 10 percent
        path_label+=list_tuple_path_label
        print("load {} {} (N={})".format(input_data, label, len(list_tuple_path_label)))
    except:
        pass
if FLAGS.mode == "train":
    train_input_data = [el[0] for el in path_label]
    train_label_data = [el[1] for el in path_label]
    # train_input_data = [el[0] for el in path_label[:len(path_label)//100]] # debugging purpose
    # train_label_data = [el[1] for el in path_label[:len(path_label)//100]] # debugging purpose
elif FLAGS.mode=="val":
    val_input_data = [el[0] for el in path_label[-len(path_label)//100:]]
    val_label_data = [el[1] for el in path_label[-len(path_label)//100:]]

# set iterator
if FLAGS.mode == "train":
    sample_weight = utils.uniform_sample_weight(len(train_label_data))
    train_batch_fetcher = iterator_ctc_shared_array.BatchFetcher((train_input_data, train_label_data), sample_weight,
                                           utils.ocr_ctc_processing_func_train, batch_size, sample=True, replace=True, shared_array_shape=[input_shape, (max_char_len, )], max_char_len=max_char_len)
elif FLAGS.mode == "val":
    sample_weight = utils.uniform_sample_weight(len(val_label_data))
    val_batch_fetcher = iterator_ctc_shared_array.BatchFetcher((val_input_data, val_label_data), sample_weight,
                                            utils.ocr_ctc_processing_func_val, batch_size, sample=False, replace=False, shared_array_shape=[input_shape, (max_char_len, )], max_char_len=max_char_len)
# # iterator debugging
# n_iteration = 0
# for data, list_arr in train_batch_fetcher:
#     imgs , labels = list_arr
#     for index in range(imgs.shape[0]):
#         Image.fromarray((imgs[index,...,0]*30+128).astype(np.uint8)).save("debug/check_input_{}_{}.png".format(index, n_iteration))
#     print(utils.code2str(labels[0]))
#     print(n_iteration)
#     if n_iteration==0:
#         exit(1)
#     n_iteration+=1
        
for epoch in range(n_epochs):
    # train loop
    if FLAGS.mode=="train":
        n_itr = 0
        list_training_loss, list_training_acc = [], []
        for data, list_arr in train_batch_fetcher:
            # set input for computing ctc loss
            imgs, labels_categorized = list_arr
            batch_size = len(imgs)
            ctc_target = np.zeros([batch_size])
            input_length = np.ones((batch_size, 1)) * len_final_feature_width
            label_length = np.zeros((batch_size, 1))
            for index, label in enumerate(labels_categorized):
                label_length[index] = len(label[label!=-1])
            # print(input_length[0], label_length, utils.code2str(labels_categorized)[0])
            # Image.fromarray((imgs[0,...,0]*30+128).astype(np.uint8)).save("debug/check_input.png")
            # exit(1)
            tensorboard.draw_imgs("Training Image", epoch, (imgs*30+128).astype(np.uint8), plot_once=True)
            loss = network_train.train_on_batch([imgs, labels_categorized, input_length, label_length], ctc_target)
            print(loss)
            if loss > 10:
                for index in range(batch_size):
                    Image.fromarray((imgs[index,...,0]*30+128).astype(np.uint8)).save("debug/high_loss/itr{}_{}.png".format(n_itr, index))
            sys.stdout.flush()
            
            if n_itr!=0 and n_itr % 100 ==0:
                # save network
                network.save_weights(os.path.join(dir_save_model, "weight_{}itr.h5".format(n_itr)))
            n_itr+=1
            
            utils.stack_list(head1=list_training_loss, tail1=[loss] * len(imgs))
        train_metrics = {"training_ctc_loss":np.mean(list_training_loss)}
        utils.log_summary(logger, phase="training", epoch=epoch, **train_metrics)
        tensorboard.on_epoch_end(epoch, train_metrics)

    # val loop
    if FLAGS.mode=="val":
        dict_wrongly_predicted = {"pred":[], "gt":[]}
        list_dice = []
        for data, list_arr in val_batch_fetcher:
            imgs , labels_categorized = list_arr
            # tensorboard.draw_imgs("Validation Image", epoch, (imgs*30+128).astype(np.uint8), plot_once=True)
            network_out = network.predict(imgs)
            predicted_categorized = ctc_decode(network_out)
            true_str = utils.code2str(labels_categorized) # this returns list
            predicted_str = utils.code2str(predicted_categorized) # this returns list
            list_computed_dice = utils.compute_dice(labels_categorized, predicted_categorized)
            list_dice += list_computed_dice

            if FLAGS.mode=="val": # save wrongly predicted images
                for index in range(len(list_computed_dice)):
                    if list_computed_dice[index] < 1-1e-6: # wrong case
                        Image.fromarray((imgs[index,...,0]*30+128).astype(np.uint8)).save(os.path.join(dir_inference_output, "{}.png".format(len(dict_wrongly_predicted["pred"]))))
                        dict_wrongly_predicted["gt"].append(true_str[index])
                        dict_wrongly_predicted["pred"].append(predicted_str[index])
        val_metrics = {"dice":np.mean(list_dice)}
        print(val_metrics)
        df_failure_cases = pd.DataFrame(dict_wrongly_predicted)
        df_failure_cases.to_csv(os.path.join(dir_inference_output, "failure_cases.csv"), index=False)
        exit(1)

    if FLAGS.mode=="train":
        # adjust learning rate
        lr_scheduler.adjust_lr(epoch, {"loss": np.mean(list_training_loss)})
        
        # save network
        network.save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))
