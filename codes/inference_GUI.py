# -*- coding: utf-8 -*-
# python inference.py
from tkinter import Tk
from tkinter.filedialog import askopenfilename
from tkinter import Button, Frame, Label

# show window and message
root = Tk()
root.title("모두의 OCR")
root.geometry("300x60")
waiting_label = Label(root)
waiting_label.pack()
waiting_label.config(text="프로그램을 기동중입니다. 잠시만 기다려주세요.")
root.update()

import imghdr
import io
import logging
import os
import time
from functools import reduce

import cv2
import numpy as np
import PyPDF2
from keras import backend as K
from keras.models import load_model
from pdf2image import convert_from_path
from PIL import Image
from PyPDF2 import PdfFileReader, PdfFileWriter
from reportlab.lib.colors import Color
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas

# set logger
FORMATTER = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(os.path.join(os.path.dirname(os.path.abspath(__file__)), "history.log"))
handler.setFormatter(FORMATTER)
logger.addHandler(handler)

os.environ['CUDA_VISIBLE_DEVICES'] = "-1" # make sure gpu is not used

pdfmetrics.registerFont(TTFont("NanumMyeongjo", "NanumMyeongjo.ttf"))

def singleton(class_):
    instances = {}
    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return getinstance

@singleton
class InputHandler:
    """
    Class to handle inputs
    """
    def __init__(self):
        self.logger = logging.getLogger()

    def set_fpath(self, input_fpath):
        self.input_fpath = input_fpath
        if imghdr.what(self.input_fpath) is not None: # check if image
            self.input_type = "image"
            self.n_pages = 1
        else:
            try: # check if pdf
                self.n_pages = int(PyPDF2.PdfFileReader(open(self.input_fpath, "rb")).getNumPages())
                self.input_type = "pdf"  
            except:
                self.logger.info("[InputHandler] Input file is not PDF or Image file")

    def iterimages(self):
        """
        Return images in numpy array
        """
        if hasattr(self, 'images'):
            return self.images
        else:
            if self.input_type == "pdf":
                for index_page in range(self.n_pages):
                    PIL_image = convert_from_path(self.input_fpath, first_page=index_page+1, last_page=index_page+1, dpi=300)[0] # convert 1 page
                    img_arr = np.array(PIL_image)
                    self.logger.info("[InputHandler] fetch {}th page".format(index_page+1))
                    yield index_page+1, img_arr
            else:
                self.logger.info("[InputHandler] fetch an image")
                img_arr = cv2.imread(self.input_fpath)
                yield 1, img_arr
            

@singleton
class Localizer:
    """
    Class to localize lines with CNN.
    """
    def __init__(self, model_path, sigma_blur_local_zero_mean=8):
        self.logger = logging.getLogger()
        
        self.input_shape = {"default":(768, 512, 3), "high":(1536, 1024, 3)}
        self.sigma_blur_local_zero_mean = sigma_blur_local_zero_mean
        self.localization_network = load_model(model_path)
        self.input_divisor = 16
        self.min_ch_height = 4

        
    def _rectify_image_mean_shift(self, img, mask_ch_center, mask_ch_region, ch_center_conf_thresh=0.5, ch_region_conf_thresh=0.5, percentile_slop=90, n_mean_shift_itrs=5, return_angle=False):
        """
        Rectify image if rotated using mean-shift algorithm.
        
        Args:
            img: numpy array (RGB channel)
            mask_ch_center: 2d numpy array (confidence map for character center)
            mask_ch_region: 2d numpy array (confidence map for character region)
            ch_center_conf_thresh: float (threshold for mask_ch_center)
            ch_region_conf_thresh: float (threshold for mask_ch_region)
            percentile_slop: int (top X % by n_pixels will be used for estimating slope)
            ratio_majority: float (definition of majority to compute the final slope)
            n_mean_shift_itrs: int (the number of iterations for mean-shift)
            return_angle: bool
            
        Returns:
            rectified_img: numpy array (RGB channel) of same size with img
            rectified_mask_ch_center: 2d numpy array (confidence map for character center)  of same size with mask_ch_center
            rectified_mask_ch_region: 2d numpy array (confidence map for character center)  of same size with mask_ch_region
            angle: float (if return_angle is set to True)
        """
        # separate into segments
        ch_center_binary_mask = (mask_ch_center > ch_center_conf_thresh).astype(np.uint8)
        ch_region_binary_mask = (mask_ch_region > ch_region_conf_thresh).astype(np.uint8)
        n_blobs, blobs_labels = cv2.connectedComponents(ch_region_binary_mask)
        # collect n_pixels and degree
        list_tuples = []
        for blob_index in range(1, n_blobs):
            coords = np.where(blobs_labels==blob_index)
            arr_y, arr_x = coords
            mask_ch_center_for_the_region = ch_center_binary_mask[coords]
            if len(mask_ch_center_for_the_region[mask_ch_center_for_the_region > 0]) > 0:
                n_elements = len(arr_y)
                slope = 1.*(n_elements*np.sum(arr_y*arr_x) - np.sum(arr_y)*np.sum(arr_x)) / (n_elements*np.sum(arr_x*arr_x) - np.sum(arr_x)**2)
                degree = np.rad2deg(np.arctan(slope))
                list_tuples.append((n_elements, degree))
        if len(list_tuples) > 0:
            # collect degree of top X % segments by n_pixels (X=)
            list_seg_degree = []
            n_threshold = np.percentile([el[0] for el in list_tuples], percentile_slop)
            for n_elements, degree in list_tuples:
                if n_elements > n_threshold:
                    list_seg_degree.append(degree)
            
            if len(list_seg_degree) > 0:
                # find the mean with mean-shift algorithm
                arr_seg_degree = np.array(list_seg_degree)
                r_angle = np.mean(list_seg_degree)
                for _ in range(n_mean_shift_itrs):
                    kernel = 1./(np.abs(arr_seg_degree-r_angle)+1)
                    r_angle = np.sum(kernel*arr_seg_degree)/np.sum(kernel)
            else:
                r_angle = 0
        else:
            r_angle = 0
            
        # rotate back to rectify the image
        rectified_img = cv2.warpAffine(img, cv2.getRotationMatrix2D((img.shape[1]//2, img.shape[0]//2), r_angle, 1), (img.shape[1], img.shape[0]), borderValue=np.mean(img, axis=(0,1)))
        rectified_mask_ch_center = cv2.warpAffine(mask_ch_center, cv2.getRotationMatrix2D((mask_ch_center.shape[1]//2, mask_ch_center.shape[0]//2), r_angle, 1), (mask_ch_center.shape[1], mask_ch_center.shape[0]))
        rectified_mask_ch_region = cv2.warpAffine(mask_ch_region, cv2.getRotationMatrix2D((mask_ch_region.shape[1]//2, mask_ch_region.shape[0]//2), r_angle, 1), (mask_ch_region.shape[1], mask_ch_region.shape[0]))

        if return_angle:
            return rectified_img, rectified_mask_ch_center, rectified_mask_ch_region, r_angle
        else:
            return rectified_img, rectified_mask_ch_center, rectified_mask_ch_region


    def _possibly_inline(self, coord_anchor, coord_cand, limit_ratio_height_increase):
        """
        Check if coord_cand can be a candidate for an element in the same line

        Args:
            coord_anchor (int, int, int, int): staring coordinate (xmin, xmax, ymin, ymax) that acts as an anchor 
            coord_cand (int, int, int, int): candidate coordinate (xmin, xmax, ymin, ymax)
            limit_ratio_height_increase (int): tolerance in terms of the ratio of height when considered to be in the same line

        Returns: 
            whether or not coord_cand can be a candidate
        """
        xmin_anchor, xmax_anchor, ymin_anchor, ymax_anchor = coord_anchor
        xmin_cand, xmax_cand, ymin_cand, ymax_cand = coord_cand
        height_anchor = ymax_anchor - ymin_anchor
        
        return limit_ratio_height_increase * height_anchor > max(ymax_anchor, ymax_cand) - min(ymin_anchor, ymin_cand)

    def _rotate_counterclockwise(self, pt, angle):
        """
        Rotate the point by theta (radians) counterclockwise

        Args:
            pt: (x, y)
            angle: float (radians)
        Return:
            rotated pt: (x, y)
        """
        x, y = pt
        x_rotated = x * np.cos(angle) + y * np.sin(angle)
        y_rotated = - x * np.sin(angle) + y * np.cos(angle)
        rotated_pt = (x_rotated, y_rotated)
        return rotated_pt

    def _cvt_pt(self, pt_origin, size_origin, size_target, r_angle, is_origin_resized):
        """
        Convert a point between coordinates in one image to the other.
        The resized image is maintained the width:height ratio and right and bottom areas are padded.
        
        Args:
            pt_origin: (x, y)
            size_origin: (h, w)
            size_target: (h, w)
            r_angle (float): degree with which the original image is rotated counterclockwise
            is_origin_resized (bool): whether the origin is the resized image
        Return:
            pt_ori: (x, y)
        """
        h_target, w_target = size_target
        h_origin, w_origin = size_origin
        x_origin, y_origin = pt_origin
        scale_ratio = min(1. * h_origin / h_target, 1. * w_origin / w_target) if is_origin_resized else max(1. * h_origin / h_target, 1. * w_origin / w_target)
        cy_origin, cx_origin = h_origin // 2, w_origin // 2
        cy_target, cx_target = h_target // 2, w_target // 2
        r_rad = np.radians(r_angle)
        
        # rotate back
        pt_target = self._rotate_counterclockwise((x_origin-cx_origin, y_origin-cy_origin), -r_rad)
        pt_target = (pt_target[0]+cx_origin, pt_target[1]+cy_origin)
        # rescale to target size
        pt_target = (int(1.*pt_target[0]/scale_ratio), int(1.*pt_target[1]/scale_ratio))
        # rotate in target image
        pt_target = self._rotate_counterclockwise((pt_target[0]-cx_target, pt_target[1]-cy_target), r_rad)
        pt_target = (pt_target[0]+cx_target, pt_target[1]+cy_target)
            
        return pt_target

    def _is_overlapping(self, coord1, coord2):
        xmin1, xmax1, ymin1, ymax1 = coord1
        xmin2, xmax2, ymin2, ymax2 = coord2
        non_overlap = xmax1 <= xmin2 or xmax2 <= xmin1 or ymax1 <= ymin2 or ymax2 <= ymin1
        return not non_overlap

    def _find_lines(self, list_bbox_ordinary_words, list_bbox_tight_words, min_ch_height):
        """
        Return coordinates of line segments. 
        Process ordinary words (enough rooms in vertical space) and tight words

        Args:
            list_bbox_ordinary_words: list of (x_min, x_max, y_min, y_max) for orinary words (enough rooms in vertical space). Elements of the list is removed inside the function.
            list_bbox_tight_words: list of (x_min, x_max, y_min, y_max) for tight words. Elements of the list is removed inside the function.
            min_ch_height (int): minimum character height in pixels

        Returns:
            list_coords_lines: a list of coordinates (x_min, x_max, y_min, y_max) for each line.
            list_coords_words: a list of coordinates (x_min, x_max, y_min, y_max) for words in each line
        """
        list_coords_lines, list_coords_words = [], []
        list_iter_settings = [(list_bbox_ordinary_words, 1.2, min_ch_height // 2), (list_bbox_tight_words, 1.1, min_ch_height // 2)]
        set_external_bboxes = set(reduce(lambda x,y:x+y, [list_iter_settings[idx][0] for idx in range(len(list_iter_settings))]))
        for index, setting in enumerate(list_iter_settings):
            list_bbox, limit_ratio_height_increase, safety_margin = setting
            list_bbox = sorted(list_bbox, key=lambda x:x[0]+x[2], reverse=True)
            while len(list_bbox)>0:
                coord_start = coord_line = list_bbox.pop() # get top-left bbox
                set_external_bboxes -= set([coord_start])
                coords_words = [coord_start]
                
                # collect next words assuming 
                # (1) candidates for the next word increases the height by less than X times when formed as a sentence
                # (2) next word is the closest in x coordinate
                # (3) sentences do not overlap other words or sentences
                # (4) no more than 1 character exist for any x coordinate
                list_coords_cands = [coord for coord in list_bbox if self._possibly_inline(coord_start, coord, limit_ratio_height_increase)]
                list_coords_cands = sorted(list_coords_cands, key=lambda x:x[0], reverse=True)
                while len(list_coords_cands)>0:
                    coord_next_word = list_coords_cands[-1]
                    coord_line_estim = (min(coord_line[0], coord_next_word[0]), max(coord_line[1], coord_next_word[1]), min(coord_line[2], coord_next_word[2]), max(coord_line[3], coord_next_word[3]))
                    set_external_bboxes -= set([coord_next_word])
                    if not self._is_overlapping(coord_line, coord_next_word) and not np.array([self._is_overlapping(coord_line_estim, bbox) for bbox in set_external_bboxes]).any() and not np.array([self._is_overlapping(coord_line_estim, bbox) for bbox in list_coords_lines]).any():
                        # update coordinate for the line, entire coords, candidates
                        coord_line = coord_line_estim
                        coords_words.append(coord_next_word)
                        list_bbox.remove(coord_next_word)
                        set_external_bboxes -= set([coord_next_word])
                        list_coords_cands.pop()
                    else:
                        break
                    
                # append results
                list_coords_lines.append(coord_line)
                list_coords_words.append(coords_words)
        
        # add safety margin for lines (do not add safety margin in the loop due to overlapping checks) and xcoords of words
        list_coords_lines = [(el[0]-safety_margin, el[1]+safety_margin, el[2]-safety_margin, el[3]+safety_margin) for el in list_coords_lines]
        for index in range(len(list_coords_lines)):
            list_coords_in_line = list_coords_words[index]
            _, _, ymin, ymax = list_coords_lines[index]
            for index_el in range(len(list_coords_in_line)):
                if index_el==0:
                    el = list_coords_in_line[index_el]
                    list_coords_in_line[index_el] = (el[0]-safety_margin, el[1], ymin, ymax)
                if index_el==len(list_coords_in_line)-1:
                    el = list_coords_in_line[index_el]
                    list_coords_in_line[index_el] = (el[0], el[1]+safety_margin, ymin, ymax)
                if index_el!=0 and index_el!=len(list_coords_in_line)-1:
                    el = list_coords_in_line[index_el]
                    list_coords_in_line[index_el] = (el[0], el[1], ymin, ymax)

        return list_coords_lines, list_coords_words

    def _get_bbox_word(self, mask_ch_center, mask_ch_region, ch_center_conf_thresh, ch_region_conf_thresh, min_ch_height):
        """
        Return coordinates for each line. Split multiple lines if judged as so.
        
        Args:
            mask_ch_center: character center mask
            mask_ch_region: character region mask
            ch_center_conf_thresh: float
            ch_region_conf_thresh: float
            min_ch_height: int
        
        Returns:
            a list of coordinates (xmin, xmax, ymin, ymax) for ordinary words (enough rooms in vertical space)
            a list of coordinates (xmin, xmax, ymin, ymax) for tight words
        """  
        assert mask_ch_center.shape == mask_ch_region.shape
        
        list_bbox_ordinary_words, list_bbox_tight_words = [], []

        # get character regions
        mask_ch_center_threshold = np.zeros(mask_ch_center.shape).astype(np.bool)
        mask_ch_center_threshold[mask_ch_center > ch_center_conf_thresh] = 1
        mask_ch_region_threshold = np.zeros(mask_ch_region.shape).astype(np.bool)
        mask_ch_region_threshold[mask_ch_region > ch_region_conf_thresh] = 1
        n_blobs, blobs_labels = cv2.connectedComponents(mask_ch_region_threshold.astype(np.uint8))
        # collect coords of each word region
        list_coords_words = []
        for fg_index in range(1, n_blobs):
            coords = np.where(blobs_labels==fg_index)
            y_arr, x_arr = coords[0], coords[1]
            x_min, x_max, y_min, y_max = np.min(x_arr), np.max(x_arr), np.min(y_arr), np.max(y_arr)
            if y_max-y_min > min_ch_height and x_max - x_min > min_ch_height // 2: # process regions with enough height and width
                list_y_chc_bbox=[]
                mask_ch_center_threshold_inside_bbox = mask_ch_center_threshold[y_min:y_max, x_min:x_max]
                if len(mask_ch_center_threshold_inside_bbox[mask_ch_center_threshold_inside_bbox > 0]) > 0: # process if ch center exist
                    
                    # inspect center mask inside the region mask blob
                    n_blobs_center_inside_bbox, blobs_labels_center_inside_bbox = cv2.connectedComponents(mask_ch_center_threshold_inside_bbox.astype(np.uint8))
                    for center_fg_index in range(1, n_blobs_center_inside_bbox):
                        coords_center = np.where(blobs_labels_center_inside_bbox==center_fg_index)
                        y_center_arr, x_center_arr = coords_center[0], coords_center[1]
                        list_y_chc_bbox.append(int(np.mean(y_center_arr)))
                    
                    # split line if y coordinates differ too much (y coordinates inside the bbox)
                    list_y_chc_bbox = sorted(list_y_chc_bbox)
                    curr_y = list_y_chc_bbox[0]
                    list_y_bbox_rep = [curr_y]
                    for y_bbox in list_y_chc_bbox:
                        if y_bbox-curr_y > min_ch_height:
                            list_y_bbox_rep.append(y_bbox)
                            curr_y=y_bbox
                    
                    if len(list_y_bbox_rep) > 1: # multiple lines
                        mask_region_inside_bbox = mask_ch_region[y_min:y_max, x_min:x_max]
                        list_y_split = [y_min]
                        for i in range(len(list_y_bbox_rep)-1): 
                            y_bbox_min_prob = list_y_bbox_rep[i] + np.argmin([np.sum(mask_region_inside_bbox[y_bbox, :]) for y_bbox in range(list_y_bbox_rep[i], list_y_bbox_rep[i+1]+1)])
                            list_y_split.append(y_bbox_min_prob+y_min)
                        list_y_split.append(y_max)
                        for i in range(len(list_y_split)-1):
                            list_bbox_tight_words.append((x_min, x_max, list_y_split[i], list_y_split[i+1]))
                    else: 
                        list_bbox_ordinary_words.append((x_min, x_max, y_min, y_max))
        return list_bbox_ordinary_words, list_bbox_tight_words

    def _get_coords_line(self, rectified_img_ori, mask_ch_center, mask_ch_region, min_ch_height, ch_center_conf_thresh=0.3, ch_region_conf_thresh=0.1):
        """
        Return coordinates for each line. Split multiple lines if judged as so.
        
        Args:
            rectified_img_ori: rectified image
            mask_ch_center: rectified character center mask (resized)
            mask_ch_region: rectified character region mask (resized)
            min_ch_height: int
            ch_center_conf_thresh: float
            ch_region_conf_thresh: float
        
        Returns:
            a list of coordinates (xmin, xmax, ymin, ymax) for each line
        """
        assert mask_ch_center.shape == mask_ch_region.shape

        list_bbox_ordinary_words, list_bbox_tight_words = self._get_bbox_word(mask_ch_center, mask_ch_region, ch_center_conf_thresh=0.3, ch_region_conf_thresh=0.1, min_ch_height = min_ch_height)
        
        list_coords_line, list_coords_words = self._find_lines(list_bbox_ordinary_words, list_bbox_tight_words, min_ch_height=min_ch_height)

        return list_coords_line, list_coords_words

    def _run_network(self, img):
        """
        Args:
            img: 3-channel numpy array (h,w,3)
        
        Returns: 
            network output
        """
        preprocessed_img = 1.*cv2.addWeighted (img, 4, cv2.GaussianBlur(img , (0, 0) , self.sigma_blur_local_zero_mean) , -4 , 128) / 128
        network_input = np.expand_dims(preprocessed_img, axis=0)
        mask_ch_center, mask_ch_region = self.localization_network.predict(network_input)
        return mask_ch_center, mask_ch_region

    def _good_quality(self, mask_ch_center, ch_center_conf_thresh=0.3, threshold_abnormal_raito=0.01):
        """
        Check if mask of character centers are square.

        Args:
            mask_ch_center: numpy array (1-dimensional)
            ch_center_conf_thresh: float
            threshold_abnormal_raito: threshold for the ratio of vertically long character center (height to width ratio > 2)
        Returns:
            whether the segmentation is done well enough (bool)
        """
        ch_center_binary_mask = (mask_ch_center > ch_center_conf_thresh).astype(np.uint8)
        n_blobs, blobs_labels = cv2.connectedComponents(ch_center_binary_mask)
        # collect height to width ratio
        list_h_w_ratio = []
        for blob_index in range(1, n_blobs):
            coords = np.where(blobs_labels==blob_index)
            arr_y, arr_x = coords
            width = np.max(arr_x)-np.min(arr_x)
            height = np.max(arr_y)-np.min(arr_y)
            if height > 0 and width > 0:
                list_h_w_ratio.append(1.*height/width)
        arr_h_w_ratio = np.array(list_h_w_ratio)
        isgood = True if len(arr_h_w_ratio) > 0 and 1.*len(arr_h_w_ratio[arr_h_w_ratio>2])/len(arr_h_w_ratio) < threshold_abnormal_raito else False
        return isgood

    def _float2int(self, val):
        return int(np.round(val))

    def localize_lines(self, img_arr, return_intermediate_results=False):
        """
        Return a rectified image and bounding boxes from a rectified image array at the same scale of the input image. 

        Args:
            img_arr: numpy array (RGB channel)
            return_intermediate_results (bool): whether to return outputs of localization network and rotated angle
        Returns:
            rectified img: numpy array of the rectified image (RGB channel)
            coords_line_bboxes: list of bboxes for characters
        """
        h, w, _ = img_arr.shape
        for input_resolution in ["default", "high"]:
            # resize the image maintaining the width-height ratio
            scale_ratio = min(1. * self.input_shape[input_resolution][0] / h, 1. * self.input_shape[input_resolution][1] / w)
            target_size = (int(w * scale_ratio), int(h * scale_ratio))
            resized_img_arr = cv2.resize(img_arr, target_size, interpolation=cv2.INTER_LINEAR)
            padded_resized_img_arr = (np.ones(self.input_shape[input_resolution]) * np.mean(img_arr, axis=(0,1))).astype(np.uint8)
            resized_h, resized_w, _ = resized_img_arr.shape
            padded_resized_img_arr[:resized_h, :resized_w,...] = resized_img_arr
            
            # run localization network
            mask_ch_center, mask_ch_region = self._run_network(padded_resized_img_arr)
            # for debugging purpose
            # from PIL import Image
            # Image.fromarray((mask_ch_center[0,...,0]*255).astype(np.uint8)).save("debug/center.png")
            # Image.fromarray((mask_ch_region[0,...,0]*255).astype(np.uint8)).save("debug/region.png")
            
            if self._good_quality(mask_ch_center[0,...,0]):
                break
        
        # recitify image and get coordinates of character boxes (empirically rotating twice reduces error)
        rectified_img, rectified_mask_ch_center, rectified_mask_ch_region, r_angle_1st = self._rectify_image_mean_shift(padded_resized_img_arr, mask_ch_center[0,...,0], mask_ch_region[0,...,0], return_angle=True)
        rectified_img, rectified_mask_ch_center, rectified_mask_ch_region, r_angle_2nd = self._rectify_image_mean_shift(rectified_img, rectified_mask_ch_center, rectified_mask_ch_region, return_angle=True)
        r_angle = r_angle_1st+r_angle_2nd
        rectified_ori_img = cv2.warpAffine(img_arr, cv2.getRotationMatrix2D((w//2, h//2), r_angle, 1), (w, h), borderValue=np.mean(img_arr, axis=(0,1)))
        self.logger.info("[Localizer] degree of rotation in original input clock-wise: {}".format(r_angle))

        # get coords (x_min, x_max, y_min, y_max) of each line
        coords_line, coords_words = self._get_coords_line(rectified_ori_img, rectified_mask_ch_center, rectified_mask_ch_region, self.min_ch_height if input_resolution=="default" else 2*self.min_ch_height)

        # convert coords accordingly
        list_coords_line_in_rectified_ori_img = []
        for coords in coords_line:
            x_min, x_max, y_min, y_max = coords
            topleft_scaled_rotated_ori_scale = self._cvt_pt((x_min, y_min), self.input_shape[input_resolution][:2], (h, w), r_angle, is_origin_resized=True)
            bottomright_scaled_rotated_ori_scale = self._cvt_pt((x_max, y_max), self.input_shape[input_resolution][:2], (h, w), r_angle, is_origin_resized=True)
            modified_x_min, modified_y_min = topleft_scaled_rotated_ori_scale
            modified_x_max, modified_y_max = bottomright_scaled_rotated_ori_scale
            modified_x_min, modified_x_max = np.clip(modified_x_min, 0, w), np.clip(modified_x_max, 0, w)
            modified_y_min, modified_y_max = np.clip(modified_y_min, 0, h), np.clip(modified_y_max, 0, h)
            list_coords_line_in_rectified_ori_img.append((self._float2int(modified_x_min), self._float2int(modified_x_max), self._float2int(modified_y_min), self._float2int(modified_y_max)))
        list_coords_words_in_rectified_ori_img = []
        for coords_words_in_line in coords_words:
            coords_cvt_in_line = []
            for coords in coords_words_in_line:
                x_min, x_max, y_min, y_max = coords
                topleft_scaled_rotated_ori_scale = self._cvt_pt((x_min, y_min), self.input_shape[input_resolution][:2], (h, w), r_angle, is_origin_resized=True)
                bottomright_scaled_rotated_ori_scale = self._cvt_pt((x_max, y_max), self.input_shape[input_resolution][:2], (h, w), r_angle, is_origin_resized=True)
                modified_x_min, modified_y_min = topleft_scaled_rotated_ori_scale
                modified_x_max, modified_y_max = bottomright_scaled_rotated_ori_scale
                modified_x_min, modified_x_max = np.clip(modified_x_min, 0, w), np.clip(modified_x_max, 0, w)
                modified_y_min, modified_y_max = np.clip(modified_y_min, 0, h), np.clip(modified_y_max, 0, h)
                coords_cvt_in_line.append((self._float2int(modified_x_min), self._float2int(modified_x_max), self._float2int(modified_y_min), self._float2int(modified_y_max)))
            list_coords_words_in_rectified_ori_img.append(coords_cvt_in_line)

        if return_intermediate_results:
            return rectified_ori_img, list_coords_line_in_rectified_ori_img, list_coords_words_in_rectified_ori_img, mask_ch_center, mask_ch_region, r_angle
        else:
            return rectified_ori_img, list_coords_line_in_rectified_ori_img, list_coords_words_in_rectified_ori_img

@singleton
class Interpreter:
    """
    Return strings for a given sentence image.
    """
    def __init__(self, model_path, input_shape=(64, 1024, 1), sigma_blur_local_zero_mean=8):
        # set logger
        self.logger = logging.getLogger()

        self.input_shape = input_shape
        self.sigma_blur_local_zero_mean = sigma_blur_local_zero_mean
        self.list_chars_ctc = ['가', '각', '간', '갇', '갈', '갉', '갊', '감', '갑', '값', '갓', '갔', '강', '갖', '갗', '같', '갚', '갛', '개', '객', '갠', '갤', '갬', '갭', '갯', '갰', '갱', '갸', '갹', '갼', '걀', '걋', '걍', '걔', '걘', '걜', '거', '걱', '건', '걷', '걸', '걺', '검', '겁', '것', '겄', '겅', '겆', '겉', '겊', '겋', '게', '겐', '겔', '겜', '겝', '겟', '겠', '겡', '겨', '격', '겪', '견', '겯', '결', '겸', '겹', '겻', '겼', '경', '곁', '계', '곈', '곌', '곕', '곗', '고', '곡', '곤', '곧', '골', '곪', '곬', '곯', '곰', '곱', '곳', '공', '곶', '과', '곽', '관', '괄', '괆', '괌', '괍', '괏', '광', '괘', '괜', '괠', '괩', '괬', '괭', '괴', '괵', '괸', '괼', '굄', '굅', '굇', '굉', '교', '굔', '굘', '굡', '굣', '구', '국', '군', '굳', '굴', '굵', '굶', '굻', '굼', '굽', '굿', '궁', '궂', '궈', '궉', '권', '궐', '궜', '궝', '궤', '궷', '귀', '귁', '귄', '귈', '귐', '귑', '귓', '규', '균', '귤', '그', '극', '근', '귿', '글', '긁', '금', '급', '긋', '긍', '긔', '기', '긱', '긴', '긷', '길', '긺', '김', '깁', '깃', '깅', '깆', '깊', '까', '깍', '깎', '깐', '깔', '깖', '깜', '깝', '깟', '깠', '깡', '깥', '깨', '깩', '깬', '깰', '깸', '깹', '깻', '깼', '깽', '꺄', '꺅', '꺌', '꺼', '꺽', '꺾', '껀', '껄', '껌', '껍', '껏', '껐', '껑', '께', '껙', '껜', '껨', '껫', '껭', '껴', '껸', '껼', '꼇', '꼈', '꼍', '꼐', '꼬', '꼭', '꼰', '꼲', '꼴', '꼼', '꼽', '꼿', '꽁', '꽂', '꽃', '꽈', '꽉', '꽐', '꽜', '꽝', '꽤', '꽥', '꽹', '꾀', '꾄', '꾈', '꾐', '꾑', '꾕', '꾜', '꾸', '꾹', '꾼', '꿀', '꿇', '꿈', '꿉', '꿋', '꿍', '꿎', '꿔', '꿜', '꿨', '꿩', '꿰', '꿱', '꿴', '꿸', '뀀', '뀁', '뀄', '뀌', '뀐', '뀔', '뀜', '뀝', '뀨', '끄', '끅', '끈', '끊', '끌', '끎', '끓', '끔', '끕', '끗', '끙', '끝', '끼', '끽', '낀', '낄', '낌', '낍', '낏', '낑', '나', '낙', '낚', '난', '낟', '날', '낡', '낢', '남', '납', '낫', '났', '낭', '낮', '낯', '낱', '낳', '내', '낵', '낸', '낼', '냄', '냅', '냇', '냈', '냉', '냐', '냑', '냔', '냘', '냠', '냥', '너', '넉', '넋', '넌', '널', '넒', '넓', '넘', '넙', '넛', '넜', '넝', '넣', '네', '넥', '넨', '넬', '넴', '넵', '넷', '넸', '넹', '녀', '녁', '년', '녈', '념', '녑', '녔', '녕', '녘', '녜', '녠', '노', '녹', '논', '놀', '놂', '놈', '놉', '놋', '농', '높', '놓', '놔', '놘', '놜', '놨', '뇌', '뇐', '뇔', '뇜', '뇝', '뇟', '뇨', '뇩', '뇬', '뇰', '뇹', '뇻', '뇽', '누', '눅', '눈', '눋', '눌', '눔', '눕', '눗', '눙', '눠', '눴', '눼', '뉘', '뉜', '뉠', '뉨', '뉩', '뉴', '뉵', '뉼', '늄', '늅', '늉', '느', '늑', '는', '늘', '늙', '늚', '늠', '늡', '늣', '능', '늦', '늪', '늬', '늰', '늴', '니', '닉', '닌', '닐', '닒', '님', '닙', '닛', '닝', '닢', '다', '닥', '닦', '단', '닫', '달', '닭', '닮', '닯', '닳', '담', '답', '닷', '닸', '당', '닺', '닻', '닿', '대', '댁', '댄', '댈', '댐', '댑', '댓', '댔', '댕', '댜', '더', '덕', '덖', '던', '덛', '덜', '덞', '덟', '덤', '덥', '덧', '덩', '덫', '덮', '데', '덱', '덴', '델', '뎀', '뎁', '뎃', '뎄', '뎅', '뎌', '뎐', '뎔', '뎠', '뎡', '뎨', '뎬', '도', '독', '돈', '돋', '돌', '돎', '돐', '돔', '돕', '돗', '동', '돛', '돝', '돠', '돤', '돨', '돼', '됐', '되', '된', '될', '됨', '됩', '됫', '됴', '두', '둑', '둔', '둘', '둠', '둡', '둣', '둥', '둬', '뒀', '뒈', '뒝', '뒤', '뒨', '뒬', '뒵', '뒷', '뒹', '듀', '듄', '듈', '듐', '듕', '드', '득', '든', '듣', '들', '듦', '듬', '듭', '듯', '등', '듸', '디', '딕', '딘', '딛', '딜', '딤', '딥', '딧', '딨', '딩', '딪', '따', '딱', '딴', '딸', '땀', '땁', '땃', '땄', '땅', '땋', '때', '땍', '땐', '땔', '땜', '땝', '땟', '땠', '땡', '떠', '떡', '떤', '떨', '떪', '떫', '떰', '떱', '떳', '떴', '떵', '떻', '떼', '떽', '뗀', '뗄', '뗌', '뗍', '뗏', '뗐', '뗑', '뗘', '뗬', '또', '똑', '똔', '똘', '똥', '똬', '똴', '뙈', '뙤', '뙨', '뚜', '뚝', '뚠', '뚤', '뚫', '뚬', '뚱', '뛔', '뛰', '뛴', '뛸', '뜀', '뜁', '뜅', '뜨', '뜩', '뜬', '뜯', '뜰', '뜸', '뜹', '뜻', '띄', '띈', '띌', '띔', '띕', '띠', '띤', '띨', '띰', '띱', '띳', '띵', '라', '락', '란', '랄', '람', '랍', '랏', '랐', '랑', '랒', '랖', '랗', '래', '랙', '랜', '랠', '램', '랩', '랫', '랬', '랭', '랴', '략', '랸', '럇', '량', '러', '럭', '런', '럴', '럼', '럽', '럿', '렀', '렁', '렇', '레', '렉', '렌', '렐', '렘', '렙', '렛', '렝', '려', '력', '련', '렬', '렴', '렵', '렷', '렸', '령', '례', '롄', '롑', '롓', '로', '록', '론', '롤', '롬', '롭', '롯', '롱', '롸', '롼', '뢍', '뢨', '뢰', '뢴', '뢸', '룀', '룁', '룃', '룅', '료', '룐', '룔', '룝', '룟', '룡', '루', '룩', '룬', '룰', '룸', '룹', '룻', '룽', '뤄', '뤘', '뤠', '뤼', '뤽', '륀', '륄', '륌', '륏', '륑', '류', '륙', '륜', '률', '륨', '륩', '륫', '륭', '르', '륵', '른', '를', '름', '릅', '릇', '릉', '릊', '릍', '릎', '리', '릭', '린', '릴', '림', '립', '릿', '링', '마', '막', '만', '많', '맏', '말', '맑', '맒', '맘', '맙', '맛', '망', '맞', '맡', '맣', '매', '맥', '맨', '맬', '맴', '맵', '맷', '맸', '맹', '맺', '먀', '먁', '먈', '먕', '머', '먹', '먼', '멀', '멂', '멈', '멉', '멋', '멍', '멎', '멓', '메', '멕', '멘', '멜', '멤', '멥', '멧', '멨', '멩', '며', '멱', '면', '멸', '몃', '몄', '명', '몇', '몌', '모', '목', '몫', '몬', '몰', '몲', '몸', '몹', '못', '몽', '뫄', '뫈', '뫘', '뫙', '뫼', '묀', '묄', '묍', '묏', '묑', '묘', '묜', '묠', '묩', '묫', '무', '묵', '묶', '문', '묻', '물', '묽', '묾', '뭄', '뭅', '뭇', '뭉', '뭍', '뭏', '뭐', '뭔', '뭘', '뭡', '뭣', '뭬', '뮈', '뮌', '뮐', '뮤', '뮨', '뮬', '뮴', '뮷', '므', '믄', '믈', '믐', '믓', '미', '믹', '민', '믿', '밀', '밂', '밈', '밉', '밋', '밌', '밍', '및', '밑', '바', '박', '밖', '밗', '반', '받', '발', '밝', '밞', '밟', '밤', '밥', '밧', '방', '밭', '배', '백', '밴', '밸', '뱀', '뱁', '뱃', '뱄', '뱅', '뱉', '뱌', '뱍', '뱐', '뱝', '버', '벅', '번', '벋', '벌', '벎', '범', '법', '벗', '벙', '벚', '베', '벡', '벤', '벧', '벨', '벰', '벱', '벳', '벴', '벵', '벼', '벽', '변', '별', '볍', '볏', '볐', '병', '볕', '볘', '볜', '보', '복', '볶', '본', '볼', '봄', '봅', '봇', '봉', '봐', '봔', '봤', '봬', '뵀', '뵈', '뵉', '뵌', '뵐', '뵘', '뵙', '뵤', '뵨', '부', '북', '분', '붇', '불', '붉', '붊', '붐', '붑', '붓', '붕', '붙', '붚', '붜', '붤', '붰', '붸', '뷔', '뷕', '뷘', '뷜', '뷩', '뷰', '뷴', '뷸', '븀', '븃', '븅', '브', '븍', '븐', '블', '븜', '븝', '븟', '비', '빅', '빈', '빌', '빎', '빔', '빕', '빗', '빙', '빚', '빛', '빠', '빡', '빤', '빨', '빪', '빰', '빱', '빳', '빴', '빵', '빻', '빼', '빽', '뺀', '뺄', '뺌', '뺍', '뺏', '뺐', '뺑', '뺘', '뺙', '뺨', '뻐', '뻑', '뻔', '뻗', '뻘', '뻠', '뻣', '뻤', '뻥', '뻬', '뼁', '뼈', '뼉', '뼘', '뼙', '뼛', '뼜', '뼝', '뽀', '뽁', '뽄', '뽈', '뽐', '뽑', '뽕', '뾔', '뾰', '뿅', '뿌', '뿍', '뿐', '뿔', '뿜', '뿟', '뿡', '쀼', '쁑', '쁘', '쁜', '쁠', '쁨', '쁩', '삐', '삑', '삔', '삘', '삠', '삡', '삣', '삥', '사', '삭', '삯', '산', '삳', '살', '삵', '삶', '삼', '삽', '삿', '샀', '상', '샅', '새', '색', '샌', '샐', '샘', '샙', '샛', '샜', '생', '샤', '샥', '샨', '샬', '샴', '샵', '샷', '샹', '섀', '섄', '섈', '섐', '섕', '서', '석', '섞', '섟', '선', '섣', '설', '섦', '섧', '섬', '섭', '섯', '섰', '성', '섶', '세', '섹', '센', '셀', '셈', '셉', '셋', '셌', '셍', '셔', '셕', '션', '셜', '셤', '셥', '셧', '셨', '셩', '셰', '셴', '셸', '솅', '소', '속', '솎', '손', '솔', '솖', '솜', '솝', '솟', '송', '솥', '솨', '솩', '솬', '솰', '솽', '쇄', '쇈', '쇌', '쇔', '쇗', '쇘', '쇠', '쇤', '쇨', '쇰', '쇱', '쇳', '쇼', '쇽', '숀', '숄', '숌', '숍', '숏', '숑', '수', '숙', '순', '숟', '술', '숨', '숩', '숫', '숭', '숯', '숱', '숲', '숴', '쉈', '쉐', '쉑', '쉔', '쉘', '쉠', '쉥', '쉬', '쉭', '쉰', '쉴', '쉼', '쉽', '쉿', '슁', '슈', '슉', '슐', '슘', '슛', '슝', '스', '슥', '슨', '슬', '슭', '슴', '습', '슷', '승', '시', '식', '신', '싣', '실', '싫', '심', '십', '싯', '싱', '싶', '싸', '싹', '싻', '싼', '쌀', '쌈', '쌉', '쌌', '쌍', '쌓', '쌔', '쌕', '쌘', '쌜', '쌤', '쌥', '쌨', '쌩', '썅', '써', '썩', '썬', '썰', '썲', '썸', '썹', '썼', '썽', '쎄', '쎈', '쎌', '쏀', '쏘', '쏙', '쏜', '쏟', '쏠', '쏢', '쏨', '쏩', '쏭', '쏴', '쏵', '쏸', '쐈', '쐐', '쐤', '쐬', '쐰', '쐴', '쐼', '쐽', '쑈', '쑤', '쑥', '쑨', '쑬', '쑴', '쑵', '쑹', '쒀', '쒔', '쒜', '쒸', '쒼', '쓩', '쓰', '쓱', '쓴', '쓸', '쓺', '쓿', '씀', '씁', '씌', '씐', '씔', '씜', '씨', '씩', '씬', '씰', '씸', '씹', '씻', '씽', '아', '악', '안', '앉', '않', '알', '앍', '앎', '앓', '암', '압', '앗', '았', '앙', '앝', '앞', '애', '액', '앤', '앨', '앰', '앱', '앳', '앴', '앵', '야', '약', '얀', '얄', '얇', '얌', '얍', '얏', '양', '얕', '얗', '얘', '얜', '얠', '얩', '어', '억', '언', '얹', '얻', '얼', '얽', '얾', '엄', '업', '없', '엇', '었', '엉', '엊', '엌', '엎', '에', '엑', '엔', '엘', '엠', '엡', '엣', '엥', '여', '역', '엮', '연', '열', '엶', '엷', '염', '엽', '엾', '엿', '였', '영', '옅', '옆', '옇', '예', '옌', '옐', '옘', '옙', '옛', '옜', '오', '옥', '온', '올', '옭', '옮', '옰', '옳', '옴', '옵', '옷', '옹', '옻', '와', '왁', '완', '왈', '왐', '왑', '왓', '왔', '왕', '왜', '왝', '왠', '왬', '왯', '왱', '외', '왹', '왼', '욀', '욈', '욉', '욋', '욍', '요', '욕', '욘', '욜', '욤', '욥', '욧', '용', '우', '욱', '운', '울', '욹', '욺', '움', '웁', '웃', '웅', '워', '웍', '원', '월', '웜', '웝', '웠', '웡', '웨', '웩', '웬', '웰', '웸', '웹', '웽', '위', '윅', '윈', '윌', '윔', '윕', '윗', '윙', '유', '육', '윤', '율', '윰', '윱', '윳', '융', '윷', '으', '윽', '은', '을', '읊', '음', '읍', '읏', '응', '읒', '읓', '읔', '읕', '읖', '읗', '의', '읜', '읠', '읨', '읫', '이', '익', '인', '일', '읽', '읾', '잃', '임', '입', '잇', '있', '잉', '잊', '잎', '자', '작', '잔', '잖', '잗', '잘', '잚', '잠', '잡', '잣', '잤', '장', '잦', '재', '잭', '잰', '잴', '잼', '잽', '잿', '쟀', '쟁', '쟈', '쟉', '쟌', '쟎', '쟐', '쟘', '쟝', '쟤', '쟨', '쟬', '저', '적', '전', '절', '젊', '점', '접', '젓', '정', '젖', '제', '젝', '젠', '젤', '젬', '젭', '젯', '젱', '져', '젼', '졀', '졈', '졉', '졌', '졍', '졔', '조', '족', '존', '졸', '졺', '좀', '좁', '좃', '종', '좆', '좇', '좋', '좌', '좍', '좔', '좝', '좟', '좡', '좨', '좼', '좽', '죄', '죈', '죌', '죔', '죕', '죗', '죙', '죠', '죡', '죤', '죵', '주', '죽', '준', '줄', '줅', '줆', '줌', '줍', '줏', '중', '줘', '줬', '줴', '쥐', '쥑', '쥔', '쥘', '쥠', '쥡', '쥣', '쥬', '쥰', '쥴', '쥼', '즈', '즉', '즌', '즐', '즘', '즙', '즛', '증', '지', '직', '진', '짇', '질', '짊', '짐', '집', '짓', '징', '짖', '짙', '짚', '짜', '짝', '짠', '짢', '짤', '짧', '짬', '짭', '짯', '짰', '짱', '째', '짹', '짼', '쨀', '쨈', '쨉', '쨋', '쨌', '쨍', '쨔', '쨘', '쨩', '쩌', '쩍', '쩐', '쩔', '쩜', '쩝', '쩟', '쩠', '쩡', '쩨', '쩽', '쪄', '쪘', '쪼', '쪽', '쫀', '쫄', '쫌', '쫍', '쫏', '쫑', '쫓', '쫘', '쫙', '쫠', '쫬', '쫴', '쬈', '쬐', '쬔', '쬘', '쬠', '쬡', '쭁', '쭈', '쭉', '쭌', '쭐', '쭘', '쭙', '쭝', '쭤', '쭸', '쭹', '쮜', '쮸', '쯔', '쯤', '쯧', '쯩', '찌', '찍', '찐', '찔', '찜', '찝', '찡', '찢', '찧', '차', '착', '찬', '찮', '찰', '참', '찹', '찻', '찼', '창', '찾', '채', '책', '챈', '챌', '챔', '챕', '챗', '챘', '챙', '챠', '챤', '챦', '챨', '챰', '챵', '처', '척', '천', '철', '첨', '첩', '첫', '첬', '청', '체', '첵', '첸', '첼', '쳄', '쳅', '쳇', '쳉', '쳐', '쳔', '쳤', '쳬', '쳰', '촁', '초', '촉', '촌', '촐', '촘', '촙', '촛', '총', '촤', '촨', '촬', '촹', '최', '쵠', '쵤', '쵬', '쵭', '쵯', '쵱', '쵸', '춈', '추', '축', '춘', '출', '춤', '춥', '춧', '충', '춰', '췄', '췌', '췐', '취', '췬', '췰', '췸', '췹', '췻', '췽', '츄', '츈', '츌', '츔', '츙', '츠', '측', '츤', '츨', '츰', '츱', '츳', '층', '치', '칙', '친', '칟', '칠', '칡', '침', '칩', '칫', '칭', '카', '칵', '칸', '칼', '캄', '캅', '캇', '캉', '캐', '캑', '캔', '캘', '캠', '캡', '캣', '캤', '캥', '캬', '캭', '컁', '커', '컥', '컨', '컫', '컬', '컴', '컵', '컷', '컸', '컹', '케', '켁', '켄', '켈', '켐', '켑', '켓', '켕', '켜', '켠', '켤', '켬', '켭', '켯', '켰', '켱', '켸', '코', '콕', '콘', '콜', '콤', '콥', '콧', '콩', '콰', '콱', '콴', '콸', '쾀', '쾅', '쾌', '쾡', '쾨', '쾰', '쿄', '쿠', '쿡', '쿤', '쿨', '쿰', '쿱', '쿳', '쿵', '쿼', '퀀', '퀄', '퀑', '퀘', '퀭', '퀴', '퀵', '퀸', '퀼', '큄', '큅', '큇', '큉', '큐', '큔', '큘', '큠', '크', '큭', '큰', '클', '큼', '큽', '킁', '키', '킥', '킨', '킬', '킴', '킵', '킷', '킹',
                        '타', '탁', '탄', '탈', '탉', '탐', '탑', '탓', '탔', '탕', '태', '택', '탠', '탤', '탬', '탭', '탯', '탰', '탱', '탸', '턍', '터', '턱', '턴', '털', '턺', '텀', '텁', '텃', '텄', '텅', '테', '텍', '텐', '텔', '템', '텝', '텟', '텡', '텨', '텬', '텼', '톄', '톈', '토', '톡', '톤', '톨', '톰', '톱', '톳', '통', '톺', '톼', '퇀', '퇘', '퇴', '퇸', '툇', '툉', '툐', '투', '툭', '툰', '툴', '툼', '툽', '툿', '퉁', '퉈', '퉜', '퉤', '튀', '튁', '튄', '튈', '튐', '튑', '튕', '튜', '튠', '튤', '튬', '튱', '트', '특', '튼', '튿', '틀', '틂', '틈', '틉', '틋', '틔', '틘', '틜', '틤', '틥', '티', '틱', '틴', '틸', '팀', '팁', '팃', '팅', '파', '팍', '팎', '판', '팔', '팖', '팜', '팝', '팟', '팠', '팡', '팥', '패', '팩', '팬', '팰', '팸', '팹', '팻', '팼', '팽', '퍄', '퍅', '퍼', '퍽', '펀', '펄', '펌', '펍', '펏', '펐', '펑', '페', '펙', '펜', '펠', '펨', '펩', '펫', '펭', '펴', '편', '펼', '폄', '폅', '폈', '평', '폐', '폘', '폡', '폣', '포', '폭', '폰', '폴', '폼', '폽', '폿', '퐁', '퐈', '퐝', '푀', '푄', '표', '푠', '푤', '푭', '푯', '푸', '푹', '푼', '푿', '풀', '풂', '품', '풉', '풋', '풍', '풔', '풩', '퓌', '퓐', '퓔', '퓜', '퓟', '퓨', '퓬', '퓰', '퓸', '퓻', '퓽', '프', '픈', '플', '픔', '픕', '픗', '피', '픽', '핀', '필', '핌', '핍', '핏', '핑', '하', '학', '한', '할', '핥', '함', '합', '핫', '항', '해', '핵', '핸', '핼', '햄', '햅', '햇', '했', '행', '햐', '향', '허', '헉', '헌', '헐', '헒', '험', '헙', '헛', '헝', '헤', '헥', '헨', '헬', '헴', '헵', '헷', '헹', '혀', '혁', '현', '혈', '혐', '협', '혓', '혔', '형', '혜', '혠', '혤', '혭', '호', '혹', '혼', '홀', '홅', '홈', '홉', '홋', '홍', '홑', '화', '확', '환', '활', '홧', '황', '홰', '홱', '홴', '횃', '횅', '회', '획', '횐', '횔', '횝', '횟', '횡', '효', '횬', '횰', '횹', '횻', '후', '훅', '훈', '훌', '훑', '훔', '훗', '훙', '훠', '훤', '훨', '훰', '훵', '훼', '훽', '휀', '휄', '휑', '휘', '휙', '휜', '휠', '휨', '휩', '휫', '휭', '휴', '휵', '휸', '휼', '흄', '흇', '흉', '흐', '흑', '흔', '흖', '흗', '흘', '흙', '흠', '흡', '흣', '흥', '흩', '희', '흰', '흴', '흼', '흽', '힁', '히', '힉', '힌', '힐', '힘', '힙', '힛', '힝', '깄', '꺍', '붥', '닁', '럄', '얬', '칢', '긂', '읩', '뢔', '홥', '렜', '괐', '놰', '뇄', '귬', '찟', '씃', '좠', '쌰', '켙', '잌', '겍', '훕', '띡', '됭', '푈', '뱡', '샾', '궨', '궬', '됸', '솁', '캪', '똠', '슌', '똣', '뚭', '똡',  'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '+', '=', '|', '\\', '}', '{', '[', ']', "'", '"', ';', ':', '/', '?', '.', '>', ',', '<', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', "~", " "]
        self.input_shape = input_shape
        n_classes = len(self.list_chars_ctc) + 1
        self.interpretation_network = load_model(model_path)

    def _code2str(self, network_output):
        """
        Args:
            network_output: 4 dimensional output (N, C)
        
        Return:
            list_strings: outputs for the network input
        """
        list_results = []
        for result_categorized in network_output.astype(np.int):
            list_ch = []
            effective_result = result_categorized[result_categorized!=-1]
            for category in effective_result:
                list_ch.append(self.list_chars_ctc[category])
            word = "".join(list_ch)
            list_results.append(word)
        return list_results

    def _ctc_decode(self, preds):
        return K.get_value(K.ctc_decode(preds, input_length=np.ones(preds.shape[0])*preds.shape[1], greedy=True, beam_width=100, top_paths=1)[0][0])


    def _run_network(self, network_input):
        """
        Args:
            network_input: 4 dimensional inputs (N, h, w, 1)
        
        Return:
            list_strings: outputs for the network input
        """
        # return a rectified image and bounding boxes from a rectified image array
        network_output = self.interpretation_network.predict(network_input)

        predicted_categories = self._ctc_decode(network_output)
        list_predicted_str = self._code2str(predicted_categories) # this returns list
        
        return list_predicted_str


    def _prepare_input(self, img, bboxes, length_margin=10):
        """
        Args:
            img: 3 channel numpy array (h, w, 3)
            bboxes: array-like (xmin, xmax, ymin, ymax)
            length_margin (int): margin for top and bottom (pixels)
        
        Return:
            4 dimensional array (N, h, w, 1)
        """
        n_patches = len(bboxes)
        input_h, input_w = self.input_shape[:2]
        target_h = input_h - 2 * length_margin
        input_arr = np.zeros((n_patches, ) + self.input_shape)
        for i, bbox in enumerate(bboxes):
            xmin, xmax, ymin, ymax = bbox
            if xmax > xmin and ymax > ymin:
                target_w = int((xmax-xmin) * target_h / (ymax-ymin))
                resize_w = target_w if target_w <= self.input_shape[1] else input_w
                resize_h = target_h if target_w <= self.input_shape[1] else int(input_w * (ymax-ymin) / (xmax-xmin))
                resized_patch = cv2.resize(img[ymin:ymax,xmin:xmax,:].astype(np.uint8), (resize_w, resize_h))
                resized_patch_gray = cv2.cvtColor(resized_patch.astype(np.uint8), cv2.COLOR_RGB2GRAY)
                # zero_mean preprocessing
                resized_h, resized_w = resized_patch_gray.shape
                preprocessed_img = (np.ones((input_h, input_w)) * 255).astype(np.uint8) # white background
                preprocessed_img[input_h//2-resized_h//2:input_h//2-resized_h//2+resized_h, input_w//2-resized_w//2:input_w//2-resized_w//2+resized_w, ...] = resized_patch_gray
                preprocessed_img = 1.*cv2.addWeighted (preprocessed_img, 4, cv2.GaussianBlur(preprocessed_img , (0, 0) , self.sigma_blur_local_zero_mean) , -4 , 128) / 128
                input_arr[i,...] = np.expand_dims(preprocessed_img, axis=-1)
            
            
        # # check results
        # from PIL import Image
        # for index in range(n_patches):
        #     Image.fromarray((input_arr[index, ..., 0]*30+128).astype(np.uint8)).save("debug/{}.png".format(index))
        
        return input_arr

    def run(self, img, bboxes):
        """
        Args:
            img: 3 channel numpy array (h, w, 3)
            bboxes: array-like (xmin, xmax, ymin, ymax)
        
        Return:
            pairs of (string, bbox)
        """
        if len(bboxes) > 0:
            network_input = self._prepare_input(img, bboxes)
            list_strings = self._run_network(network_input)
            return zip(list(bboxes), list_strings)
        else:
            return [] # empty list

@singleton
class PDFGenerator:

    def __init__(self):
        self.logger = logging.getLogger()

        st = time.time()
        self.localizer = Localizer("model_files/localizer.h5")
        self.logger.info("loading localizer: {} s".format(time.time() - st))
        st = time.time()
        self.interpreter = Interpreter("model_files/interpreter.h5")
        self.logger.info("loading interpreter: {} s".format(time.time() - st))
        self.SET_NARROW_CHARS = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '+', '=', '|', '\\', '}', '{', '[', ']', "'", '"', ';', ':', '/', '?', '.', '>', ',', '<', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', "~"]

        self.transperant = Color(0, 0, 0, alpha=0)

    def _page_shape(self, img_shape, dpi, return_denominator=True):
        h, w = img_shape[:2]
        denominator = 1. * dpi / 25.4 # dpi: dots per inch, 1 inch=25.4 mm
        page_shape = (int(1.*h/denominator), int(1.*w/denominator))
        if return_denominator:
            return page_shape, denominator
        else:
            return page_shape

    def _conv_coords_pixel2page(self, bbox, string, pagesize, denomianator):
        n_chars = np.sum([0.5 if ch in self.SET_NARROW_CHARS else 1 for ch in string.replace(" ","")])
        xmin, xmax, ymin, ymax =bbox
        h, w = pagesize[:2]
        xmin_in_mm = int(1.*xmin/denomianator)
        xmax_in_mm = int(1.*xmax/denomianator)
        ymin_in_mm = int(1.*ymin/denomianator)
        ymax_in_mm = int(1.*ymax/denomianator)
        
        # coordinates in pdf start from bottom left
        x = xmin_in_mm
        y = h - ymax_in_mm # y coordinate starts from bottom
        font_size = min(ymax_in_mm-ymin_in_mm, 1.*(xmax_in_mm-xmin_in_mm) / n_chars) if n_chars!=0 else 0
        return x, y, font_size

    def _split_words(self, sentence, bbox_sentence, bbox_words):
        assert len(bbox_words) > 0
        
        if len(bbox_words)==1:
            list_words = [sentence]
        else:
            list_words = []
            str_words = sentence.split(" ")
            len_chars = np.sum([np.sum([0.5 if ch in self.SET_NARROW_CHARS else 1 for ch in w]) for w in str_words])
            list_width_bbox_words = [bbox[1]-bbox[0] for bbox in bbox_words]
            total_width_bbox_words = np.sum(list_width_bbox_words)
            n_words_per_pixel = 1. * len_chars / total_width_bbox_words
            str_start_index, index_bbox_words = 0, 0
            while str_start_index < len(str_words) and index_bbox_words < len(list_width_bbox_words):
                if str_start_index == len(str_words) - 1: # final element (no need to compute)
                    n_words = 1
                elif str_start_index < len(str_words)-1:    
                    n_chars_est = n_words_per_pixel * list_width_bbox_words[index_bbox_words]
                    for n_words in range(1, len(str_words)-str_start_index):
                        n_chars = np.sum([np.sum([0.5 if ch in self.SET_NARROW_CHARS else 1 for ch in w]) for w in str_words[str_start_index:str_start_index + n_words]])
                        n_chars_add = np.sum([np.sum([0.5 if ch in self.SET_NARROW_CHARS else 1 for ch in w]) for w in str_words[str_start_index:str_start_index + n_words + 1]])
                        if np.abs(n_chars_add - n_chars_est) > np.abs(n_chars - n_chars_est):
                            break
                            
                # add words and udpate the indices
                list_words.append(" ".join(str_words[str_start_index:str_start_index + n_words]))
                str_start_index+=n_words
                index_bbox_words+=1
                
            # insert dummy strings for non-allocated bboxes
            for _ in range(len(bbox_words)-len(list_words)):
                list_words.append(" ")
            # insert remaning words into the last bbox
            for index_remnant in range(str_start_index, len(str_words)):
                list_words.append(str_words[index_remnant])

        return list_words

    def run(self, fpath):
        pdfWriter = PdfFileWriter()
        
        input_handler = InputHandler()
        input_handler.set_fpath(fpath)
        output_path = os.path.splitext(fpath)[0] + "_OCR_processed.pdf"
        if os.path.exists(output_path): # remove output file if exists
            os.remove(output_path)

        for page, img_arr in input_handler.iterimages():
            # make an image have 3 channels
            img_shape = img_arr.shape
            if len(img_shape)==2:
                h, w = img_shape
                img_arr = np.repeat(np.expand_dims(img_arr, axis=-1), 3, axis=-1)
            elif len(img_shape)==3:
                h, w, _ = img_shape
            elif len(img_shape)==4: 
                h, w, _, _ = img_shape
                img_arr = img_arr[...,:3]
            else:
                self.logger.error("[error] image ({} - page {}) should have 2 or 3 or 4 channels".format(os.path.basename(fpath), page))
                continue

            # localize sentences
            st = time.time()
            rectified_img, bboxes_lines, bboxes_words, mask_ch_center, mask_ch_region, r_angle = self.localizer.localize_lines(img_arr, return_intermediate_results=True)
            self.logger.info("running localization network: {}".format(time.time() - st))
            
            # interpret sentences
            st = time.time()
            list_bbox_sentence = self.interpreter.run(rectified_img, bboxes_lines)
            self.logger.info("running iterpretation network: {}".format(time.time() - st))
            # for st in list_bbox_sentence:
            #     print(st)

            # write to pdf 
            with io.BytesIO() as processed_pdf_io_pipe:
                st = time.time()
                out_page_shape, denomianator = self._page_shape(rectified_img.shape, dpi=300, return_denominator=True)
                can = canvas.Canvas(processed_pdf_io_pipe, pagesize=(out_page_shape[1], out_page_shape[0]))
                can.setFillColor(self.transperant)
                for index_sentence, element in enumerate(list_bbox_sentence):
                    # split words in a sentence
                    bbox_sentence, sentence = element
                    bboxes_words_in_sentence = bboxes_words[index_sentence]
                    list_words = self._split_words(sentence, bbox_sentence, bboxes_words_in_sentence)
                    # write each word
                    for index_words in range(len(bboxes_words_in_sentence)):
                        x, y, font_size = self._conv_coords_pixel2page(bboxes_words_in_sentence[index_words], list_words[index_words], out_page_shape, denomianator)
                        if font_size != 0:
                            can.setFont("NanumMyeongjo", font_size)
                            can.drawString(x, y, u"{}".format(list_words[index_words]))
                can.save()
                processed_pdf = PdfFileReader(processed_pdf_io_pipe)
                self.logger.info("writing to pdf: {}".format(time.time() - st))
            
                # merge original image (pdf) -- CAVEATE: processed_pdf_io_pipe should be open to merge pdfs
                st = time.time()
                with io.BytesIO() as ori_pdf_io_pipe:
                    Image.fromarray(rectified_img).save(ori_pdf_io_pipe, format="PDF") # output rectified image into io pipe
                    original_pdf = PdfFileReader(ori_pdf_io_pipe)
                    page_ori, page_processed = original_pdf.getPage(0), processed_pdf.getPage(0) # get pages
                    rot_ori, rot_gen = page_ori.get('/Rotate'), page_processed.get('/Rotate') # get rotation info (rot_gen is supposed to be 0)
                    self.logger.info("[PDFGenerator] rot_ori: {}, rot_gen: {}".format(rot_ori, rot_gen))
                    
                    # resize to the genertaed size
                    if rot_ori is None or (rot_ori-rot_gen) % 180==0:
                        target_width, target_height = float(page_processed.mediaBox.getWidth()), float(page_processed.mediaBox.getHeight())
                    else:
                        target_width, target_height = float(page_processed.mediaBox.getHeight()), float(page_processed.mediaBox.getWidth())
                    page_ori.scaleTo(target_width, target_height)

                    # merge two pdf
                    if rot_ori is None:
                        page_ori.mergePage(page_processed)
                    else:
                        page_ori.mergeRotatedTranslatedPage(page_processed, rot_ori-rot_gen, float(page_processed.mediaBox.getWidth()/2), float(page_processed.mediaBox.getWidth()/2))
                    # add to pdf writer incremently 
                    pdfWriter.addPage(page_ori)
                    with open(output_path, 'wb') as out_f:
                        pdfWriter.write(out_f)
                    self.logger.info("merging with the original image: {}".format(time.time() - st))
            

# file selection GUI
def get_fpath():
    fpath = askopenfilename()
    if len(fpath) > 0: # run if any file is selected
        pdf_generator = PDFGenerator()
        pdf_generator.run(fpath)

frame = Frame(root)
frame.pack()
button = Button(frame, text="파일 선택", command=get_fpath)
button.pack()
waiting_label.destroy()
creator_label = Label(root)
creator_label.pack()
creator_label.config(text="Powered by Jaemin Son")
root.update()
root.update()
root.mainloop()
